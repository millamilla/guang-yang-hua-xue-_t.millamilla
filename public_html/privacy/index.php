<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>個人情報保護方針 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-privacy">
			<div class="in">
				<h2><span>個人情報保護方針</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/privacy/" itemprop="url">個人情報保護方針</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<h2 class="title-01"><span class="in">個人情報の取り扱いについて</span></h2>
				<p class="text-02">お問合せフォームにご記入いただいたお客様の個人情報については、弊社において下記のとおりお取り扱いいたします。</p>
			</section>
			<!--section--> 
			
			<!--section-->
			<section id="sec-02" class="section-01">
				<h2 class="title-01"><span class="in">個人情報保護方針</span></h2>
				<p class="text-02">光陽化学工業株式会社では、お客様からのお問合せ等で収集した個人情報のお取り扱いに関して、安心してご利用いただけるよう、「個人情報保護方針」を策定し、全従業員に個人情報保護の重要性の認識と取組みを徹底させることにより、これを推進して参ります。</p>
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-02"><span class="in">個人情報の管理</span></h3>
					<p class="text-02">当社は、お客さまの個人情報を正確かつ最新の状態に保ち、個人情報への不正アクセス・紛失・破損・改ざん・漏洩などを防止するため、セキュリティシステムの維持・管理体制の整備・社員教育の徹底等の必要な措置を講じ、安全対策を実施し個人情報の厳重な管理を行います。</p>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-02"><span class="in">個人情報の利用目的</span></h3>
					<p class="text-02">本ウェブサイトでは、お客様からのお問合せ時に、お名前、e-mail アドレス、電話番号等の個人情報をご登録いただく場合がございますが、これらの個人情報はご提供いただく際の目的以外では利用いたしません。<br>
						お客さまからお預かりした個人情報は、当社からのご連絡や業務のご案内やご質問に対する回答として、電子メールや資料のご送付に利用いたします。</p>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-02"><span class="in">個人情報の第三者への開示・提供の禁止</span></h3>
					<p class="text-02">当社は、お客さまよりお預かりした個人情報を適切に管理し、次のいずれかに該当する場合を除き、個人情報を第三者に開示いたしません。</p>
					<ul class="list-02">
						<li>お客さまの同意がある場合</li>
						<li>お客さまが希望されるサービスを行なうために当社が業務を委託する業者に対して開示する場合</li>
						<li>法令に基づき開示することが必要である場合</li>
					</ul>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-02"><span class="in">個人情報の安全対策</span></h3>
					<p class="text-02">当社は、個人情報の正確性及び安全性確保のために、セキュリティに万全の対策を講じています。</p>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-02"><span class="in">ご本人の照会</span></h3>
					<p class="text-02">お客さまがご本人の個人情報の照会・修正・削除などをご希望される場合には、ご本人であることを確認の上、対応させていただきます。</p>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-02"><span class="in">法令、規範の遵守と見直し</span></h3>
					<p class="text-02">当社は、保有する個人情報に関して適用される日本の法令、その他規範を遵守するとともに、本ポリシーの内容を適宜見直し、その改善に努めます。</p>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<p class="text-02">弊社は、事前の通知を行うことなく、上記の内容を変更・改善する場合があり、変更後の内容は、当ホームページ上に明記致します。</p>
				</section>
				<!--section--> 
				
			</section>
			<!--section--> 
			
			<!--section-->
			<section id="sec-03" class="section-01">
				<h2 class="title-01"><span class="in">個人情報に関するお問合せ窓口</span></h2>
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-02"><span class="in">光陽化学工業株式会社 本社</span></h3>
					<p class="text-02">〒536-0025 大阪市城東区森之宮２丁目３番５号<br>
						TEL：06-6969-1821（代表）<br>
						FAX：06-6969-1825</p>
					<p><a class="btn-01" href="/company/index.php#map01"><span class="ico-map">周辺マップはこちら</span></a></p>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-02"><span class="in">東京支社</span></h3>
					<p class="text-02">〒103-0024 東京都中央区日本橋小舟町１５番１５号 ルネ小舟町ビル２Ｆ<br>
						TEL：03-3661-2700（代表）<br>
						FAX：03-3661-2706</p>
					<p><a class="btn-01" href="/company/index.php#map02"><span class="ico-map">周辺マップはこちら</span></a></p>
				</section>
				<!--section--> 
				
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->

</body>
</html>
