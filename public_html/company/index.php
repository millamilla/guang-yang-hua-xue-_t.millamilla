<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>会社案内 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-company">
			<div class="in">
				<h2><span>会社案内</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/company/" itemprop="url">会社案内</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--lnav-->
			<div class="lnav">
				<ul class="num-4">
					<li><a href="#sec-01"><span>企業理念</span></a></li>
					<li><a href="#sec-02"><span>会社概要</span></a></li>
					<li><a href="#sec-03"><span>沿革</span></a></li>
					<li><a href="#sec-04"><span>アクセス</span></a></li>
				</ul>
			</div>
			<!--lnav--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<h2 class="title-center"><span class="in">企業理念</span></h2>
				<p class="img ta-c"><img src="/img/contents/img_company_01.jpg" class="mw-1000" alt="最新の技術、最先端の素材で新しい価値の創造にチャレンジし続けます。"/></p>
				<p class="text-01">私たちは、「技術と環境の調和」をコンセプトに、<br>
					様々な分野に環境と安全に配慮した「使って安全・使って安心・使って優しい」製品を提供しています。<br>
					これからも研究・開発型企業の先駆者として、新しい価値の創造にチャレンジし続け、<br>
					化学でもっと素敵な社会の発展に貢献します。 </p>
			</section>
			<!--section--> 
			
			<!--section-->
			<section id="sec-02" class="section-01">
				<h2 class="title-01"><span>会社概要</span></h2>
				<div class="table-02">
					<table>
						<tbody>
							<tr>
								<th class="sp-w1">商号</th>
								<td>光陽化学工業株式会社(KOYO CHEMICALS INC.) </td>
							</tr>
							<tr>
								<th>設立</th>
								<td>1974年（昭和49年）3月</td>
							</tr>
							<tr>
								<th>資本金</th>
								<td>3,000万円</td>
							</tr>
							<tr>
								<th>役員</th>
								<td>代表取締役社長 内田 年昭</td>
							</tr>
							<tr>
								<th>社員数</th>
								<td>45名</td>
							</tr>
							<tr>
								<th>営業種目</th>
								<td>化学工業薬品の製造販売<br>
									印刷関連及び写真製版用薬品・機材の製造販売 </td>
							</tr>
							<tr>
								<th>本社所在地</th>
								<td>本社／工場 〒536-0025 大阪市城東区森之宮２丁目３番５号<br>
									TEL06-6969-1821 FAX06-6969-1825<br>
									<br>
									<p><a class="btn-01" href="#map01"><span class="ico-map">周辺マップはこちら</span></a></p>
									<br></td>
							</tr>
							<tr>
								<th>支社所在地</th>
								<td>東京支社 〒103-0024 東京都中央区日本橋小舟町１５番１５号 ルネ小舟町ビル２Ｆ<br>
									TEL03-3661-2700 FAX03-3661-2706<br>
									<br>
									<p><a class="btn-01" href="#map02"><span class="ico-map">周辺マップはこちら</span></a></p>
									<br></td>
							</tr>
						</tbody>
					</table>
				</div>
			</section>
			<!--section--> 
			
			<!--section-->
			<section id="sec-03" class="section-01">
				<h2 class="title-01"><span>沿革</span></h2>
				<div class="table-02">
					<table>
						<tbody>
							<tr>
								<th class="sp-w2">1958年（昭和33年） 7月</th>
								<td>「株式会社光陽社研究所」として、大阪市城東区森之宮に発足</td>
							</tr>
							<tr>
								<th>1963年（昭和38年） 5月</th>
								<td>「コーヨー・レジスト」の製造販売開始<br>
									感光剤技術でプリンティング事業分野に進出</td>
							</tr>
							<tr>
								<th>1964年（昭和39年）10月 </th>
								<td>研究所を新築</td>
							</tr>
							<tr>
								<th>1974年（昭和49年） 3月</th>
								<td>(株)光陽社研究所より独立し、光陽化学工業株式会社設立（資本金1,000万円）</td>
							</tr>
							<tr>
								<th>1976年（昭和51年） 3月</th>
								<td>増資 資本金2,000万円</td>
							</tr>
							<tr>
								<th>1976年（昭和51年）10月</th>
								<td>東京営業所開設（東京都新宿区）</td>
							</tr>
							<tr>
								<th>1978年（昭和53年）11月</th>
								<td>本社ビル竣工</td>
							</tr>
							<tr>
								<th>1980年（昭和55年）10月</th>
								<td>増資 資本金3,000万円</td>
							</tr>
							<tr>
								<th>1986年（昭和61年）11月</th>
								<td>本社工場竣工</td>
							</tr>
							<tr>
								<th>1987年（昭和62年） 12月</th>
								<td>洗浄技術で医療分野に進出</td>
							</tr>
							<tr>
								<th>1987年（昭和62年） 4月</th>
								<td>東京営業所を東京支社に改称</td>
							</tr>
							<tr>
								<th>1994年（平成 6年） 2月</th>
								<td>中国合弁会社深圳美科化工有限公司 業務開始</td>
							</tr>
							<tr>
								<th>1996年（平成 8年） 4月</th>
								<td>感光剤技術で電子材料分野に進出<br>
									オフセット印刷関連薬品の本格販売</td>
							</tr>
							<tr>
								<th>1997年（平成 9年） 10月</th>
								<td>環境対策製品の販売強化<br>
									（「使って安心、使って安全、使って優しい」 シリーズ）の発売開始</td>
							</tr>
							<tr>
								<th>1998年（平成10年） 10月</th>
								<td>オフセット印刷用湿し水添加液の発売開始</td>
							</tr>
							<tr>
								<th>2000年（平成12年） 2月</th>
								<td>ISO9001認証取得（JQA-QM4342）</td>
							</tr>
							<tr>
								<th>2003年（平成15年） 7月</th>
								<td>(株)光陽社（親会社）より自社株全株を取得</td>
							</tr>
							<tr>
								<th>2004年（平成16年） 4月</th>
								<td>生活用品分野に進出</td>
							</tr>
							<tr>
								<th>2005年（平成17年） 5月</th>
								<td>オフセット印刷用湿し水機能性濃縮添加液の発売開始</td>
							</tr>
							<tr>
								<th>2005年（平成17年） 7月</th>
								<td>深圳美科化工有限公司を深圳光陽印刷器材有限公司に社名変更</td>
							</tr>
							<tr>
								<th>2008年（平成20年） 9月</th>
								<td>東京支社移転（東京都中央区）</td>
							</tr>
							<tr>
								<th>2016年（平成28年） 2月</th>
								<td>ISO9001認証 自主返上 (JQA-QM4342)</td>
							</tr>
							<tr>
								<th>2019年（令和元年） 11月</th>
								<td>「深圳光陽印刷器材有限公司」の株式譲渡</td>
							</tr>
						</tbody>
					</table>
				</div>
			</section>
			<!--section--> 
			
			<!--section-->
			<section id="sec-04" class="section-01">
				<h2 class="title-01"><span>アクセスマップ</span></h2>
				
				<!--section-->
				<section id="map01" class="section-02">
					<h3 class="title-02">本社／工場</h3>
					<p class="text-02">〒536-0025 大阪市城東区森之宮２丁目３番５号<br>
						TEL06-6969-1821 FAX06-6969-1825</p>
					<div class="gmap">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3280.8937416637045!2d135.5374680152318!3d34.68263118043913!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000e0bf31574259%3A0x94899146373ae15f!2z44CSNTM2LTAwMjUg5aSn6Ziq5bqc5aSn6Ziq5biC5Z-O5p2x5Yy65qOu5LmL5a6u77yS5LiB55uu77yT4oiS77yV!5e0!3m2!1sja!2sjp!4v1520729053848" width="1000" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</section>
				<!--/section--> 
				
				<!--section-->
				<section id="map02" class="section-02">
					<h3 class="title-02">東京支社</h3>
					<p class="text-02">〒103-0024 東京都中央区日本橋小舟町１５番１５号 ルネ小舟町ビル２Ｆ<br>
						TEL03-3661-2700 FAX03-3661-2706 </p>
					<div class="gmap">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3240.6499169120116!2d139.77748571525908!3d35.68562118019333!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188951133fc339%3A0xc55ca4676c91c892!2z44CSMTAzLTAwMjQg5p2x5Lqs6YO95Lit5aSu5Yy65pel5pys5qmL5bCP6Iif55S677yR77yV4oiS77yR77yV!5e0!3m2!1sja!2sjp!4v1520729082677" width="1000" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</section>
				<!--/section--> 
				
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->

</body>
</html>
