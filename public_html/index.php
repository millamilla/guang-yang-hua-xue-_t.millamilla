<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>光陽化学工業</title>

<!-- inc -->
<?php include("inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--movie-->
		<section id="movie-index">
			<div class="in video-slider">
			<!--	<h2><img src="/img/index/copy_index.png" alt="研究・開発型企業として、歩み続けた先駆者の軌跡"/></h2>-->
				<p class="scroll"><a href="#item-index"><span></span><span></span><span></span></a></p>
					
				<video src="/img/index/mv_index.mp4" autoplay muted playsinline id="top-video">
					<p>動画を再生するにはvideoタグをサポートしたブラウザが必要です。</p>
				</video>
				
					<ul class="top-slider">
						<li><a href="/search/offset.php"><img src="/img/contents/top-slide1.png" alt="image01"></a></li>
						<li><a href="/search/uvink.php"><img src="/img/contents/top-slide2.png" alt="image02"></a></li>
						<li><a href="/search/oilyink.php"><img src="/img/contents/top-slide3.png" alt="image03"></a></li>
					</ul>
			
				
			</div>
		</section>
		<!--/movie--> 
		
		<!--item-index-->
		<section id="item-index">
			<h2 class="title-info"><span class="in"><span class="">PRODUCT<br><span class="small">光陽化学からのおすすめ</span></span></span></h2>
			<p class="fs-text">こちらからお選びください</p>
			<ul class="list-item" id="top_btns">
				<li class="single"><a href="/search/offset.php" class="no-">
					<p class="text">オフセット<br class="sp-only">輪転機用</p>
					</a></li>
				<li class="single"><a href="/search/oilyink.php" class="no-">
					<p class="text">枚葉機油性<br class="sp-only">インキ用</p>
					</a></li>
				<li class="single"><a href="/search/uvink.php" class="no-">
					<p class="text">枚葉機 UV <br class="sp-only">インキ用</p>
					</a></li>
			</ul>
			<div class="in_bnr"><a href="/valuableinfo/environment.php"><img src="/img/contents/top_product-bnr.png"></a></div>
		</section>
		<!--/item-index--> 

		<!--info-index-->
		<section id="info-index">
			<h2 class="title-info"><span class="in"><span class="eng">What's New</span></span></h2>
			<ul class="list-info">
			<li class="single">
					<p class="ico"><span>新着</span></p>
					<p class="date">2021.12.09</p>
					<p class="text"><a href="doc/whatsnew_deliv_211209.pdf" target="_blank">年末年始休暇期間前後のデリバリーについてのご案内</a></p>
				</li>
			<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2021.09.02</p>
					<p class="text"><a href="doc/whatsnew_pocket_210901.pdf" target="_blank">ポケット湿し水濃度計 PAL-SOLAIA 販売中止のご案内</a></p>
				</li>
			<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2021.07.21</p>
					<p class="text"><a href="doc/whatsnew_polytape_210721.pdf" target="_blank">ポリエステルテープ 販売中止のご案内</a></p>
				</li>
			<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2021.07.20</p>
					<p class="text"><a href="doc/whatsnew_deliv_210721.pdf" target="_blank">８月夏季休暇期間前後のデリバリーについてのご案内</a></p>
				</li>
			<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2021.06.29</p>
					<p class="text"><a href="doc/whatsnew_deliv_2107.pdf" target="_blank">7月特別祝日前後のデリバリーについてのご案内</a></p>
				</li>
			<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2021.04.16</p>
					<p class="text"><a href="doc/whatsnew_gw.pdf" target="_blank">5月ゴールデンウィーク休暇期間前後のデリバリーについてのご案内</a></p>
				</li>
			<li class="single">
					<!-- <p class="ico"><span>新着</span></p> -->
					<p class="ico">&nbsp;</p>
					<p class="date">2020.12.11</p>
					<p class="text"><a href="doc/whatsnews_201211_1.pdf" target="_blank"  style="text-decoration:none;"><span style="text-decoration:underline;">年末年始休暇期間前後のデリバリーについてのご案内</span></a></p>
				</li>
			<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2020.08.21</p>
					<p class="text"><a href="doc/whatsnews_200821_1.pdf" target="_blank"  style="text-decoration:none;"><span style="text-decoration:underline;">今後の機器修理活動について</span></a></p>
				</li>
			<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2020.08.19</p>
					<p class="text"><a href="doc/whatsnews_200817_1.pdf" target="_blank"  style="text-decoration:none;">&#9312;　<span style="text-decoration:underline;">「ラバーメンテナンスクリーナー RB-1」の販売中止について</span></a><br>
					<a href="doc/whatsnews_200730_1.pdf" target="_blank"  style="text-decoration:none;">&#9313;　<span style="text-decoration:underline;">「ラバーメンテナンスクリーナー RB-1」を2020.10.1以降に所有されている場合の注意点</span></a></p>
				</li>
			<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2020.07.30</p>
					<p class="text"><a href="doc/whatsnews_200730_2.pdf" target="_blank"  style="text-decoration:none;"><span style="text-decoration:underline;">新製品「ラバーメンテナンスクリーナー RB-2EX」が2020年8月3日（月）に発売開始</span></a></p>
				</li>
			<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2020.07.27</p>
					<p class="text"><a href="doc/whatsnews_200727_1.pdf" target="_blank">８月夏季休暇期間前後のデリバリーについてのご案内　1</a><br>
					<a href="doc/whatsnews_200727_2.pdf" target="_blank">８月夏季休暇期間前後のデリバリーについてのご案内　2</a></p>
				</li>
			<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2020.07.20</p>
					<p class="text"><a href="doc/whatsnews_200720.pdf" target="_blank">「新型コロナウィルス感染症対策」と「ご来社される皆様へご協力のお願い」</a></p>
				</li>
			<li class="single">
				<p class="ico">&nbsp;</p>
					<p class="date">2020.06.26</p>
					<p class="text">新型コロナウイルス感染拡大に伴う弊社の対応とお願い（2）<br>7月も受注締切時間12時を継続することと致します。<br>ご理解ご協力のほどよろしくお願い申し上げます。</p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2020.05.26</p>
					<p class="text"><a href="doc/whatsnews_200526.pdf" target="_blank">新型コロナウイルス感染拡大に伴う弊社の対応とお願い（1）</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2020.05.18</p>
					<p class="text"><a href="doc/whatsnew_200518.pdf" target="_blank">ご注文FAX送信先（番号）変更のお願い</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2020.04.16</p>
					<p class="text"><a href="doc/whatsnew_200416.pdf" target="_blank">5月ゴールデンウィーク休暇期間前後のデリバリーについてのご案内</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2020.04.10</p>
					<p class="text"><a href="doc/whatsnew_200410.pdf" target="_blank">新型コロナウイルス感染拡大に伴う弊社の対応とお願い</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2019.12.11</p>
					<p class="text"><a href="doc/whatsnew_191211_1.pdf" target="_blank">年末年始休暇前後のデリバリーについてのご案内</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2019.10.21</p>
					<p class="text"><a href="newitem/">新製品「セーフティーUVインキ洗浄液SV-1」「セーフティーダンプキーパーPK-25EX」を発売しました。</a></p>
				</li>

				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2019.10.21</p>
					<p class="text">新製品「ネオフイルムクリーナー NF-55」を発売しました。</p>
				</li>
		
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2019.10.21</p>
					<p class="text"><a href="doc/whatsnew_191021_2.pdf" target="_blank">「SOLAIA WEB WK801、SOLAIA 517」製品ラベルの変更および<br>「ニューAPクリーナー、ニューAPプロテクター」外箱ケース変更のご案内</a></p>
				</li>
				
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2019.10.21</p>
					<p class="text"><a href="doc/whatsnew_191021_1.pdf" target="_blank">水棒関連品、インキローラー関連品、製版関連品の販売中止について</a></p>
				</li>
				
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2019.07.26</p>
					<p class="text"><a href="doc/whatsnew_190726.pdf" target="_blank">8月夏季休暇期間前後のデリバリーについてのご案内</a></p>
				</li>
			
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2019.06.12</p>
					<p class="text"><a href="doc/whatsnew_190611.pdf" target="_blank">大阪サミット開催に伴うデリバリーについてのご案内</a></p>
				</li>

				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2019.04.23</p>
					<p class="text"><a href="doc/whatsnew_190416.pdf" target="_blank">製版関連品、機器関連品の販売中止について</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2019.03.01</p>
					<p class="text">2018年9月開始のサンプル提供キャンペーンは終了しました。</p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.10.29</p>
					<p class="text"><a href="/newitem/">New Itemページを更新しました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.10.29</p>
					<p class="text"><a href="/search/">Focus Products ページをアップしました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.09.12</p>
					<p class="text"><a href="/newitem/">New Itemページを更新しました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.09.03</p>
					<p class="text">新製品のサンプルはＨＰから申込みができるようになりました。</p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.09.03</p>
					<p class="text"><a href="/newitem/">新製品「SOLAIA WEB WK711クリア」「SOLAIA 517」を発売しました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.07.24</p>
					<p class="text"><a href="/lab/index.php#sec-02">技術レポートをアップしました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.07.24</p>
					<p class="text"><a href="/newitem/">新製品ページを更新しました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.07.17</p>
					<p class="text"><a href="/business/">事業内容ページを更新しました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.04.16</p>
					<p class="text"><a href="">HPリニューアルしました。</a></p>
				</li>
			</ul>
		</section>
		<!--/info-index--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("inc/script.php"); ?>
<!-- /inc -->
<script>
	function mypreload() {
		for(var i = 0; i< arguments.length; i++){
			$("<img>").attr("src", arguments[i]);
		}
	}
	//関数の呼び出し。引数には先読みしておく画像のパスを指定  
	mypreload('/img/contents/top-slide1.png', '/img/contents/top-slide2.png', '/img/contents/top-slide3.png');

	$('.top-slider').slick({
	autoplay:true,
	autoplaySpeed:5100,
	dots:true,
	});
	 $('.top-slider').hide();
	 $( window ).ready(function(){
	 	var topVideo = document.getElementsByTagName('video')[0];
	 	topVideo.addEventListener('ended', function() {
	 		$('.top-slider').show().slick('setPosition');
	 }, false);
	 });
	var topVideo = document.getElementById('top-video');
</script>
</body>
</html>
