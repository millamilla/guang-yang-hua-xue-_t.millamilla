<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>研究開発 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-lab">
			<div class="in">
				<h2><span>研究開発</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/company/" itemprop="url">研究開発</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<h2 class="title-center"><span class="in">「技術と環境の調和」</span></h2>
				<div class="box-lab-01">
					<p class="text-03"><strong>「技術と環境の調和」</strong>をコンセプトに、界面科学を活かした配合技術を強みとし、研究開発を推進しています。</p>
					<p class="text-03">有機溶剤を多用する工業分野において、水を活用することは、使用する有機溶剤を削減できるので、環境と安全への配慮の観点で有用です。</p>
					<p class="text-03">当社の研究開発は、界面活性剤や高分子化学薬剤に新たな素材を加え、水を改質し、有機溶剤の性能を補完する水―有機混合薬剤や、新たな機能を付与した水系薬剤やエマルション、ペースト状薬剤に至るまで、幅広い製品設計に対応し、高機能洗浄剤や多機能水などの工業用薬剤を展開しています。</p>
					<p class="text-03">湿し水や消防法非該当のローラー洗浄液は、プリンティング事業に応用展開した当社の代表製品です。なかでも、湿し水は、オフセット（平版）印刷における版面上での濡れ性向上を目的とした工業用薬剤で、親油性の画線部と親水性の非画線部を区別するために、低い表面張力と適度なインキ乳化性、印刷版の整面性をコントロールする多機能水の代表です。</p>
					<p class="text-03">私達は、科学の目で物事の「本質」を追求し、新たな価値を創り続けることで、社会貢献していきます。</p>
					<div class="box-lab-02">
						<div class="box-lab-02-01">
							<p><img src="/img/contents/img_lab_01.jpg" alt=""/></p>
							<p class="text-">実験室</p>
						</div>
						<div class="box-lab-02-02">
							<p><img src="/img/contents/img_lab_02.jpg" alt=""/></p>
							<p class="text-">試験分析室</p>
						</div>
					</div>
					<div class="ta-c">
						<p class="text-04">技術に関するお問合せはこちらから</p>
						<p><a href="/inquiry/" class="btn-02"><span>お問合せフォーム</span></a></p>
					</div>
				</div>
			</section>
			<!--section--> 
			
			<!--section-->
			<section id="sec-02" class="section-01">
				<h2 class="title-01"><span class="in">技術レポート</span></h2>
				
				<!--section-->
				<section class="section-03">
					<h3 class="toggle-head"><span>湿し水について　vol.1</span></h3>
					
					<!--toggle-body-->
					<div class="toggle-body"> 
						
						<!--section-->
						<section class="section-lab">
							<h4 class="title-03">1．エッチ液に関する基礎知識</h4>
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">湿し水の組成</h5>
								<p class="img"><img src="/img/contents/img_lab_03.jpg" alt=""/></p>
								<p class="text-02">※添加液：一般的にはアルコール類<br>
									一部グリコールエーテル類あり</p>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">湿し水原水</h5>
								<ul class="list-01">
									<li>水道水</li>
									<li>工業用水<br>
										河川水、地下水等から簡易処理された水<br>
										（殺菌処理等なし）</li>
									<li>純水<br>
										イオン交換水、RO水</li>
								</ul>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">湿し水原水について①</h5>
								<dl class="dlist-01">
									<dt>pHとは</dt>
									<dd>pHは一般的に数値が1から14( 10-¹～10-¹⁴mol/l)であり、pH値が7より小さいとき酸性を示し、7より大きいときアルカリ性を示します。<br>
										飲料水（水道水）水質基準では、pH 5.8～8.6(弱酸～弱アルカリ)と定められています。</dd>
								</dl>
								<dl class="dlist-01">
									<dt>導電率（電気伝導率）とは</dt>
									<dd>水の電気伝導率はその水の電気の通し易さを示すものです。<br>
										水が電気を通すのは水中の電解質によるものであり、水中に電解質の量が多ければ多いほど電流が多く流れます。</dd>
								</dl>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">湿し水原水について②</h5>
								<dl class="dlist-01">
									<dt>水の硬度とは</dt>
									<dd>水中のカルシウムイオン及びマグネシウムイオンの量を、これに対応する炭酸カルシウム（CaCO3）の量に換算して水１リットル中のmg数で表したもので、カルシウム硬度とマグネシウム硬度の合計量を全硬度（又は総硬度）といいます。</dd>
								</dl>
								<dl class="dlist-01">
									<dt>残留塩素とは</dt>
									<dd>消毒用塩素化合物（次亜塩素酸ナトリウム、次亜塩素酸カルシウム等）のうち、水道水中に残っている塩素のことを残留塩素と言います。</dd>
								</dl>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">エッチ液・添加液の種類</h5>
								<div class="tbl-lab-01">
									<table>
										<tbody>
											<tr>
												<th>エッチ液</th>
												<th>アルコール系添加液</th>
											</tr>
											<tr>
												<td>溶剤Ⅰ系<br>
													（溶剤４０％未満）</td>
												<td>エタノール</td>
											</tr>
											<tr>
												<td>溶剤Ⅱ系<br>
													（溶剤４０％以上）</td>
												<td>IPA</td>
											</tr>
											<tr>
												<td>nonVOC</td>
												<td>NPA</td>
											</tr>
											<tr>
												<td>ガム系</td>
												<td>TBA</td>
											</tr>
										</tbody>
									</table>
								</div>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">エッチ液成分と効能①</h5>
								<div class="tbl-lab-01">
									<table>
										<tbody>
											<tr>
												<th>成分</th>
												<th>含有量</th>
												<th>原料名</th>
											</tr>
											<tr>
												<th>溶剤</th>
												<td>２０～６０％</td>
												<td class="ta-l">エチレングリコールエーテル<br>
													プロピレングリコールエーテル</td>
											</tr>
											<tr>
												<th>界面活性剤</th>
												<td>１～５％</td>
												<td class="ta-l">ノニオン系界面活性剤<br>
													アニオン系界面活性剤</td>
											</tr>
											<tr>
												<th>水溶性樹脂</th>
												<td>１～５％</td>
												<td class="ta-l">アラビアガム、CMC、セルロース系<br>
													（溶剤可溶タイプ）</td>
											</tr>
											<tr>
												<th>酸類</th>
												<td>１～５％</td>
												<td class="ta-l">リン酸、クエン酸、酢酸、酒石酸</td>
											</tr>
											<tr>
												<th>塩類</th>
												<td>１～５％</td>
												<td class="ta-l">リン酸塩、クエン酸塩、硝酸塩</td>
											</tr>
											<tr>
												<th>その他</th>
												<td>微量</td>
												<td class="ta-l">消泡剤、防腐剤、防錆剤<br>
													カルシウムキレート剤他</td>
											</tr>
										</tbody>
									</table>
								</div>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">エッチ液成分と効能②</h5>
								<div class="tbl-lab-01">
									<table>
										<tbody>
											<tr>
												<th>成分</th>
												<th>効能</th>
											</tr>
											<tr>
												<th>溶剤</th>
												<td class="ta-l">・動的表面張力を下げて版面を瞬時に濡らす<br>
													・粘度を上げて、ローラー間を通過しやすくする</td>
											</tr>
											<tr>
												<th>界面活性剤</th>
												<td class="ta-l">・表面張力を下げて版面を瞬時に濡らす<br>
													・軽度の油汚れの除去</td>
											</tr>
											<tr>
												<th>水溶性樹脂</th>
												<td class="ta-l">・非画線部に吸着し親水性を維持する<br>
													・エッチ液の粘度調整</td>
											</tr>
											<tr>
												<th>酸類</th>
												<td class="ta-l">・pHを弱酸性にし、インキ汚れを抑制する</td>
											</tr>
											<tr>
												<th>塩類</th>
												<td class="ta-l">・pHを変動させる物質が混入しても、緩衝剤として働きpH変化を少なくする</td>
											</tr>
										</tbody>
									</table>
								</div>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">湿し水としての役割</h5>
								<ul class="list-01">
									<li>動的表面張力の低下</li>
									<li>非画線部の親水性を維持</li>
									<li>乳化力（水分散力）の安定化</li>
									<li>その他<br>
										（消泡、防錆、防腐、グレーズ抑制）</li>
								</ul>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">非画線部の親水性を維持</h5>
								<dl class="dlist-01">
									<dt>インキ油や水アカ汚れ</dt>
									<dd>エッチ液成分の界面活性剤＋無機塩の配合で洗浄効果および付着防止効果を最大限に発揮する必要があります。</dd>
								</dl>
								<dl class="dlist-01">
									<dt>アルミ表面の酸化による感脂化汚れ</dt>
									<dd>除去効果が高い酸により表面の腐食皮膜の除去しつつ、酸化抑制剤にて過剰の腐食を制限しています。<br>
										また、グレージングの原因になるカルシウム成分と結合し、不溶化するリン酸に替わる酸が検討されています。</dd>
								</dl>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">乳化力（水分散力）の安定化</h5>
								<p class="img"><img src="/img/contents/img_lab_04.jpg" alt=""/></p>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">乳化力（水分散力）の安定化</h5>
								<div class="tbl-lab-01">
									<table>
										<tbody>
											<tr>
												<th></th>
												<th>パターン①</th>
												<th>パターン②</th>
												<th>パターン③</th>
											</tr>
											<tr>
												<th>版面水量少</th>
												<td>シャドウ部が絡む</td>
												<td>全面汚れ</td>
												<td>汚れにくい</td>
											</tr>
											<tr>
												<th>版面水量多</th>
												<td>インキ着肉、転移不良</td>
												<td>多少安定</td>
												<td>安定性大</td>
											</tr>
											<tr>
												<th>水幅<br>
													<span>（給湿液効率を<br>
													無視した場合）</span></th>
												<td><img src="/img/contents/img_lab_05.jpg" alt=""/></td>
												<td><img src="/img/contents/img_lab_06.jpg" alt=""/></td>
												<td><img src="/img/contents/img_lab_07.jpg" alt=""/></td>
											</tr>
										</tbody>
									</table>
								</div>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">乳化状態</h5>
								<p class="img"><img src="/img/contents/img_lab_09.jpg" alt=""/></p>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">インキ適性について</h5>
								<p class="img"><img src="/img/contents/img_lab_08.jpg" alt=""/></p>
							</section>
							<!--/section--> 
							
						</section>
						<!--/section--> 
						
						<!--section-->
						<section class="section-lab">
							<h4 class="title-03">2．湿し水の管理</h4>
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">湿し水の管理と印刷品質</h5>
								<ul class="list-01">
									<li>エッチ液および添加液の濃度管理が最も重要</li>
									<li>湿し水の冷却温度</li>
									<li>pH、導電率は湿し水交換の目安に</li>
									<li>原水管理（水道水、純水）</li>
								</ul>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">エッチ液と添加液の濃度管理</h5>
								<div class="tbl-lab-01">
									<table>
										<tbody>
											<tr>
												<th></th>
												<th>ノンアルコール印刷</th>
												<th>アルコール併用印刷</th>
											</tr>
											<tr>
												<th>含有量測定</th>
												<td class="ta-l">糖度計による数値換算</td>
												<td class="ta-l">エッチ液メーカーに分析依頼</td>
											</tr>
											<tr>
												<th>規定量未混入<br>
													によるトラブル</th>
												<td class="ta-l">添加量少<br>
													・地汚れ<br>
													<br>
													添加量多<br>
													・過乳化<br>
													・乾燥不良<br>
													・ブロッキング</td>
												<td class="ta-l">アルコール少<br>
													・地汚れ<br>
													<br>
													アルコール多<br>
													・素抜け<br>
													・ドライダウンによるインキ濃度低下</td>
											</tr>
										</tbody>
									</table>
								</div>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in ov-h">
								<h5 class="title-04">湿し水の冷却温度</h5>
								<p class="text-02">湿し水循環タンクの冷却調整を施し、水舟で約１０℃を維持することを推奨</p>
								<p class="text-05">１℃の温度差で水上がり（汲み上げ性）が変化します。</p>
								<div class="box-lab-in-02">
									<div class="box-lab-in-02-table f-l w-30">
										<div class="tbl-lab-01">
											<table>
												<tbody>
													<tr>
														<th>温度(℃)</th>
														<th>動粘度cSt（mm2/S）</th>
													</tr>
													<tr>
														<td>0</td>
														<td>1.792</td>
													</tr>
													<tr>
														<td>5</td>
														<td>1.520</td>
													</tr>
													<tr>
														<td>10</td>
														<td>1.307</td>
													</tr>
													<tr>
														<td>15</td>
														<td>1.139</td>
													</tr>
													<tr>
														<td>20</td>
														<td>1.004</td>
													</tr>
													<tr>
														<td>25</td>
														<td>0.893</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="box-lab-in-02-text f-l">
										<p class="text-02">１５℃以上になると水上がりが悪化し印刷品質（光沢感など）が低下します。<br>
											但し、水温の下げ過ぎによる結露発生に注意します。</p>
										<p class="text-06">（水温１０℃での湿し水粘度）<br>
											ＩＰＡ５％水溶液：１.７～１.８cSt<br>
											エッチ液２％水溶液：１.４～１.５cSt</p>
									</div>
								</div>
							</section>
							<!--/section--> 
							
						</section>
						<!--/section--> 
						
						<!--section-->
						<section class="section-lab">
							<h4 class="title-03">3．機材メンテナンスの重要性</h4>
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">機材メンテナンスの重要性</h5>
								<ul class="list-01">
									<li>適性な洗浄方法を見つけ出し、継続することで、インキローラーが本来持つ水幅を維持・確保する。</li>
									<li>水棒ローラーのゴム硬度の定期的な確認と保水処理により親水性を維持・確保する。</li>
									<li>ブランケットの洗浄残渣が印刷汚れの要因になるケースもあり洗浄確認が必要。</li>
								</ul>
							</section>
							<!--/section--> 
							
							<!--section-->
							<section class="section-lab-in">
								<h5 class="title-04">グレーズ（glaze）とは</h5>
								<p class="text-05">乾いたインキ、用紙のコーティング材、アラビアゴム、湿し水の薬品、そしてゴミなどの非常に細かい物質が積み重なってローラーの孔が詰まった状態。またはその膜。</p>
								<p class="img"><img src="/img/contents/img_lab_10.jpg" alt=""/></p>
							</section>
							<!--/section--> 
							
						</section>
						<!--/section--> 
						
						<!--section-->
						<section class="section-lab">
							<p class="ta-c"><a class="btn-01" href="/doc/D_RD_vol1.pdf" target="_blank"><span class="ico-pdf">PDFダウンロード</span></a></p>
						</section>
						<!--/section--> 
						
					</div>
					<!--/toggle-body--> 
					
				</section>
				<!--/section--> 
				
				<!--section-->
				<section class="section-03">
					<h3 class="toggle-head"><span>除電滑走剤について　vol.1</span></h3>
					
					<!--toggle-body-->
					<div class="toggle-body"> 
						
						<!--/section-lab-->
						<div class="section-lab"> 
							
							<!--section-->
							<section id="sec-01" class="section-01">
								<h3 class="title-01">静電気の発生と抑制について</h3>
								<p class="text-02">物体が擦れたり、剥離をした際に、発生した静電気が様々な障害を引き起こす事が知られています。帯電した静電気を抑制するには、電荷を逃がしたり、中和する方法が挙げられますが、化学的な抑制方法として、帯電防止剤の塗布があります。薬剤を塗布する事で、基材表面の親水基に出来た水膜内の自由電子などが、電荷を中和したり、漏洩することで帯電を低く抑えることが可能です。</p>
								<section class="section-02">
									<h4 class="title-03"><span>静電気の発生事例 </span></h4>
									<p class="img"><img src="/img/contents/antistatic_agent_01.jpg" class="img-s" alt=""/></p>
								</section>
								<section class="section-02">
									<h4 class="title-03"><span>帯電防止の原理 </span></h4>
									<p class="img"><img src="/img/contents/antistatic_agent_02.jpg" class="img-s" alt=""/></p>
								</section>
							</section>
							<!--section--> 
							
							<!--section-->
							<section id="sec-02" class="section-01">
								<h3 class="title-01"> 身近にある帯電防止剤</h3>
								<section class="section-02">
									<p class="img"><img src="/img/contents/antistatic_agent_03.jpg" class="img-s" alt=""/></p>
								</section>
								<section class="section-02">
									<p class="img"><img src="/img/contents/antistatic_agent_04.jpg" class="img-s" alt=""/></p>
								</section>
								<section class="section-02">
									<p class="img"><img src="/img/contents/antistatic_agent_05.jpg" class="img-s" alt=""/></p>
								</section>
								<section class="section-02">
									<h4 class="title-center"><span class="in">オフセット輪転機における帯電防止剤とは？</span></h4>
									<p class="text-02">オフセット輪転機の印刷では、大量の印刷を短時間で仕上げるため、高速回転で印刷機を稼動します。<br>
										印刷工程において、金属ローラーと紙の摩擦や、紙の剥離により、<strong class="c-01">静電気</strong>が発生するので、静電気対策が必要となります。帯電防止剤は、印刷工程で発生する静電気を低減する薬剤です。</p>
									<p class="img"><img src="/img/contents/antistatic_agent_06.jpg" class="img-s" alt=""/></p>
								</section>
							</section>
							<!--section--> 
							
							<!--section-->
							<section id="sec-03" class="section-01">
								<h3 class="title-01">当社製品の紹介　　　～除電滑走剤S-cubosシリーズ～</h3>
								<section class="section-02">
									<h4 class="title-02"><span>優れた除電性能 </span></h4>
									<ul class="list-01">
										<li>特殊配合技術により、業界最高水準の除電性能を追求した製品</li>
										<li>紙に対する液浸透性に優れ、効率よく有効成分を紙に移行するため、高い除電効果と印刷に最適な滑走性を発揮します。</li>
									</ul>
								</section>
								<section class="section-02 p-rel">
									<p class="img"><img src="/img/contents/antistatic_agent_07.jpg" class="img-s" alt=""/></p>
									<!--<div id="mv-abs-01">
						<p><a data-remodal-target="mv-modal-01"><img src="/img/contents/btn_mv_01.jpg" alt=""></a></p>
					</div>--> 
								</section>
								<section class="section-02">
									<h4 class="title-02 ind-01"><span>優印刷専門薬剤の特性 </span></h4>
									<section class="section-03">
										<h5 class="title-04">①　ローラー防汚性能</h5>
										<p class="img"><img src="/img/contents/antistatic_agent_08.jpg" class="img-s" alt=""/></p>
									</section>
									<section class="section-03">
										<h5 class="title-04">②　液安定性能</h5>
										<div id="btn-aa-wrap-01">
											<p class="img"><img src="/img/contents/antistatic_agent_09.jpg" class="img-s" alt=""/></p>
											<div id="btn-aa-01">
												<p><span class="ta-c">詳細はこちら</span></p>
												<p><a class="btn-02" href="/products/category05.php"><span class="ico-pdf">除電滑走剤　S-cubos 33N</span></a></p>
												<p><a class="btn-02" href="/products/category05.php"><span class="ico-pdf">除電滑走剤　S-cubos 55S</span></a></p>
											</div>
										</div>
									</section>
								</section>
							</section>
							<!--section--> 
							
						</div>
						<!--/section-lab--> 
						
					</div>
					<!--/toggle-body--> 
					
				</section>
				<!--/section--> 
				
				<!--section-->
				<section class="section-03">
					<h3 class="toggle-head"><span>インキローラー洗浄について　vol.1</span></h3>
					
					<!--toggle-body-->
					<div class="toggle-body"> 
						
						<!--/section-lab-->
						<div class="section-lab"> 
							
							<!--section-->
							<section id="sec-00" class="section-02">
								<p class="text-02">一般的に、洗浄とは不要な汚れを薬品などを使って除去することを言います。<br>
									洗浄の対称によって、洗浄方法が異なります。</p>
							</section>
							<!--section--> 
							
							<!--section-->
							<section id="sec-01" class="section-01">
								<h3 class="title-01">身近にある洗浄例</h3>
								<p class="text-02">普段の生活の中で、洗浄シーンに直面することはたくさんあります。<br>
									ここでは、身近な洗浄の例を示します。<br>
									洗浄は汚れを落とすだけではなく、基材への影響を考えることが重要です。</p>
								<p class="img"><img src="/img/contents/ink_roller_01.jpg" class="img-s" alt=""/></p>
								<div class="table-01">
									<table>
										<tbody>
											<tr>
												<th scope="col">基材（具体例）</th>
												<th scope="col">汚れ（代表例）</th>
												<th scope="col">考慮する影響</th>
											</tr>
											<tr>
												<td>車外装</td>
												<td>ホコリ、油膜</td>
												<td>塗装膜の侵食<br>
													ゴム基材の劣化</td>
											</tr>
											<tr>
												<td>ガラス</td>
												<td>水アカ、油膜</td>
												<td>水跡、洗剤残り<br>
													ワイパーゴムの劣化</td>
											</tr>
											<tr>
												<td>車内装</td>
												<td>皮脂</td>
												<td>樹脂の劣化<br>
													皮、布の劣化</td>
											</tr>
											<tr>
												<td>服（繊維）</td>
												<td>ホコリ、皮脂、調味料</td>
												<td>繊維の収縮<br>
													繊維中の汚れ</td>
											</tr>
										</tbody>
									</table>
								</div>
							</section>
							<!--section--> 
							
							<!--section-->
							<section id="sec-02" class="section-01">
								<h3 class="title-01">洗浄剤による洗浄メカニズム</h3>
								<section class="section-02">
									<p class="img"><img src="/img/contents/ink_roller_02.jpg" class="img-s" alt=""/></p>
								</section>
								<section class="section-02">
									<h4 class="title-02">オフセット印刷における洗浄</h4>
									<p class="text-02">オフセット印刷機は、多くのローラーが配置され、そのローラーは、インキ、紙、湿し水など多くの資材と接触するので、洗浄は大切な工程となります。</p>
									<p class="img"><img src="/img/contents/ink_roller_03.jpg" class="img-s" alt=""/></p>
								</section>
							</section>
							<!--section--> 
							
							<!--section-->
							<section id="sec-03" class="section-01">
								<h3 class="title-01">製品ご紹介（油性インキ対応）</h3>
								<p class="text-02">当社のオフセット印刷機
									インキローラー洗浄液シリーズは “汚れ”性状に応じ有効的な洗浄液を設計し、展開しております。<br>
									また、使用する有機溶剤の環境負荷の低減を提案します。</p>
								<p class="img"><img src="/img/contents/ink_roller_04.jpg" class="img-s" alt=""/></p>
								<section class="section-02">
									<h4 class="title-02">1.　油性インキ用標準品</h4>
									<div class="sec-ir">
										<div class="left">
											<h5><img src="../img/contents/ink_roller_05.jpg" class="img-" alt=""/></h5>
											<p class="text-02">手洗浄や自動洗浄タンクに充填する場合に使用する。<br>
												主に有機溶剤を用い、インキを溶解して洗浄する。</p>
										</div>
										<div class="right">
											<p class="tit-right">有機則非該当で安心、安全の洗浄液！！</p>
											<p><img src="../img/contents/ink_roller_08.jpg" class="img-" alt=""/></p>
											<p class="text-">詳細はこちら</p>
											<p><a href="/products/category03.php#sec-06" class="btn-02">ロールクリンＫＲ－１</a></p>
										</div>
									</div>
								</section>
								<section class="section-02">
									<h4 class="title-02">2.　強力洗浄タイプ</h4>
									<div class="sec-ir">
										<div class="left">
											<h5><img src="../img/contents/ink_roller_06.jpg" alt=""/></h5>
											<p><img src="../img/contents/ink_roller_07.jpg" alt=""/></p>
											<p class="text-02">水馴染みの良い洗浄液にすることで、洗浄液残りを削減。<br>
												溶剤使用量削減、特色適応性良好、紙粉対応</p>
										</div>
										<div class="right">
											<p class="tit-right">汚れを抱えた洗浄剤を水で流す！</p>
											<p><img src="../img/contents/ink_roller_09.jpg" alt=""/></p>
											<p class="text-">詳細はこちら</p>
											<p><a href="/products/category03.php#sec-06" class="btn-02">セーフティー ロールクリンSR-1</a></p>
										</div>
									</div>
								</section>
								<section class="section-02">
									<h4 class="title-02">３.　洗浄補助剤</h4>
									<div class="sec-ir">
										<div class="left">
											<h5><img src="../img/contents/ink_roller_10.jpg" alt=""/></h5>
											<p><img src="../img/contents/ink_roller_11.jpg" alt=""/></p>
											<p class="text-02">洗浄液だけでは落としきれなかった汚れが付着している場合、次のインキに取り込まれ、色が濁る。<br>
												<br>
												洗浄液で溶解しきれなかった細かな汚れを、粘度のある洗浄剤や、コンパウンド等の物理的作用で洗浄。</p>
										</div>
										<div class="right">
											<p class="tit-right">色替え洗浄に抜群の効果を発揮！</p>
											<p><img src="../img/contents/ink_roller_12.jpg" alt=""/></p>
											<p class="text-">詳細はこちら</p>
											<p><a href="/products/category03.php#sec-08" class="btn-02">クリーンチェンジャー　CC-1</a></p>
										</div>
									</div>
								</section>
								<section class="section-02">
									<h4 class="title-02">４.　紙粉洗浄と汚れのチェック</h4>
									<div class="sec-ir">
										<div class="left">
											<h5><img src="../img/contents/ink_roller_13.jpg" alt=""/></h5>
											<p><img src="../img/contents/ink_roller_14.jpg" alt=""/></p>
											<p class="text-02">紙粉が付着した状態では、紙面に対するインキの転移性が悪くなったり、絵柄が欠けたりする。<br>
												紙粉は有機溶剤に溶けないため、水系成分を含んだエマルション洗浄液で洗浄する。<br>
												<br>
												また、ローラーの汚れ残りが確認しやすい白色設計</p>
										</div>
										<div class="right">
											<p class="tit-right">一般洗浄後の仕上げ洗浄にこの一本！！</p>
											<p><img src="../img/contents/ink_roller_15.jpg" alt=""/></p>
											<p class="text-">詳細はこちら</p>
											<p><a href="/products/category03.php#sec-06" class="btn-02">セーフティー ローラークリーナーRC-10</a></p>
										</div>
									</div>
								</section>
								<section class="section-02">
									<h4 class="title-02">5.　グレーズ洗浄</h4>
									<div class="sec-ir">
										<div class="left">
											<h5><img src="../img/contents/ink_roller_16.jpg" alt=""/></h5>
											<p><img src="../img/contents/ink_roller_17.jpg" alt=""/></p>
											<p class="text-02">グレーズは、ミネラル分が固着して硬い結晶となっており、有機溶剤には溶解しない。<br>
												粘度が高く、コンパウンド等を含んだ物理的作用をもつ薬品で洗浄。</p>
										</div>
										<div class="right">
											<p class="tit-right">インキローラーのグレーズを強力に除去！！</p>
											<p><img src="../img/contents/ink_roller_18.jpg" alt=""/></p>
											<p class="text-">詳細はこちら</p>
											<p class="ind-01"><a href="/products/category03.php#sec-08" class="btn-02">サン・ソイクリーナー SY-1</a></p>
											<p><a href="/products/category03.php#sec-08" class="btn-02">サン・ソイクリーナー SY-3</a></p>
										</div>
									</div>
								</section>
							</section>
							<!--section--> 
							
						</div>
						<!--/section-lab--> 
						
					</div>
					<!--/toggle-body--> 
					
				</section>
				<!--/section--> 
				
				<!--section-->
				<section class="section-03">
					<h3 class="toggle-head"><span>水棒洗浄液について　vol.1</span></h3>
					
					<!--toggle-body-->
					<div class="toggle-body"> 
						
						<!--/section-lab-->
						<div class="section-lab"> 
							
							<!--section-->
							<section id="sec-01" class="section-01">
								<h3 class="title-01">水棒洗浄液とは？</h3>
								<p class="text-02">オフセット印刷機には、多くのローラーが配置されています。各ローラーにはそれぞれ機能があり、その性能を満たすために、ローラーの洗浄工程は重要な役割を担っています。なかでも、連続給水方式の湿し水装置における水棒ローラー（ゴムローラー/クロムローラー）は、湿し水を安定した水膜で版面に供給する、印刷性能に関わる重要な役割を担っています。</p>
								<p class="text-02">水棒洗浄液は、ローラー上のインキを洗浄し、ローラーに残った油分を取り除くことで、水上がりの安定性を保持させる薬剤です。</p>
								<p class="img"><img src="/img/contents/water_wash_01.jpg" class="img-s" alt=""/></p>
							</section>
							<!--/section--> 
							
							
						</div>
						<!--/section-lab--> 
						
					</div>
					<!--/toggle-body--> 
					
				</section>
				<!--/section--> 
				
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->

</body>
</html>
