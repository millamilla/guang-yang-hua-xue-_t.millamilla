<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>光陽化学工業</title>

<!-- inc -->
<?php include("inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("inc/header_c.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--movie-->
		<section id="movie-index">
			<div class="in">
			<!--	<h2><img src="/img/index/copy_index.png" alt="研究・開発型企業として、歩み続けた先駆者の軌跡"/></h2>-->
				<p class="scroll"><a href="#item-index"><span></span><span></span><span></span></a></p>
					
				<video src="/img/index/mv_index181029.mp4" autoplay muted playsinline>
					<p>動画を再生するにはvideoタグをサポートしたブラウザが必要です。</p>
				</video>
			</div>
		</section>
		<!--/movie--> 
		
		<!--item-index-->
		<!--<section id="item-index">
			<h2 class="title-info"><span class="in"><span class="">新製品情報</span></span></h2>
			<ul class="list-item">
				<li class="single"><a href="/newitem/item.php#ni1807-01" class="no-">
					<p class="img"><img src="/img/common/ico_bs_01.jpg" alt=""/></p>
					<p class="text">SOLAIA WEB<br>WK711<br>クリア</p>
					</a></li>
				<li class="single"><a href="/newitem/item.php#ni1807-02" class="no-">
					<p class="img"><img src="/img/common/ico_bs_01.jpg" alt=""/></p>
					<p class="text">SOLAIA<br>
					 517<br>&nbsp;</p>
					</a></li>
				
			</ul>
		</section>-->
		<!--/item-index--> 
		
		<!--info-index-->
		<section id="info-index">
			<h2 class="title-info"><span class="in"><span class="eng">What's New</span></span></h2>
			<ul class="list-info">

				<li class="single">
					<p class="ico"><span>新着</span></p>
					<p class="date">2019.10.21</p>
					<p class="text"><a href="newitem/">新製品「セーフティーUVインキ洗浄液SV-1」「セーフティーダンプキーパーPK-25EX」を販売しました。</a></p>
				</li>

				<li class="single">
					<p class="ico"><span>新着</span></p>
					<p class="date">2019.10.21</p>
					<p class="text">新製品「ネオフイルムクリーナー NF-55」を販売しました。</p>
				</li>
		
				<li class="single">
					<p class="ico"><span>新着</span></p>
					<p class="date">2019.10.21</p>
					<p class="text"><a href="doc/whatsnew_191021_2.pdf" target="_blank">「SOLAIA WEB WK801、SOLAIA 517」製品ラベルの変更および<br>「ﾆｭｰAPｸﾘｰﾅｰ、ﾆｭｰAPﾌﾟﾛﾃｸﾀｰ」外箱ケース変更のご案内</a></p>
				</li>
				
				<li class="single">
					<p class="ico"><span>新着</span></p>
					<p class="date">2019.10.21</p>
					<p class="text"><a href="doc/whatsnew_191021_1.pdf" target="_blank">水棒関連品、インキローラー関連品、製版関連品の販売中止について</a></p>
				</li>
				
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2019.07.26</p>
					<p class="text"><a href="doc/whatsnew_190726.pdf" target="_blank">8月夏季休暇期間前後のデリバリーについてのご案内</a></p>
				</li>
			
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2019.06.12</p>
					<p class="text"><a href="doc/whatsnew_190611.pdf" target="_blank">大阪サミット開催に伴うデリバリーについてのご案内</a></p>
				</li>

				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2019.04.23</p>
					<p class="text"><a href="doc/whatsnew_190416.pdf" target="_blank">製版関連品、機器関連品の販売中止について</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2019.03.01</p>
					<p class="text">2018年9月開始のサンプル提供キャンペーンは終了しました。</p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.10.29</p>
					<p class="text"><a href="/newitem/">New Itemページを更新しました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.10.29</p>
					<p class="text"><a href="/search/">Focus Products ページをアップしました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.09.12</p>
					<p class="text"><a href="/newitem/">New Itemページを更新しました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.09.03</p>
					<p class="text">新製品のサンプルはＨＰから申込みができるようになりました。</p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.09.03</p>
					<p class="text"><a href="/newitem/">新製品「SOLAIA WEB WK711クリア」「SOLAIA 517」を発売しました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.07.24</p>
					<p class="text"><a href="/lab/index.php#sec-02">技術レポートをアップしました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.07.24</p>
					<p class="text"><a href="/newitem/">新製品ページを更新しました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.07.17</p>
					<p class="text"><a href="/business/">事業内容ページを更新しました。</a></p>
				</li>
				<li class="single">
					<p class="ico">&nbsp;</p>
					<p class="date">2018.04.16</p>
					<p class="text"><a href="">HPリニューアルしました。</a></p>
				</li>
			</ul>
		</section>
		<!--/info-index--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("inc/script.php"); ?>
<!-- /inc -->

</body>
</html>
