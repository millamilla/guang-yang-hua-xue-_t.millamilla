<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>各種工業用ケミカル | 光陽化学工業</title>

<!-- inc -->
<?php include("../../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span>各種工業用ケミカル</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/business/" itemprop="url">事業内容</a></span>  &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/business/industrial" itemprop="url">各種工業用ケミカル</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<ul class="nav-business nav-business__industrial">
					
					<li class="single"><!--<a href="/business/daily.php" class="no-">--><div class="no- virtual-a">
						<p class="img"><img src="/img/common/ico_daily.png" alt=""/></p>
						<p class="text">日用品関連事業</p>
						</div><!--</a>--></li>
					<li class="single"><!--<a href="/business/electric.php" class="no-">--><div class="no- virtual-a">
						<p class="img"><img src="/img/common/ico_electric.png" alt=""/></p>
						<p class="text">電子材料関連薬品事業</p>
						</div><!--</a>--></li>
					<li class="single"><!--<a href="/business/medical.php" class="no-">--><div class="no- virtual-a">
						<p class="img"><img src="/img/common/ico_medical.png" alt=""/></p>
						<p class="text">医療機器薬剤事業</p>
						</div><!--</a>--></li>
				</ul>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../../inc/script.php"); ?>
<!-- /inc -->

</body>
</html>
