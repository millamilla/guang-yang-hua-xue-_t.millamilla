<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>プリンティング事業用 ケミカル | 事業内容 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span>事業内容</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/business/" itemprop="url">事業内容</a></span> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/business/printing.php" itemprop="url">プリンティング事業用 ケミカル</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<h2 class="title-center"><span class="in">プリンティング事業用 ケミカル</span></h2>
				<p class="img ta-c"><img src="/img/contents/main_printing.jpg" class="mw-1000" alt=""/></p>
				<p class="text-01 ta-l">「界面科学」をコアとした配合設計技術や素材応用技術を応用し、オフセット輪転印刷機／枚葉印刷機に必要なケミカル製品全般を提供しています。</p>
				<p class="text-01 ta-l">特に環境対策製品に着眼し、各種環境規制に関わる法令に適合した製品はもとより、性能と品質の向上、薬液の使用量削減、ノンVOC化、作業環境の改善、地球温暖化防止、資源の循環型利用、廃棄物の削減など、様々な観点から製品を提供し続けています。なかでも、汚れを落とす強力洗浄剤から、汚れないように予防を目的とする汚れ防止剤やソフト洗浄剤のように、事後対策製品／ビジネスから、事前予防製品／ビジネスを推進することにより、安全と安心を提供し産業の発展に貢献しています。更に、各種メーカーとの共創による次世代製品の開発に注力し、人と環境に優しい製品創りに日夜挑戦し続けています。</p>
				<p class="ta-c"><a href="/products/" class="btn-02"><span>製品一覧を見る</span></a></p>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc --> 
<script>
$(function() {
 
	$('.cmap area').hover(function() {
		
		var chBg = $(this).attr("name");
		$("#"+chBg+" img").attr('src', $("#"+chBg+" img").attr('src').replace('_off', '_on'));

		
		
	}, function() {
		var chBg = $(this).attr("name");
		$("#"+chBg+" img").attr('src', $("#"+chBg+" img").attr('src').replace('_on', '_off'));

	
	});
	
});
</script>
</body>
</html>
