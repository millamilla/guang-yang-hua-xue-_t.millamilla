<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>医療機器薬剤事業 | 事業内容 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span>事業内容</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/business/" itemprop="url">事業内容</a></span> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/business/medical.php" itemprop="url">医療機器薬剤事業</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<h2 class="title-center"><span class="in">医療機器薬剤事業</span></h2>
				<p class="img ta-c"><img src="/img/contents/main_medical.jpg" class="mw-1000" alt=""/></p>
				<p class="text-01 ta-l">「界面科学」を活かした配合技術と、スラリーやペーストなどの生産技術、光学と感光性材料の応用技術を医療分野に適応させ、各種ＯＥＭやＰＢ製品を提供しています。
					実例として、医療用器具洗浄液や電極用導電性ペースト、放射線インジケーター、歯科X線フイルム用現像液などがあります。 </p>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->

</body>
</html>
