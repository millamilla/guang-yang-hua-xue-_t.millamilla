<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>電子材料関連薬品事業 | 事業内容 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span>事業内容</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/business/" itemprop="url">事業内容</a></span> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/business/electric.php" itemprop="url">電子材料関連薬品事業</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<h2 class="title-center"><span class="in">電子材料関連薬品事業</span></h2>
				<p class="img ta-c"><img src="/img/contents/main_electric.jpg" class="mw-1000" alt=""/></p>
				<p class="text-01 ta-l">当社基盤技術の一つである感光剤技術（オフセット印刷版用のポジ型およびネガ型感光剤）を光学技術と融合し、電子材料分野に応用展開することで、各種ＯＥＭやＰＢ製品を提供しています。
					実例として、半導体パッケージやFPCをはじめ、電子基板の配線形成用ドライフィルム向けの感光剤などがあります。 </p>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->

</body>
</html>
