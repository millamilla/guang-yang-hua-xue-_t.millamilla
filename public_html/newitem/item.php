<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>New Item（プリンティング事業） | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span class="eng">New Item（プリンティング事業）</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/newitem/item.php" itemprop="url">New Item（プリンティング事業）</a></span></div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section id="sec-01" class="section-01"> 
				
				<!--section-->
				<!--<section class="item-cell-wrap" id="new_item01"> -->
					
					<!--title-->
					<!--<h2 class="item-title">オフセット輪転機用エッチ液</h2>-->
					<!--title--> 
					
					<!--section-->
					<!--<section id="ni1807-01" class="item-single">
						<h3 class="item-title-single eng">SOLAIA WEB WK711 クリア</h3>
						<p class="copy">ロングラン印刷でも湿し水が汚れにくく常にクリアだから清潔で安定した品質を維持！！</p>
						<div class="item-main">
							<div class="item-img">
								<p><img src="/img/newitem/item_1807_01.jpg" alt=""/></p>
							</div>
							<div class="item-info">
								<dl>
									<dt><span>製品特長</span></dt>
									<dd>
										<ul class="list-02">
											<li>（1）湿し水が常に透明で清潔です。</li>
											<li>（2）特殊配合技術により、汚れにくい安定した湿し水環境を長期に維持します。</li>
											<li>（3）湿し水が汚れやすいロングラン印刷では、特に優れた効果を発揮します。</li>
											<li>（4）湿し水タンクを清掃する負担が軽減されます。 </li>
											<li>（5）高い給湿液効率で、高速印刷に最適です。</li>
										</ul>
									</dd>
								</dl>
								<dl>
									<dt><span>使用方法</span></dt>
									<dd>・本エッチ液を１.５～３.５％に希釈してご使用ください。</dd>
									<dd>・本エッチ液は定量添加補充にてご使用ください。</dd>
								</dl>
							</div>
						</div>
						<div class="item-sub">
							<div class="item-table">
								<table>
									<tbody>
										<tr>
											<th rowspan="3"><p>適用法令</p></th>
											<td>安衛法（有機則）　</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<td>PRTR法</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<td>消防法（危険物）</td>
											<td><p>危険物　第4類第3石油類　水溶性液体</p></td>
										</tr>
										<tr>
											<th rowspan="2"><p>製品規格</p></th>
											<td>容量</td>
											<td><p>20L</p></td>
										</tr>
										<tr>
											<td>包装形態</td>
											<td><p>ポリ容器 </p></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="item-doc">
								<ul class="list-doc">
									<li><a class="btn-01" href="/doc/cat_WK711_CL.PDF" target="_blank"><span class="ico-pdf">製品カタログ</span></a></li>
									<li><a class="btn-01" href="/doc/SDS_711CL.pdf" target="_blank"><span class="ico-pdf">SDS(改訂日2018年07月17日)</span></a></li>
								</ul>
							</div>
						</div>
						<!-- inc -->
						
						<!-- /inc -->
						<!--<section class="section-item">
							<h4 class="title-01b"><span class="in">紹介資料</span></h4>
							<p class="title-02">■ 印刷時における湿し水の様子（湿し水タンクを上から観察）</p>
							<p><img src="/img/contents/item_document_01.jpg" alt=""/></p>
						</section>
					</section>-->
					<!--section--> 
					
				<!--</section>-->
				
				
				
				<!--section-->
				<section class="item-cell-wrap" id="new_item02"> 
					
					<!--title-->
					<h2 class="item-title">オフセット枚葉機用エッチ液</h2>
					<!--title--> 
					
					<!--section-->
					<section id="ni1807-02" class="item-single">
						<h3 class="item-title-single eng">SOLAIA 517</h3>
						<p class="copy">高性能高溶剤型エッチ液の「非危険物化」を実現！！<br>
							消防法対策に最適！！</p>
						<div class="item-main">
							<div class="item-img">
								<p><img src="/img/newitem/item_1807_02.jpg" alt=""/></p>
							</div>
							<div class="item-info">
								<dl>
									<dt><span>製品特長</span></dt>
									<dd>
										<ul class="list-02">
											<li>（1）高溶剤型エッチ液では実現が難しかった非危険物化を実現した枚葉機用エッチ液です。</li>
											<li>（2）優れた給湿液効率を発揮し、ノンアルコール化が可能です。</li>
											<li>（3）油性/UVインキに対応した兼用エッチ液です。</li>
											<li>（4）独自の特殊乳化制御技術により、安定した高品質印刷を実現します。</li>
											<li>（5）安衛法（有機則）、消防法が非該当の法規制対策製品です。</li>
										</ul>
									</dd>
								</dl>
								<dl>
									<dt><span>使用方法</span></dt>
									<dd>・本エッチ液を１.５～３.５％に希釈してご使用ください。</dd>
									<dd>・本エッチ液は定量添加補充にてご使用ください。</dd>
								</dl>
							</div>
						</div>
						<div class="item-sub">
							<div class="item-table">
								<table>
									<tbody>
										<tr>
											<th rowspan="3"><p>適用法令</p></th>
											<td>安衛法（有機則）　</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<td>PRTR法</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<td>消防法（危険物）</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<th rowspan="2"><p>製品規格</p></th>
											<td>容量</td>
											<td><p>10L、20L</p></td>
										</tr>
										<tr>
											<td>包装形態</td>
											<td><p>ポリ容器 </p></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="item-doc">
								<ul class="list-doc">
									<li><a class="btn-01" href="/doc/cat_517.pdf" target="_blank"><span class="ico-pdf">製品カタログ</span></a></li>
									<li><a class="btn-01" href="/doc/SDS_517.pdf" target="_blank"><span class="ico-pdf">SDS(改訂日2018年07月17日)</span></a></li>
								</ul>
							</div>
						</div>
						<!-- inc -->
						<?php include("../inc/arrow.php"); ?>
						<!-- /inc -->
						
						<section class="section-item">
							<h4 class="title-01b"><span class="in">紹介資料</span></h4>
							<p class="title-02">■ 湿し水使用環境における給湿液効率</p>
							<p class="txt-01">（湿し水濃度：2%、湿し水水温：10℃）</p>
							<p><img src="/img/contents/item_document_03.jpg" alt=""/></p>
						</section>
					</section>
					<!--section--> 
					
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="item-cell-wrap" id="new_item06"> 
					
					<!--section-->
					<section id="ni1807-01" class="item-single">
						<h3 class="item-title-single eng">SOLAIA WEB WK801</h3>
						<p class="copy">グレーズ、パイリング対策にも効果的！<br>
							次世代型　多機能エッチ液です。</p>
						<div class="item-main">
							<div class="item-img">
								<p><img src="/img/newitem/item_1809_01.jpg" alt=""/></p>
							</div>
							<div class="item-info">
								<dl>
									<dt><span>製品特長</span></dt>
									<dd>
										<ul class="list-02">
											<li>（1）高い網点再現性を発揮し、高品質印刷を実現</li>
											<li>（2）印刷の高速化に対応し、生産性向上に寄与</li>
											<li>（3）グレーズ、パイリングの抑制に優れた効果を発揮</li>
											<li>（4）消防法非該当の法規制対策品</li>
										</ul>
									</dd>
								</dl>
								<dl>
									<dt><span>使用方法</span></dt>
									<dd>・本エッチ液を１.５～３.５％に希釈してご使用ください。</dd>
									<dd>・本エッチ液は定量添加補充にてご使用ください。</dd>
								</dl>
							</div>
						</div>
						<div class="item-sub">
							<div class="item-table">
								<table>
									<tbody>
										<tr>
											<th rowspan="3"><p>適用法令</p></th>
											<td>安衛法（有機則）　</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<td>PRTR法</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<td>消防法</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<th rowspan="2"><p>製品規格</p></th>
											<td>容量</td>
											<td><p>20L</p></td>
										</tr>
										<tr>
											<td>包装形態</td>
											<td><p>ポリ容器 </p></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="item-doc">
								<ul class="list-doc">
									<li><a class="btn-01" href="/doc/WK801_pdf-1.pdf" target="_blank"><span class="ico-pdf">製品カタログ</span></a></li>
									<li><a class="btn-01" href="/doc/02_SOLAIA_WEB_WK_801.pdf" target="_blank"><span class="ico-pdf">SDS(改訂日2018年01月16日)</span></a></li>
								</ul>
							</div>
						</div>
						<!-- inc -->
						<?php include("../inc/arrow.php"); ?>
						<!-- /inc -->
						
						<section class="section-item">
							<h4 class="title-01b"><span class="in">紹介資料</span></h4>
							<p class="title-02">■ WK801により、グレーズやパイリングを抑制する理由</p>
							<p class="txt-01">ゴム素材の汚れは「パイリング」、金属素材の汚れは「グレーズ」を引き起こします。</p>
							<p><img src="/img/contents/item_document_02.gif" width="100%" alt=""/></p>
						</section>
					</section>
					<!--section--> 
					
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="item-cell-wrap" id="new_item07"> 
					
					<!--section-->
					<section id="ni1807-01" class="item-single">
						<h3 class="item-title-single eng">セーフティーUVインキ洗浄液　SV-1</h3>
						<p class="copy">消防法非該当 洗浄液の次世代品！！<br>天然由来成分を配合し、低VOCおよび低臭気を実現！！</p>
						<div class="item-main">
							<div class="item-img">
								<p><img src="/img/newitem/item_1910_02.jpg" alt=""/></p>
							</div>
							<div class="item-info">
								<dl>
									<dt><span>製品特長</span></dt>
									<dd>
										<ul class="list-02">
											<li>（1）消防法非該当かつ低VOCの環境対策製品</li>
											<li>（2）天然由来成分を配合し、現場の作業環境を配慮した低臭気化を実現。</li>
										</ul>
									</dd>
								</dl>
								<dl>
									<dt><span>使用方法</span></dt>
									<dd>・従来ご使用のインキ洗浄液と同様の使用方法でご使用ください。</dd>
									<dd>・ご使用の際は、他の洗浄液と混合しないでください。</dd>
								</dl>
							</div>
						</div>
						<div class="item-sub">
							<div class="item-table">
								<table>
									<tbody>
										<tr>
											<th rowspan="3"><p>適用法令</p></th>
											<td>安衛法（有機則）　</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<td>PRTR法</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<td>消防法</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<th rowspan="2"><p>製品規格</p></th>
											<td>容量</td>
											<td><p>16kg</p></td>
										</tr>
										<tr>
											<td>包装形態</td>
											<td><p>斗缶 </p></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="item-doc">
								<ul class="list-doc">
									<li><a class="btn-01" href="/doc/cata_sv-1.pdf" target="_blank"><span class="ico-pdf">製品カタログ</span></a></li>
									<li><a class="btn-01" href="/doc/sds_sv-1.pdf" target="_blank"><span class="ico-pdf">SDS(作成日2019年09月4日)</span></a></li>
								</ul>
							</div>
						</div>
						<!-- inc -->
						<?php include("../inc/arrow.php"); ?>
						<!-- /inc -->
						
						<section class="section-item">
							<h4 class="title-01b"><span class="in">紹介資料</span></h4>
							<p class="title-02">～Coming Soon～</p>
						</section>
					</section>
					<!--section--> 
					
				</section>
				<!--section-->
				
				<!--section-->
				<section class="item-cell-wrap" id="new_item08"> 
					
					<!--section-->
					<section id="ni1807-01" class="item-single">
						<h3 class="item-title-single eng">セーフティーダンプキーパー PK-25EX</h3>
						<p class="copy">消防法非該当のオールラウンダー製品！！<br>インキ溶解性、作業性、親水性を兼ね備える水棒洗浄液。</p>
						<div class="item-main">
							<div class="item-img">
								<p><img src="/img/newitem/item_1910_01.jpg" alt=""/></p>
							</div>
							<div class="item-info">
								<dl>
									<dt><span>製品特長</span></dt>
									<dd>
										<ul class="list-02">
											<li>（1）連続給水ローラーの綺麗を維持し高品質かつ安定した印刷を提供します</li>
											<li>（2）低臭気かつ、快適な拭き取り心地により現場の作業性を向上させます</li>
										</ul>
									</dd>
								</dl>
								<dl>
									<dt><span>使用方法</span></dt>
									<dd>・本製品をウエスまたは、スポンジに適量含ませ給水ローラー表面の汚れを拭き上げてください。</dd>
									<dd>・洗浄後、液が残らないよう拭き上げてください。仕上げにローラーを回転させて水になじませてください。</dd>
								</dl>
							</div>
						</div>
						<div class="item-sub">
							<div class="item-table">
								<table>
									<tbody>
										<tr>
											<th rowspan="3"><p>適用法令</p></th>
											<td>安衛法（有機則）　</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<td>PRTR法</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<td>消防法</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<th rowspan="4"><p>製品規格</p></th>
											<td>容量</td>
											<td><p>1L</p></td>
										</tr>
										<tr>
											<td>梱包単位</td>
											<td><p>1ケース10本入</p></td>
										</tr>
										<tr>
											<td>容量</td>
											<td><p>18L</p></td>
										</tr>
										<tr>
											<td>包装形態</td>
											<td><p>斗缶</p></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="item-doc">
								<ul class="list-doc">
									<li><a class="btn-01" href="/doc/cata_pk-25ex.pdf" target="_blank"><span class="ico-pdf">製品カタログ</span></a></li>
									<li><a class="btn-01" href="/doc/sds_pk-25ex.pdf" target="_blank"><span class="ico-pdf">SDS(改訂日2019年09月18日)</span></a></li>
								</ul>
							</div>
						</div>
						<!-- inc -->
						<?php include("../inc/arrow.php"); ?>
						<!-- /inc -->
						
						<section class="section-item">
							<h4 class="title-01b"><span class="in">紹介資料</span></h4>
							<p class="title-02">～Coming Soon～</p>
						</section>
					</section>
					<!--section--> 
					
				</section>
				<!--section-->
				
				<!--section-->
				<section class="item-cell-wrap" id="new_item05"> 
					
					<!--title-->
					<h2 class="item-title">オフ輪印刷 後加工用 帯電防止液シリーズ</h2>
					<!--title--> 
					
					<!--section-->
					<section id="ni1823-03" class="item-single">
						<h3 class="item-title-single eng">除電滑走剤　S-cubos 55S</h3>
						<p class="copy">特殊配合技術により、高次元の除電滑走性を追求</p>
						<div class="item-main">
							<div class="item-img">
								<p><img src="/img/newitem/item_1823_03.jpg" alt=""/></p>
							</div>
							<div class="item-info">
								<dl>
									<dt><span>製品特長</span></dt>
									<dd>
										<ul class="list-02">
											<li>（1）スタッカーバンドラー、B4シート出し等に最適な滑走性を発揮</li>
											<li>（2）特殊配合技術による高い除電効果</li>
											<li>（3）高速印刷、高斤量の印刷紙にも適応</li>
										</ul>
									</dd>
								</dl>
								<dl>
									<dt><span>使用方法</span></dt>
									<dd>・薬液を5～20%に希釈してご使用ください。</dd>
									<dd>・紙質、印刷スピードに応じて、希釈量と塗布量を調整してください。</dd>
								</dl>
							</div>
						</div>
						<div class="item-sub">
							<div class="item-table">
								<table>
									<tbody>
										<tr>
											<th rowspan="3"><p>適用法令</p></th>
											<td>安衛法（有機則）　</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<td>PRTR法</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<td>消防法（危険物）</td>
											<td><p>非該当</p></td>
										</tr>
										<tr>
											<th rowspan="2"><p>製品規格</p></th>
											<td>容量</td>
											<td><p>17kg</p></td>
										</tr>
										<tr>
											<td>包装形態</td>
											<td><p>複合容器 </p></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="item-doc">
								<ul class="list-doc">
									<li><a class="btn-01" href="/doc/cat_55S_3.pdf" target="_blank"><span class="ico-pdf">製品カタログ</span></a></li>
									<li><a class="btn-01" href="/doc/SDS_S_cubos_55_S.pdf" target="_blank"><span class="ico-pdf">SDS(改訂日2017年10月10日)</span></a></li>
								<li><a class="btn-02" href="/lab/index.php#sec-02"><span class="ico-">技術情報はこちら</span></a></li>
								</ul>
							</div>
						</div>
						<!-- inc -->
						<?php include("../inc/arrow.php"); ?>
						<!-- /inc -->
						<section class="section-item">
							<h4 class="title-01b"><span class="in">紹介資料</span></h4>
							<p class="title-02">■ S-cubos 55S がもつ汚れ予防機能について</p>
							<p class="txt-01">高次元の除電性能と紙揃えに最適な滑走性を兼ね備えることはもちろん、<br>
								シリコンタンク／水舟内の薬液や、塗布ローラーを汚さずにクリーンに維持出来るので、<br>
								印刷後加工における2次的な印刷品質の低下を予防します。</p>
							<p><img src="/img/contents/item_document_06.jpg" alt=""/></p>
						</section>
					</section>
					<!--section--> 
					
				</section>
				<!--section--> 
				
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->

</body>
</html>
