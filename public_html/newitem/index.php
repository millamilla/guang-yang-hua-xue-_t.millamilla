<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>New Item （プリンティング事業） | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">
<div id="header-simple">
  <h1><a href="/"><img src="/img/common/logo_simple.png" alt=""/></a></h1>
  <div class="wrap-campaign"> 
    <p class="btn-campaign"><a href="/campaign/"><img src="/img/contents/bnr_head.jpg" alt="NEW ITEM  サンプル提供キャンペーン"/></a></p>
  </div>
</div>

<!--pagetitle-->
<div style="padding-top: 70px;">
  <div id="pagetitle" class="bg-business new-items">
    <div class="in">
      <h2 class="for_position">
        <span class="eng">New Item （プリンティング事業）</span>
        <span class="nav-search2_tex"> 平成より準備を整え<br>令和元年からスタートした「消防法非該当製品」</span>
      </h2>
    </div>
  </div>
</div>
<!--/pagetitle--> 

<!--full-->
<div id="wrap-full">
  <div class="wrap-half">
    <div class="item-pickup-wrap bg180702" id="new_item02"> <a href="/newitem/item.php#new_item02">
      <div class="item-half">
        <div class="in">
          <p class="item-copy">オフセット枚葉機用エッチ液</span></p>
          <p class="item-title"><span>SOLAIA 517</p>
          <p class="item-txt">消防法非該当且つ高性能品</p>
        </div>
      </div>
      </a> </div>
    <div class="item-pickup-wrap bg180901" id="new_item06"> <a href="/newitem/item.php#new_item06">
      <div class="item-half">
        <div class="in">
          <p class="item-copy">オフセット輪転機用エッチ液</span></p>
          <p class="item-title"><span>SOLAIA WEB WK801</p>
          <p class="item-txt">消防法非該当且つグレーズ、パイリングを抑制</p>
        </div>
      </div>
      </a> </div>
  </div>
  <div class="wrap-half">
    <div class="item-pickup-wrap bg180701" id="new_item01"> <a href="/newitem/item.php#new_item07">
      <div class="item-half">
        <div class="in">
          <p class="item-copy">オフセット印刷機向け</p>
          <p class="item-title">UV インキローラー洗浄剤<span><br>セーフティーUVインキ洗浄液SV-1</span></p>
          <p class="item-txt">消防法非該当の次世代型洗浄液</p>
        </div>
      </div>
      </a> </div>
    <div class="item-pickup-wrap bg180806 bg01" id="new_item03"> <a href="/newitem/item.php#new_item08">
      <div class="item-half">
        <div class="in">
          <p class="item-copy">オフセット印刷機向け </span></p>
          <p class="item-title">連続給水用ローラー洗浄剤<span><br>セーフティーダンプキーパー PK-25EX</p>
          <p class="item-txt">消防法非該当のオールラウンダーな水棒洗浄剤</p>
        </div>
      </div>
      </a> </div>
  </div>
  <div class="wrap-half">
    <div class="item-pickup-wrap bg180803 bg02" id="new_item05"> <a href="/newitem/item.php#new_item05">
      <div class="item-half">
        <div class="in">
          <p class="item-copy">オフ輪印刷 後加工用 帯電防止液シリーズ</p>
          <p class="item-title"><br><span>除電滑走剤　S-cubos 55S</span></p>
          <p class="item-txt">消防法非該当のシリコーン含有タイプ</p>
        </div>
      </div>
      </a> </div>
    <div class="item-pickup-wrap bg200730 bg01" id="new_item03"> <a href="/products/category03.php#sec-08">
      <div class="item-half">
        <div class="in rb2ex">
          <p class="item-copy">オフセット印刷機向け </span></p>
          <p class="item-title"><br><span>ラバーメンテナンスクリーナーRB-2EX</p>
          <p class="item-txt">消防法非該当のインキローラー用、グレーズ堆積抑制剤</p>
        </div>
      </div>
      </a> </div>
  </div>
</div>

<!--/full--> 

<!-- inc -->
<?php include("../inc/footer.php"); ?>
<!-- /inc --> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc --> 

<script>
$(function() {
 $(".wrap-campaign").animate({ 
    right: 0
  }, 1000 );
 });
</script>
</body>
</html>
