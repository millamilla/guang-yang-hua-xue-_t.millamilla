<script src="/js/jquery-1.11.3.min.js"></script>
<script src="/js/common.js"></script>
<script src="/js/jquery.rwdImageMaps.min.js"></script>
<script src="/js/jquery.inview.min.js"></script>
<script src="/js/jquery.matchHeight.js"></script>
<script src="/js/remodal/remodal.js"></script>
<script type="text/javascript" src="/js/slick/slick.min.js"></script>
<script>
$(function(){
  $('img[usemap]').rwdImageMaps();
});
</script>