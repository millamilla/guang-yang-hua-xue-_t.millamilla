<section id="vategory-nav" class="section-01">
	<div class="inav">
		<section class="section-03">
			<ul class="num-4">
				<li class="single"><a href="/products/category01.php" class="no-">
					<p class="img"><img src="/img/common/ico_bs_01.jpg" alt="" title=""></p>
					<p class="text">湿し水<br>関連薬品 </p>
					</a></li>
				<li class="single"><a href="/products/category02.php" class="no-">
					<p class="img"><img src="/img/common/ico_bs_02.jpg" alt="" title=""></p>
					<p class="text">水棒<br>関連薬品</p>
					</a></li>
				<li class="single"><a href="/products/category03.php" class="no-">
					<p class="img"><img src="/img/common/ico_bs_03.jpg" alt="" title=""></p>
					<p class="text">インキローラー<br>
						関連薬品</p>
					</a></li>
				<li class="single"><a href="/products/category04.php" class="no-">
					<p class="img"><img src="/img/common/ico_bs_04.jpg" alt="" title=""></p>
					<p class="text">ブランケット<br>
						関連薬品</p>
					</a></li>
			</ul>
		</section>
		<section class="section-03">
			<ul class="num-4">
				<li class="single"><a href="/products/category05.php" class="no-">
					<p class="img"><img src="/img/common/ico_bs_05.jpg" alt="" title=""></p>
					<p class="text">後加工用薬品<br> (帯電防止液)</p>
					</a></li>
				<li class="single"><a href="/products/category06.php" class="no-">
					<p class="img"><img src="/img/common/ico_bs_06.jpg" alt="" title=""></p>
					<p class="text lh1">刷版<br>関連薬品</p>
					</a></li>
				<li class="single"><a href="/products/category07.php" class="no-">
					<p class="img"><img src="/img/common/ico_bs_07.jpg" alt="" title=""></p>
					<p class="text">その他メンテナンス<br>
						関連薬品</p>
					</a></li>
				<li class="single"><a href="/products/category08.php" class="no-">
					<p class="img"><img src="/img/common/ico_bs_08.png" alt="" title=""></p>
					<p class="text lh1">製版<br>
関連薬品</p>
					</a></li>
			</ul>
		</section>
	</div>
</section>
