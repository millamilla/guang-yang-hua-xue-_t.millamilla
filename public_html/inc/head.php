<?php
$ua=$_SERVER['HTTP_USER_AGENT'];
$browser=
((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false));
?>
<?php
if($browser=='sp') { ?>
	<!-- sp -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

<?php } else { ?>
	<!-- pc -->
	<meta name="viewport" content="target-densitydpi=device-dpi, width=1200, maximum-scale=1.0, user-scalable=yes">
<?php } ?>

<meta name="author" content="光陽化学工業">
<meta name="keywords" content="印刷関連機器,製造,販売,製版,刷版,印刷用薬品">
<meta name="description" content="写真製版・印刷用機材及び化学工業薬品の製造・販売　光陽化学工業（株）へようこそ">
<meta name="robots" content="all">
<link rel="stylesheet" type="text/css" href="/css/reset.css">
<link href="https://fonts.googleapis.com/earlyaccess/sawarabigothic.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="stylesheet" type="text/css" href="/js/remodal/remodal.css">
<link rel="stylesheet" type="text/css" href="/js/remodal/remodal-default-theme.css">
<link rel="stylesheet" type="text/css" href="/css/zen.css">
<link href="/js/slick/slick-theme.css" rel="stylesheet" type="text/css">
<link href="/js/slick/slick.css" rel="stylesheet" type="text/css">
<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="js/html5shiv-printshiv.js"></script>
<![endif]-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117505546-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117505546-1');
</script>
