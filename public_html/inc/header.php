<!--<div class="alert-wrap pc-only">
	<div class="alert">
		<p>修正中です</p>
	</div>
</div>-->
<!--header-->
<div id="header-wrap">
  <header id="g-header">
    <h1 class="pc-only"><a href="/"><img src="/img/common/logo.png" alt="光陽化学工業"/></a></h1>
    <h1 class="sp-only"><a href="/"><img src="/img/common/logo_sp.png" alt="光陽化学工業"/></a></h1>
    <div class="toggle-btn sp-only">
      <div class="icon-animation type-1"> <span class="top"></span> <span class="middle"></span> <span class="bottom"></span> </div>
    </div>
    <nav id="g-nav">
      <ul>
      <li class="single nav-parent"><a class="no-op bg-orange" href="/search"><span class="ico-recommend"><span class="">お薦め製品</span></span></a>
          <ul class="nav-child">
            <li><a href="/search/offset.php" class="no-"><span class="size-s-">オフセット　輪転機用</span></a></li>
            <li><a href="/search/uvink.php" class="no-"><span class="size-s-">枚葉機　UVインキ用</span></a></li>
            <li><a href="/search/oilyink.php" class="no-"><span class="size-s-">枚葉機　油性インキ用</span></a></li>
            <li><a href="/search/search_04.php" class="no-"><span class="size-s-">お薦め製品一覧表</span></a></li>
          </ul>
        </li>
        <li class="single"><a href="/valuableinfo/" class="no-op bg-orange"><span class="ico-valuable">お得情報</span></a></li>
        <li class="single  nav-parent"><a href="/newitem/" class="no-op bg-orange"><span class="ico-new "><span class="">新製品</span><br class="pc-only">
          <span class="s">（プリンティング事業）</span></span></a>
          <ul class="nav-child">
            <li><a href="/newitem/#new_item02" class="no-"><span>オフセット輪転機用エッチ液</span></a></li>
            <li><a href="/newitem/#new_item02" class="no-"><span>オフセット枚葉機用エッチ液</span></a></li>
            <li><a href="/newitem/#new_item03" class="no-"><span>インキローラー洗浄液</span></a></li>
            <li><a href="/newitem/#new_item03" class="no-"><span>水棒関連薬品</span></a></li>
            <li><a href="/newitem/#new_item05" class="no-"><span>オフ輪印刷 後加工用<br>
              帯電防止液シリーズ</span></a></li>
          </ul>
        </li>
        <li class="single nav-parent"><a href="/products/" class="no-op"><span class="ico-pickup">製品一覧<br class="pc-only">
          <span class="s">（プリンティング事業）</span></span></a>
          <ul class="nav-child">
            <li><a href="/products/category01.php" class="no-"><span>湿し水関連薬品</span></a></li>
            <li><a href="/products/category02.php" class="no-"><span>水棒関連薬品</span></a></li>
            <li><a href="/products/category03.php" class="no-"><span>インキローラー関連薬品</span></a></li>
            <li><a href="/products/category04.php" class="no-"><span>ブランケット関連薬品</span></a></li>
            <li><a href="/products/category05.php" class="no-"><span>後加工用薬品 (帯電防止液)</span></a></li>
            <li><a href="/products/category06.php" class="no-"><span>刷版関連薬品</span></a></li>
            <li><a href="/products/category07.php" class="no-"><span>その他メンテナンス<br class="pc-only">
              関連薬品</span></a></li>
            <li><a href="/products/category08.php" class="no-"><span>製版関連薬品</span></a></li>
          </ul>
        </li>
        <li class="single"><a href="/inquiry/" class="no-op"><span class="ico-contact">お問合せ</span></a></li>

        <li class="single nav-parent"><a href="/lab/" class="no-op"><span class="ico-dot">研究開発</span></a>
          <ul class="nav-child">
            <li><a href="/lab/index.php#sec-01"><span>開発理念</span></a></li>
            <li><a href="/lab/index.php#sec-02"><span>技術レポート</span></a></li>
          </ul>
        </li>
        <li class="single nav-parent"><a href="/company/" class="no-op"><span class="ico-dot">会社案内</span></a>
          <ul class="nav-child">
            <li><a href="/company/index.php#sec-01"><span>企業理念</span></a></li>
            <li><a href="/company/index.php#sec-02"><span>会社概要</span></a>
              <ul>
                <li><a href="/company/index.php#sec-03"><span>沿革</span></a></li>
              </ul>
            </li>
            <li><a href="/company/index.php#sec-04"><span>アクセス</span></a></li>
          </ul>
        </li>
        <li class="single nav-parent"><a href="/business/" class="no-op"><span class="ico-dot">事業内容</span></a>
          <ul class="nav-child">
            <li><a href="/business/printing.php" class="no-"><span>プリンティング事業用 ケミカル</span></a></li>
            <li><a href="/business/industrial/" class="no-"><span>各種工業用 ケミカル</span></a></li>
          </ul>
        </li>
		  
    </ul>
    </nav>
  </header>
</div>
<!--/header--> 