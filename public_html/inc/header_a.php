<!--<div class="alert-wrap pc-only">
	<div class="alert">
		<p>修正中です</p>
	</div>
</div>-->
<!--header-->
<div id="header-wrap">
  <header id="g-header">
    <h1 class="pc-only"><a href="/"><img src="/img/common/logo.png" alt="光陽化学工業"/></a></h1>
    <h1 class="sp-only"><a href="/"><img src="/img/common/logo_sp.png" alt="光陽化学工業"/></a></h1>
    <div class="toggle-btn sp-only">
      <div class="icon-animation type-1"> <span class="top"></span> <span class="middle"></span> <span class="bottom"></span> </div>
    </div>
    <nav id="g-nav">
      <ul>
        <li class="single nav-parent"><a href="/company/" class="no-op"><span class="ico-company">会社案内</span></a>
          <ul class="nav-child">
            <li><a href="/company/index.php#sec-01"><span>企業理念</span></a></li>
            <li><a href="/company/index.php#sec-02"><span>会社概要</span></a>
              <ul>
                <li><a href="/company/index.php#sec-03"><span>沿革</span></a></li>
              </ul>
            </li>
            <li><a href="/company/index.php#sec-04"><span>アクセス</span></a></li>
          </ul>
        </li>
        <li class="single nav-parent"><a href="/business/" class="no-op"><span class="ico-business">事業内容</span></a>
          <ul class="nav-child">
            <li><a href="/business/printing.php" class="no-"><span>プリンティング事業</span></a></li>
            <li><a href="/business/medical.php" class="no-"><span>医療機器薬剤事業</span></a></li>
            <li><a href="/business/electric.php" class="no-"><span>電子材料関連薬品事業</span></a></li>
            <li><a href="/business/daily.php" class="no-"><span>日用品関連事業</span></a></li>
          </ul>
        </li>
        <li class="single nav-parent"><a href="/search/" class="no-op"><span class="ico-recommend"><span class="eng">Focus Products</span></span></a>
          <ul class="nav-child">
            <li><a href="/search/search_01.php" class="no-"><span class="size-s-">消防法 非該当<br>
              製品シリーズ&lt;第１弾&gt;</span></a></li>
            <li><a href="/search/search_02.php" class="no-"><span class="size-s-">オフセットUV印刷向け<br>
              製品シリーズ</span></a></li>
            <li><a href="/search/search_03.php" class="no-"><span class="size-s-">オフセット輪転印刷向け<br>
              製品シリーズ</span></a></li>
            <li><a href="/search/search_04.php" class="no-"><span class="size-s-">オフ輪印刷 後加工用<br>
              帯電防止液シリーズ</span></a></li>
          </ul>
        </li>
        <li class="single  nav-parent"><a href="/newitem/" class="no-op"><span class="ico-new "><span class="eng">New Item</span><br class="pc-only">
          <span class="s">（プリンティング事業）</span></span></a>
          <ul class="nav-child">
            <li><a href="/newitem/#new_item01" class="no-"><span>オフセット輪転機用エッチ液</span></a></li>
            <li><a href="/newitem/#new_item02" class="no-"><span>オフセット枚葉機用エッチ液</span></a></li>
            <li><a href="/newitem/#new_item03" class="no-"><span>インキローラーメンテナンス薬品　</span></a></li>
            <li><a href="/newitem/#new_item04" class="no-"><span>水棒関連薬品</span></a></li>
            <li><a href="/newitem/#new_item05" class="no-"><span>オフ輪印刷 後加工用<br>
              帯電防止液シリーズ</span></a></li>
            <!--<li class="list-bnr"><a href="/campaign/"><img class="pc-only" src="/img/contents/bnr_side_close.jpg" alt=""/><img class="sp-only w-100 mw-none" src="/img/contents/bnr_head_close.jpg" alt=""/></a></li>-->
          </ul>
        </li>
        <li class="single nav-parent"><a href="/products/" class="no-op"><span class="ico-pickup">製品一覧<br class="pc-only">
          <span class="s">（プリンティング事業）</span></span></a>
          <ul class="nav-child">
            <li><a href="/products/category01.php" class="no-"><span>湿し水関連薬品</span></a></li>
            <li><a href="/products/category02.php" class="no-"><span>水棒関連薬品</span></a></li>
            <li><a href="/products/category03.php" class="no-"><span>インキローラー関連薬品</span></a></li>
            <li><a href="/products/category04.php" class="no-"><span>ブランケット関連薬品</span></a></li>
            <li><a href="/products/category05.php" class="no-"><span>後加工用薬品 (帯電防止液)</span></a></li>
            <li><a href="/products/category06.php" class="no-"><span>刷版関連薬品</span></a></li>
            <li><a href="/products/category07.php" class="no-"><span>その他メンテナンス<br class="pc-only">
              関連薬品</span></a></li>
            <li><a href="/products/category08.php" class="no-"><span>製版関連薬品</span></a></li>
          </ul>
        </li>
        <li class="single nav-parent"><a href="/lab/" class="no-op"><span class="ico-lab">研究開発</span></a>
          <ul class="nav-child">
            <li><a href="/lab/index.php#sec-01"><span>開発理念</span></a></li>
            <li><a href="/lab/index.php#sec-02"><span>技術レポート</span></a></li>
          </ul>
        </li>
        <li class="single"><a href="/inquiry/" class="no-op"><span class="ico-contact">お問合せ</span></a></li>
        <li class="nav-parent">
         <div class="tw-btn">
			<section>
            <div class="button" style="padding-top: 10px;"> <a href="http://koyo-chemicals.t.millamilla.co.jp/campaign/" class="twitter-follow-button" data-show-count="false" data-size="large">無料サンプルキャンペーン中</a> </div>
            <div class="cover">
              <div class="innie" style="padding-top: 10px;"> <a class="twitter-logo tw_click" height="10" viewBox="0 0 400 400" width="10">NEW ITEM </a> </div>
              <div class="spine"></div>
              <div class="outie" style="padding-top: 10px;"> <a class="twitter-logo tw_click" height="10" viewBox="0 0 400 400" width="10">NEW ITEM </a> </div>
            </div>
            <div class="shadow"></div>
          </section>
			</div>
			<div class="btn_pointer btn_flashing" align="center">
			<img src="../img/common/btn_pointer.gif" >
			</div>
        </li>
        <div class="btnNpointer">
        <li class="gra-btn  nav-parent">
          <div class="wrapper sample_btn"> <a href="/campaign" class="fancy-button bg-gradient1">
            <span class="main"><span class="tex-lg">NEW ITEM</span><br>
              <span class="tex-sm">サンプルキャンペーン</span>
              <!-- <span class="campaign_close">今回のキャンペーンは<br>終了致しました</span> -->
            </span></a> 
          </div>
        </li>
        <div class="btn_flashing2" align="center"> <img src="/img/common/btn_pointer.png" ></div> 
      </div>
      </ul>
    </nav>
  </header>
</div>
<!--/header--> 