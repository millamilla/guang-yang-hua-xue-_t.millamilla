<!--footer-->

<div id="footer-wrap">
	<p id="page-top"><a href="#top"><img src="/img/common/btn_gotop.png" alt=""/></a></p>
	<footer id="g-footer">
		<nav id="footer-nav">
			<dl class="list-fnav w3">
				<dt class="parent"><a href="/search/"><span>お薦め製品</span></a></dt>
				<dd class="child">
					<ul class="">
						<li><a href="/search/offset.php"><span>オフセット輪転機用</span></a></li>
						<li><a href="/search/uvink.php"><span>枚葉機 UVインキ用</span></a></li>
						<li><a href="/search/oilyink.php"><span>枚葉機 油性インキ用</span></a></li>
						<li><a href="/search/search_04.php"><span>お薦め製品一覧</span></a></li>
					</ul>
				</dd>
			</dl>
			<dl class="list-fnav w3">
				<dt class="parent"><a href="/valuableinfo/"><span>お得情報</span></a></dt>
				<dd class="child">
					<ul class="">
						<li><a href="/campaign/"><span>キャンペーン情報</span></a></li>
						<li><a href="/valuableinfo/environment.php"><span>さぁ始めよう 環境対策</span></a></li>
					</ul>
				</dd>
			</dl>
			<dl class="list-fnav w1">
				<dt class="parent"><a href="/newitem/"><span>新製品</span></a></dt>
			</dl>
			<dl class="list-fnav w6">
				<dt class="parent"><a href="/products/"><span>製品一覧</span></a></dt>
				<dt class="parent"><a href="/inquiry/"><span>お問合せ</span></a></dt>
				<dt class="parent"><a href="/privacy/"><span>個人情報保護方針</span></a></dt>
			</dl>			
			<dl class="list-fnav w3">
				<dt class="parent"><a href="/lab/"><span>研究開発</span></a></dt>
				<dd class="child">
					<ul class="">
						<li><a href="/lab/index.php#sec-01"><span>開発理念</span></a></li>
						<li><a href="/lab/index.php#sec-02"><span>技術レポート</span></a></li>
					</ul>
				</dd>
			</dl>
			<dl class="list-fnav w1">
				<dt class="parent"><a href="/company/"><span>会社案内</span></a></dt>
				<dd class="child">
					<ul>
						<li><a href="/company/index.php#sec-01"><span>企業理念</span></a></li>
						<li><a href="/company/index.php#sec-02"><span>会社概要</span></a></li>
						<li><a href="/company/index.php#sec-03"><span>沿革</span></a></li>
						<li><a href="/company/index.php#sec-04"><span>アクセス</span></a></li>
					</ul>
				</dd>
			</dl>
			<dl class="list-fnav w2">
				<dt class="parent"><a href="/business/"><span>事業内容</span></a></dt>
				<dd class="child">
					<ul>
						<li><a href="/business/printing.php" class="no-"><span>プリンティング事業用 ケミカル</span></a></li>
						<li><a href="/business/industrial/" class="no-"><span>各種工業用 ケミカル</span></a></li>
					</ul>
				</dd>
			</dl>
		</nav>
		<div id="footer-info">
			<p class="logo"><a href="/"><img src="/img/common/logo_footer.png" alt="光陽化学工業"/></a></p>
			<address>
			化学工業薬品の製造販売、印刷関連及び写真製版用薬品・機材の製造販売｜光陽化学工業株式会社<br>
			大阪本社：536-0025　大阪府大阪市城東区森之宮2-3-5　[TEL]06-6969-0026　[FAX]06-6969-1824<br>
			東京支社：103-0024　東京都中央区日本橋小舟町15-15　ルネ小舟町ビル２Ｆ　[TEL]03-3661-2700　[FAX]03-3661-2706
			</address>
		</div>
	</footer>
	<div id="copyright">Copyright (c)2018KOYO CHEMICALS INC. All Rights Reserved.</div>
</div>
<!--/footer--> 