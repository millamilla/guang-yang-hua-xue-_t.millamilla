<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>お薦め製品 | 光陽化学工業</title>

<link rel="stylesheet" type="text/css" href="test.css">

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span>お薦め製品</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="" itemprop="url">lineup</a></span> </div>
			</div>
			<!--breadcrumb--> 
			<div id="lineup-page">
				<!--section-->
				<section id="sec-01" class="section-01 separateline">
					<h2 class="lineup__title"><span>枚葉機 油性インキ用 ラインアップ</span></h2>
					<div class="bg">
						<ul class="lineup__kind lineup__kind01">
							<li class="lineup-01 left">
								<div id="js-show-popup01">
									<span class="title">インキローラー洗浄液</span>
									<span class="item">・セーフティー ロールクリン<br class="pc-only">SR-1</span>
									<span class="item">・ロールクリン KR-1</span>
								</div>
							</li>
							
							<li class="lineup-01 right">
								<div id="js-show-popup02">
									<span class="title">オフセット枚葉機用 エッチ液</span>
									<span class="sub-title">SOLAIA シリーズ</span>
									<span class="item">・SOLAIA 517</span>
									<span class="item">・SOLAIA 507</span>
									<span class="item">・SOLAIA 505</span>
								</div>
							</li>
						</ul>
						<ul class="lineup__kind lineup__kind02">
							<li class="lineup-01 left">
								<div id="js-show-popup03">
									<span class="title">インキローラーメンテナンス</span>
									<span class="item">・ラバーメンテナンスクリーナー RB-2EX</span>
									<span class="item">・色替え洗浄剤 クリーンチェンジャー CC-1</span>
								</div>
							</li>
							<li class="lineup-01 right">
								<div id="js-show-popup04">
									<span class="title">インキローラー洗浄液</span>
									<span class="sub-title">セーフティーダンプキーパー<br class="pc-only">PK-25EX </span>
								</div>
							</li>
						</ul>
					</div>
					<!-- ポップアップ01 -->
					<div class="popup" id="lineup-popup01">
						<div class="popup-inner">
							<div class="close-btn" id="js-close-btn01"><i class="fas fa-times"></i></div>
							<div class="popup-inner__lists">
								<a href="/products/category03.php#sec-06" class="popup-inner__item">
									<h4 class="popup-inner__item-title">セーフティー ロールクリン SR-1</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにセーフティー ロールクリン SR-1についての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/PIC_SR_1.jpg" alt="セーフティー ロールクリン SR-1"></p>
									</div>
								</a>
								<a href="/products/category03.php#sec-06" class="popup-inner__item">
									<h4 class="popup-inner__item-title">ロールクリン KR-1</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにロールクリン KR-1についての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/PIC_KR_1.jpg" alt="ロールクリン KR-1"></p>
									</div>
								</a>
							</div>
						</div>
						<div class="black-background" id="js-black-bg01"></div>
					</div>
					<!-- ポップアップ01 -->
					<!-- ポップアップ02 -->
					<div class="popup" id="lineup-popup02">
						<div class="popup-inner">
							<div class="close-btn" id="js-close-btn02"><i class="fas fa-times"></i></div>
							<div class="popup-inner__lists">
								<a href="/products/category01.php#sec-01" class="popup-inner__item">
									<h4 class="popup-inner__item-title">SOLAIA 517</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにSOLAIA 517についての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/pic_517_2.jpg" alt="SOLAIA 517"></p>
									</div>
								</a>
								<a href="/products/category01.php#sec-01" class="popup-inner__item">
									<h4 class="popup-inner__item-title">SOLAIA 507</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにSOLAIA 507についての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/pic_517_2.jpg" alt="SOLAIA 517"></p>
									</div>
								</a>
								<a href="/products/category01.php#sec-01" class="popup-inner__item">
									<h4 class="popup-inner__item-title">SOLAIA 505</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにSOLAIA 505についての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/pic_517_2.jpg" alt="SOLAIA 517"></p>
									</div>
								</a>
							</div>
						</div>
						<div class="black-background" id="js-black-bg02"></div>
					</div>
					<!-- ポップアップ02 -->
					<!-- ポップアップ03 -->
					<div class="popup" id="lineup-popup03">
						<div class="popup-inner">
							<div class="close-btn" id="js-close-btn03"><i class="fas fa-times"></i></div>
							<div class="popup-inner__lists">
								<a href="/products/category03.php#sec-08" class="popup-inner__item">
									<h4 class="popup-inner__item-title">ラバーメンテナンスクリーナー RB-2EX</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにラバーメンテナンスクリーナー RB-2EXについての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/PIC_RB-2EX.png" alt="ラバーメンテナンスクリーナーRB-2EX"></p>
									</div>
								</a>
								<a href="/products/category03.php#sec-08" class="popup-inner__item">
									<h4 class="popup-inner__item-title">色替え洗浄剤 クリーンチェンジャー CC-1</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここに色替え洗浄剤 クリーンチェンジャー CC-1についての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/PIC_CC_1.jpg" alt="色替え洗浄剤　クリーンチェンジャー　CC-1"></p>
									</div>
								</a>
							</div>
						</div>
						<div class="black-background" id="js-black-bg03"></div>
					</div>
					<!-- ポップアップ03 -->
					<!-- ポップアップ04 -->
					<div class="popup" id="lineup-popup04">
						<div class="popup-inner">
							<div class="close-btn" id="js-close-btn04"><i class="fas fa-times"></i></div>
							<div class="popup-inner__lists">
								<a href="/products/category02.php#sec-01" class="popup-inner__item">
									<h4 class="popup-inner__item-title">セーフティーダンプキーパー PK-25EX</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにセーフティーダンプキーパー PK-25EXについての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/PIC_pk-25ex.jpg" alt="セーフティーダンプキーパー PK-25EX"></p>
									</div>
								</a>
							</div>
						</div>
						<div class="black-background" id="js-black-bg04"></div>
					</div>
					<!-- ポップアップ04 -->
				</section>
				<!--section--> 


				<!--section-->
				<section id="sec-02" class="section-01">
					<h2 class="lineup__title"><span>枚葉機 油性インキ用 ラインアップ</span><br><span class="small">消防法対策製品</span></h2>
					<div class="bg">
						<ul class="lineup__kind lineup__kind01">
							<li class="lineup-01 left">
								<div id="js-show-popup05">
									<span class="title">インキローラー洗浄液</span>
									<span class="item">・セーフティー ロールクリン<br class="pc-only">SR-1</span>
									<span class="item">・ロールクリン KR-1</span>
								</div>
							</li>
							
							<li class="lineup-01 right">
								<div id="js-show-popup06">
									<span class="title">オフセット枚葉機用 エッチ液</span>
									<span class="sub-title">SOLAIA シリーズ</span>
									<span class="item">・SOLAIA 517</span>
									<span class="item">・SOLAIA 507</span>
									<span class="item">・SOLAIA 505</span>
								</div>
							</li>
						</ul>
						<ul class="lineup__kind lineup__kind02">
							<li class="lineup-01 left">
								<div id="js-show-popup07">
									<span class="title">インキローラーメンテナンス</span>
									<span class="item">・ラバーメンテナンスクリーナー RB-2EX</span>
									<span class="item">・色替え洗浄剤 クリーンチェンジャー CC-1</span>
								</div>
							</li>
							<li class="lineup-01 right">
								<div id="js-show-popup08">
									<span class="title">インキローラー洗浄液</span>
									<span class="sub-title">セーフティーダンプキーパー<br class="pc-only">PK-25EX </span>
								</div>
							</li>
						</ul>
					</div>
					<!-- ポップアップ01 -->
					<div class="popup" id="lineup-popup05">
						<div class="popup-inner">
							<div class="close-btn" id="js-close-btn05"><i class="fas fa-times"></i></div>
							<div class="popup-inner__lists">
								<a href="/products/category03.php#sec-06" class="popup-inner__item">
									<h4 class="popup-inner__item-title">セーフティー ロールクリン SR-1</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにセーフティー ロールクリン SR-1についての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/PIC_SR_1.jpg" alt="セーフティー ロールクリン SR-1"></p>
									</div>
								</a>
								<a href="/products/category03.php#sec-06" class="popup-inner__item">
									<h4 class="popup-inner__item-title">ロールクリン KR-1</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにロールクリン KR-1についての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/PIC_KR_1.jpg" alt="ロールクリン KR-1"></p>
									</div>
								</a>
							</div>
						</div>
						<div class="black-background" id="js-black-bg05"></div>
					</div>
					<!-- ポップアップ01 -->
					<!-- ポップアップ02 -->
					<div class="popup" id="lineup-popup06">
						<div class="popup-inner">
							<div class="close-btn" id="js-close-btn06"><i class="fas fa-times"></i></div>
							<div class="popup-inner__lists">
								<a href="/products/category01.php#sec-01" class="popup-inner__item">
									<h4 class="popup-inner__item-title">SOLAIA 517</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにSOLAIA 517についての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/pic_517_2.jpg" alt="SOLAIA 517"></p>
									</div>
								</a>
								<a href="/products/category01.php#sec-01" class="popup-inner__item">
									<h4 class="popup-inner__item-title">SOLAIA 507</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにSOLAIA 507についての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/pic_517_2.jpg" alt="SOLAIA 517"></p>
									</div>
								</a>
								<a href="/products/category01.php#sec-01" class="popup-inner__item">
									<h4 class="popup-inner__item-title">SOLAIA 505</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにSOLAIA 505についての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/pic_517_2.jpg" alt="SOLAIA 517"></p>
									</div>
								</a>
							</div>
						</div>
						<div class="black-background" id="js-black-bg06"></div>
					</div>
					<!-- ポップアップ02 -->
					<!-- ポップアップ03 -->
					<div class="popup" id="lineup-popup07">
						<div class="popup-inner">
							<div class="close-btn" id="js-close-btn07"><i class="fas fa-times"></i></div>
							<div class="popup-inner__lists">
								<a href="/products/category03.php#sec-08" class="popup-inner__item">
									<h4 class="popup-inner__item-title">ラバーメンテナンスクリーナー RB-2EX</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにラバーメンテナンスクリーナー RB-2EXについての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/PIC_RB-2EX.png" alt="ラバーメンテナンスクリーナーRB-2EX"></p>
									</div>
								</a>
								<a href="/products/category03.php#sec-08" class="popup-inner__item">
									<h4 class="popup-inner__item-title">色替え洗浄剤 クリーンチェンジャー CC-1</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここに色替え洗浄剤 クリーンチェンジャー CC-1についての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/PIC_CC_1.jpg" alt="色替え洗浄剤　クリーンチェンジャー　CC-1"></p>
									</div>
								</a>
							</div>
						</div>
						<div class="black-background" id="js-black-bg07"></div>
					</div>
					<!-- ポップアップ03 -->
					<!-- ポップアップ04 -->
					<div class="popup" id="lineup-popup08">
						<div class="popup-inner">
							<div class="close-btn" id="js-close-btn08"><i class="fas fa-times"></i></div>
							<div class="popup-inner__lists">
								<a href="/products/category02.php#sec-01" class="popup-inner__item">
									<h4 class="popup-inner__item-title">セーフティーダンプキーパー PK-25EX</h4>
									<div class="popup-inner__item-flex">
										<p class="popup-inner__item-text">ここにセーフティーダンプキーパー PK-25EXについての簡単な説明がはいります。</p>
										<p class="popup-inner__item-img"><img src="/img/contents/PIC_pk-25ex.jpg" alt="セーフティーダンプキーパー PK-25EX"></p>
									</div>
								</a>
							</div>
						</div>
						<div class="black-background" id="js-black-bg08"></div>
					</div>
					<!-- ポップアップ04 -->
				</section>
				<!--section--> 
			</div>
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->

<script>

function popupImage() {
	for(let i =1; i<9; i++){

		let popup = document.getElementById('lineup-popup0' + i);
		let showBtn = document.getElementById('js-show-popup0' + i);
		let blackBg = document.getElementById('js-black-bg0' + i);
		let closeBtn = document.getElementById('js-close-btn0' + i);
		
		closePopUp(blackBg, popup);
		closePopUp(closeBtn, popup);
		closePopUp(showBtn, popup);

		function closePopUp(elem, $popup_content) {
			elem.addEventListener('click', function() {
				$popup_content.classList.toggle('is-show');
			});
		}
	}
}
popupImage();

</script>
</body>
</html>
