<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>ブランケット関連薬品 | 製品一覧 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span>製品一覧</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/products/" itemprop="url">製品一覧</a></span> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/products/category04.php" itemprop="url">ブランケット関連薬品</a></span></div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section class="section-01">
				<h2 class="title-00">ブランケット関連薬品</h2>
				
				<!--section-->
				<section id="sec-01" class="section-02">
					<div class="wrap-list-sds">
						<ul class="list-sds">
							<li class="single">
								<p class="title"><span>セーフティーブラン洗浄液　SB-1EX</span></p>
								<div class="in">
									<div class="img"><img src="/img/contents/PIC_SB_1_EX.jpg" alt=""></div>
									<ul class="btn">
										
										<li><a href="/doc/CATALOG_SB_1_EX.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_SB_1_EX.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年03月25日)</span></a></li>
									</ul>
								</div>
							</li>
						
							<li class="single">
								<p class="title">ブラン洗浄液　EBD-G</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_EBD_G.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年03月07日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">ブラン洗浄液　EBD-P</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_EBD_P.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年05月10日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">ブランリカバー BR-7</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_BR_7.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年03月07日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">ブラン・スウィーパー　SW-3</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_SW_3.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年03月30日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">コーヨー紙粉洗浄液　SS-11</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_SS_11.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年03月30日)</span></a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</section>
				<!--section-->
				
				<section id="sec-12" class="section-02">
					<p class="text-03 ta-c ">こちらに掲載のない製品についてのSDSは<a href="/inquiry/" class="link-01">お問合せ</a>ください</p>
				</section>
				<section class="section-01 ta-c">
					<p class="text-02">Acrobat Reader は、無料でダウンロードできます。</p>
					<p class="ind-02"><a href="https://get.adobe.com/jp/reader/" target="_blank" class="btn-02"><span class="ico-">ダウンロードはこちらから</span></a></p>
				</section>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc --> 
<script>
$(function() {
 
	$('.cmap area').hover(function() {
		
		var chBg = $(this).attr("name");
		$("#"+chBg+" img").attr('src', $("#"+chBg+" img").attr('src').replace('_off', '_on'));

		
		
	}, function() {
		var chBg = $(this).attr("name");
		$("#"+chBg+" img").attr('src', $("#"+chBg+" img").attr('src').replace('_on', '_off'));

	
	});
	
});
</script>
</body>
</html>
