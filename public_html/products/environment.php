<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>環境関連法 | 製品一覧 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span>環境関連法</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/products/" itemprop="url">製品一覧</a> &gt; </span></div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/products/environment.php" itemprop="url">環境関連法</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--lnav-->
			<div class="lnav">
				<ul class="num-1">
					<li class="full-sp full"><a href="/products/"><span>製品一覧</span></a></li>
				</ul>
			</div>
			<!--lnav--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<h2 class="title-01"><a name="roanhou"></a>労働安全衛生法</h2>
				
				<!--section-->
				<section class="section-02">
					<div class="box-environment-00">
						<h3 class="title-box-environment">目的</h3>
						<ul class="list-01">
							<li>労働災害防止のための危害防止基準の確立</li>
							<li>労働者の安全と健康の確保</li>
							<li>快適職場環境の形成を促進する</li>
						</ul>
					</div>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-03">有機溶剤中毒予防規則</h3>
					<p class="text-02">どんな種類の有機溶剤を使用した場合に規則の適用があるか<br>
						数百種類ある有機溶剤の中で社会的に、割合多く使用されていた4４種類を「有機則」の適用とした。</p>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-03">労働安全衛生法　有機溶剤中毒予防規則</h3>
					<div class="box-environment-01">
						<div class="box-environment-01-left table-03">
							<table>
								<tbody>
									<tr>
										<th colspan="2">有機溶剤区分</th>
									</tr>
									<tr>
										<td rowspan="2" style="vertical-align : middle;">第１種</td>
										<td>１・２－ジクロルエチレン</td>
									</tr>
									<tr>
										<td>二硫化炭素</td>
									</tr>
									<tr>
										<td rowspan="20" style="vertical-align : middle;">第２種</td>
										<td>アセトン</td>
									</tr>
									<tr>
										<td>イソブチルアルコール</td>
									</tr>
									<tr>
										<td>イソプロピルアルコール（ＩＰＡ）</td>
									</tr>
									<tr>
										<td>イソペンチルアルコール</td>
									</tr>
									<tr>
										<td>エチルエーテル</td>
									</tr>
									<tr>
										<td>エチレングリコールモノエチルエーテル</td>
									</tr>
									<tr>
										<td>エチレングリコールモノエチルエーテル<br>
											アセテート</td>
									</tr>
									<tr>
										<td>エチレングリコールモノブチルエーテル</td>
									</tr>
									<tr>
										<td>エチレングリコールモノメチルエーテル</td>
									</tr>
									<tr>
										<td>オルトージクロルベンゼン</td>
									</tr>
									<tr>
										<td>キシレン</td>
									</tr>
									<tr>
										<td>クレゾール</td>
									</tr>
									<tr>
										<td>クロルベンゼン</td>
									</tr>
									<tr>
										<td>酢酸イソブチル</td>
									</tr>
									<tr>
										<td>酢酸イソプロピル</td>
									</tr>
									<tr>
										<td>酢酸イソペンチル</td>
									</tr>
									<tr>
										<td>酢酸エチル</td>
									</tr>
									<tr>
										<td>酢酸ブチル</td>
									</tr>
									<tr>
										<td>酢酸プロピル</td>
									</tr>
									<tr>
										<td>酢酸ペンチル</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="box-environment-01-right table-03">
							<table>
								<tbody>
									<tr>
										<th colspan="2">有機溶剤区分 </th>
									</tr>
									<tr>
										<td rowspan="15" style="vertical-align : middle;">第２種</td>
										<td>酢酸メチル</td>
									</tr>
									<tr>
										<td>シクロヘキサノール</td>
									</tr>
									<tr>
										<td>シクロヘキサノン</td>
									</tr>
									<tr>
										<td>Ｎ・Ｎ―ジメチルホルムアミド</td>
									</tr>
									<tr>
										<td>テトラヒドロフラン</td>
									</tr>
									<tr>
										<td>１・１・１－トリクロルエタン</td>
									</tr>
									<tr>
										<td>トルエン</td>
									</tr>
									<tr>
										<td>ノルマルヘキサン</td>
									</tr>
									<tr>
										<td>１－ブタノール</td>
									</tr>
									<tr>
										<td>２－ブタノール</td>
									</tr>
									<tr>
										<td>メタノール</td>
									</tr>
									<tr>
										<td>メチルエチルケトン</td>
									</tr>
									<tr>
										<td>メチルシクロヘキサノール</td>
									</tr>
									<tr>
										<td>メチルシクロヘキサノン</td>
									</tr>
									<tr>
										<td>メチルブチルケトン</td>
									</tr>
									<tr>
										<td rowspan="7" style="vertical-align : middle;">第３種</td>
										<td>ガソリン</td>
									</tr>
									<tr>
										<td>コールタールナフサ</td>
									</tr>
									<tr>
										<td>石油エーテル</td>
									</tr>
									<tr>
										<td>石油ナフサ</td>
									</tr>
									<tr>
										<td>石油ベンジン</td>
									</tr>
									<tr>
										<td>テレピン油</td>
									</tr>
									<tr>
										<td>ミネラルスピリット</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-03">設備</h3>
					<h4 class="title-04">第１種及び第２種有機溶剤等</h4>
					<p class="text-02">蒸気の発散源を密閉する設備、局所排気装置又はプッシュプル型換気装置。</p>
					<h4 class="title-04">第３種有機溶剤等</h4>
					<p class="text-02">タンク等の内部作業において有機溶剤の蒸気の発散源を密閉する設備、局所排気装置、プッシュプル型換気装置又は全体換気装置を設けなければならない。<br>
						なお、屋内作業場の周壁の２側面以上が開放されている時、短時間の業務で送気マスクを使用させる時等には、設備の設置が省略できる。</p>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-03">健康診断</h3>
					<p>屋内作業場における第１種及び第２種有機溶剤等の業務及びタンク内における第３種有機溶剤等の業務（６月以内ごとに一回実施）</p>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-03">作業主任者の選任</h3>
					<p>物質を製造し、又は取り扱う作業については一定の技能講習を修了した作業主任者を選任する。</p>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-03">定期自主検査</h3>
					<p>局所排気装置等については、一年以内ごとに一回定期に自主検査を行い、その結果及び補修の状況を３年間保存する。</p>
				</section>
				<!--section--> 
				
			</section>
			<!--/section--> 
			
			<!--section-->
			<section id="sec-02" class="section-01">
				<h2 class="title-01">消防法</h2>
				
				<!--section-->
				<section class="section-02">
					<div class="box-environment-00">
						<h3 class="title-box-environment">目的</h3>
						<ul class="list-01">
							<li>火災の予防・警戒・鎮圧により、国民の生命、身体及び財産を火災から保護する。</li>
						</ul>
					</div>
				</section>
				<!--section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-03">法規制適用条件</h3>
					<ul>
						<li>消防法に規定する危険物を指定数量以上貯蔵、または扱う場合</li>
						<li>指定数量未満及び指定可燃物は市町村火災予防条例の技術上の基準で規制</li>
						<li>消防法に規定する危険物を運搬する場合（量に関係ない）</li>
					</ul>
				</section>
				<!--/section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-03">消防法危険物の分類と指定数量</h3>
					<div class="table-03">
						<table>
							<tbody>
								<tr>
									<th>類別</th>
									<th>性質</th>
									<th>指定数量</th>
									<th>印刷作業における処理薬品</th>
								</tr>
								<tr>
									<td rowspan="10" style="vertical-align : middle;"> 第４類</td>
									<td>特殊引火物</td>
									<td class="ta-r">50Ｌ</td>
									<td></td>
								</tr>
								<tr>
									<td>第１石油類（非水溶性液体）</td>
									<td class="ta-r">200Ｌ</td>
									<td>速乾性ブラン洗浄液など</td>
								</tr>
								<tr>
									<td>第１石油類（水溶性液体）</td>
									<td class="ta-r">400Ｌ</td>
									<td>ＩＰＡ代替添加液など</td>
								</tr>
								<tr>
									<td>アルコール類</td>
									<td class="ta-r">400Ｌ</td>
									<td>ＩＰＡ、エチルアルコールなど</td>
								</tr>
								<tr>
									<td>第２石油類（非水溶性液体）</td>
									<td class="ta-r">1,000Ｌ</td>
									<td>洗い油（鉱物、植物）、ローラークリーナー、水棒関連薬品など</td>
								</tr>
								<tr>
									<td>第２石油類（水溶性液体）</td>
									<td class="ta-r">2,000Ｌ</td>
									<td>エッチ液など</td>
								</tr>
								<tr>
									<td>第３石油類（非水溶性液体）</td>
									<td class="ta-r">2,000Ｌ</td>
									<td>植物系洗い油</td>
								</tr>
								<tr>
									<td>第３石油類（水溶性液体）</td>
									<td class="ta-r">4,000Ｌ</td>
									<td>エッチ液など</td>
								</tr>
								<tr>
									<td>第４石油類</td>
									<td class="ta-r">6,000Ｌ</td>
									<td></td>
								</tr>
								<tr>
									<td>動植物油類</td>
									<td class="ta-r">10,000Ｌ</td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
					<p>指定数量の１／５未満　　　　　：　届出は不要。但し、規制はある。<br>
						指定数量の１／５以上１未満　　：　少量危険物貯蔵、取り扱いの届け出が必要。<br>
						指定数量の１以上　　　　　　　：　屋内貯蔵所、一般取扱所等の許可が必要。</p>
				</section>
				<!--/section--> 
				
			</section>
			<!--/section--> 
			
			<!--section-->
			<section id="sec-03" class="section-01">
				<h2 class="title-01">化学物質管理促進法（ＰＲＴＲ法）</h2>
				
				<!--section-->
				<section class="section-02">
					<div class="box-environment-00 ind-01">
						<h3 class="title-box-environment">正式法律名</h3>
						<p class="text-03">「特定化学物質の環境への排出量の把握等及び管理の改善の促進に関する法律」</p>
					</div>
					<div class="box-environment-00">
						<h3 class="title-box-environment">目的</h3>
						<ul class="list-01">
							<li>事業者による化学物質の自主的な管理の改善を促進し、環境の保全上の支障を未然に防止する</li>
						</ul>
					</div>
				</section>
				<!--/section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-03">ＰＲＴＲ制度とは</h3>
					<p class="text-02">人の健康や生態系に有害なおそれのある化学物質が、事業所から環境（大気、水、土壌）へ排出される量及び廃棄物に含まれて事業所外へ移動する量を、事業者が自ら把握し国に届け出をし、国は届出データや推計に基づき、排出量・移動量を集計・公表する制度です。</p>
				</section>
				<!--/section--> 
				
				<!--section-->
				<section class="section-02">
					<h3 class="title-03">化学物質管理促進法 ＰＲＴＲ法</h3>
					
					<!--section-->
					<section class="section-03">
						<h4 class="title-04">ＰＲＴＲ法対象物質</h4>
						<p>第一種指定化学物質　　　・　４６２物質　ジクロロメタン、キシレン、トルエン、テトラクロロエチレン等<br>
							（内、特定第一種指定化学物質　・　１５物質　ベンゼン、エチレンオキシド、ダイオキシン類等）<br>
							第二種指定化学物質　　　・　１００物質　ジクロロ酢酸等</p>
					</section>
					
					<!--section-->
					<section class="section-03">
						<h4 class="title-04">ＰＲＴＲ法対象事業者</h4>
						<p>業　　　種　　　　・・・・・・・　　２４の業種<br>
							事業者規模　　 ・・・・・・・　　常用雇用者数２１人以上<br>
							１年間排出・移動量　・・・　　第一種指定化学物質１ｔ以上の事業所<br>
							※但し特定第一種指定化学物質は０．５ｔ以上</p>
					</section>
					
					<!--section-->
					<section class="section-03">
						<h4 class="title-04">ＰＲＴＲ法届出</h4>
						<p>ＰＲＴＲ法該当物質使用　→　排出量・移動量を把握し行政機関に１回／年届出</p>
					</section>
					
					<!--section-->
					<section class="section-03">
						<h4 class="title-04">開示請求</h4>
						<p>国として集計した結果を公表した日以後、主務大臣（環境大臣、経済産業大臣又は第一種指定化学物質等取扱事業者の行う事業を所管する大臣）あてに、個別の事業所が届け出た排出量等の情報について開示請求することができます。</p>
					</section>
				</section>
				<!--/section--> 
				
			</section>
			<!--section--> 
			
			<!--section-->
			<section id="sec-04" class="section-01">
				<p class="text02">2018年3月現在<br>
					詳細は各省庁のＨＰを参照ください。</p>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->

</body>
</html>
