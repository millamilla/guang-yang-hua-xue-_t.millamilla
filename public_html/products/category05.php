<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>後加工用薬品 (帯電防止液) | 製品一覧 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span>製品一覧</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/products/" itemprop="url">製品一覧</a></span> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/products/category05.php" itemprop="url">後加工用薬品 (帯電防止液)</a></span></div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section class="section-01">
				<h2 class="title-00">後加工用薬品 (帯電防止液)</h2>
				
				<!--section-->
				<section id="sec-01" class="section-02">
					<h2 class="title-03 square-a">オフセット輪転機用 帯電防止液</h2>
					<div class="wrap-list-sds">
						<ul class="list-sds">
							<li class="single">
								<p class="title"><span>除電滑走剤　S-cubos 55S <span class="new"><img src="/img/common/ico_new.gif" alt=""></span></span></p>
								<div class="in">
									<div class="img"><img src="/img/contents/PIC_S_cubos_55_S.jpg" alt=""></div>
									<ul class="btn">
										<li><a href="/lab/index.php#sec-02" class="btn-detail"><span>技術情報はこちら</span></a></li>
										<li><a href="/doc/cat_55S_3.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_S_cubos_55_S.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2017年10月10日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title"><span>除電滑走剤　S-cubos 33N <span class="new"><img src="/img/common/ico_new.gif" alt=""></span></span></p>
								<div class="in">
									<div class="img"><img src="/img/contents/PIC_S_cubos_33_N.jpg" alt=""></div>
									<ul class="btn">
										<li><a href="/lab/index.php#sec-02" class="btn-detail"><span>技術情報はこちら</span></a></li>
										<li><a href="/doc/cat_33N_3.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_S_cubos_33_N.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2017年10月10日)</span></a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</section>
				<!--section-->
				
				<section id="sec-12" class="section-02">
					<p class="text-03 ta-c ">こちらに掲載のない製品についてのSDSは<a href="/inquiry/" class="link-01">お問合せ</a>ください</p>
				</section>
				<section class="section-01 ta-c">
					<p class="text-02">Acrobat Reader は、無料でダウンロードできます。</p>
					<p class="ind-02"><a href="https://get.adobe.com/jp/reader/" target="_blank" class="btn-02"><span class="ico-">ダウンロードはこちらから</span></a></p>
				</section>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc --> 
<script>
$(function() {
 
	$('.cmap area').hover(function() {
		
		var chBg = $(this).attr("name");
		$("#"+chBg+" img").attr('src', $("#"+chBg+" img").attr('src').replace('_off', '_on'));

		
		
	}, function() {
		var chBg = $(this).attr("name");
		$("#"+chBg+" img").attr('src', $("#"+chBg+" img").attr('src').replace('_on', '_off'));

	
	});
	
});
</script>
</body>
</html>
