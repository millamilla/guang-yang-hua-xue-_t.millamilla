<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>湿し水関連薬品 | 製品一覧 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span>製品一覧</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/products/" itemprop="url">製品一覧</a></span> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/products/category01.php" itemprop="url">湿し水関連薬品</a></span></div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section class="section-01">
				<h2 class="title-00">湿し水関連薬品</h2>
				
				<!--lnav-->
				<div class="lnav">
					<ul class="num-3n">
						<li><a href="#sec-01"><span class="square-a square">エッチ液（枚葉機用）</span></a></li>
						<li><a href="#sec-02"><span class="square-b square">エッチ液（オフセット輪転機用）</span></a></li>
						<li><a href="#sec-03"><span class="square-c square">湿し水添加液</span></a></li>
						<li><a href="#sec-04"><span class="square-d square">その他湿し水関連薬品</span></a></li>
						<li class="pc-only"><a href="#sec-11">&nbsp;</a></li>
						<li class="pc-only"><a href="#sec-11">&nbsp;</a></li>
					</ul>
				</div>
				<!--lnav--> 
				
				<!--section-->
				<section id="sec-01" class="section-02">
					<h2 class="title-03 square-a">エッチ液（枚葉機用）</h2>
					<div class="wrap-list-sds">
						<ul class="list-sds">
							<li class="single">
								<p class="title"><span>SOLAIA 517  <span class="new"><img src="/img/common/ico_new.gif" alt=""></span></span></p>
								<div class="in">
									<div class="img"><img src="/img/contents/pic_517_2.jpg" alt=""/></div>
									<ul class="btn">
										<li><a href="/newitem/item.php#new_item02" class="btn-detail"><span>技術資料はこちら</span></a></li>
										<li><a href="/doc/cat_517.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_517.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2018年07月17日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">SOLAIA 505</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/cat_505_2.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_SOLAIA_505.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2015年09月25日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">SOLAIA 507</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/cat_507_2.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_SOLAIA_507.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年04月07日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">SOLAIA 501</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_SOLAIA_501.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2015年09月25日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">SOLAIA 503</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_SOLAIA_503.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年03月30日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">SOLAIA 303</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_SOLAIA_303.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2015年09月25日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">SOLAIA 307</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_SOLAIA_307.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2015年09月25日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">SOLAIA KS-N</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_SOLAIA_KS_N.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年04月07日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">SOLAIA KS-Y</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_SOLAIA_KS_Y.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年04月07日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">SOLAIA EP-3</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_SOLAIA_EP_3.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年4月7日)</span></a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</section>
				<!--section--> 
				
				<!--section-->
				<section id="sec-02" class="section-02">
					<h2 class="title-03 square-b">エッチ液（オフセット輪転機用）</h2>
					<div class="wrap-list-sds">
						<ul class="list-sds">
							<li class="single">
								<p class="title"><span>SOLAIA WEB WK711 クリア  <span class="new"><img src="/img/common/ico_new.gif" alt=""></span></span></p>
								<div class="in">
									<div class="img"><img src="/img/contents/PIC_SOLAIA_WK711.jpg" alt=""/></div>
									<ul class="btn">
										<li><a href="/newitem/item.php#new_item01" class="btn-detail"><span>技術資料はこちら</span></a></li>
										<li><a href="/doc/cat_WK711_CL.PDF" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_711CL.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2018年07月17日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title"><span>SOLAIA WEB WK801 <span class="new"><img src="/img/common/ico_new.gif" alt=""></span></span></p>
								<div class="in">
									<div class="img"><img src="/img/contents/pic_WK801_2.jpg" alt=""/></div>
									<ul class="btn">
										<li><a href="/newitem/item.php#new_item06" class="btn-detail"><span>技術資料はこちら</span></a></li>
										<li><a href="/doc/CATALOG_SOLAIA_WEB_WK_801.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_SOLAIA_WEB_WK_801.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2018年01月16日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">SOLAIA WEB WF7500</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_SOLAIA_WEB_WF_7500.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年04月07日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">SOLAIA 701</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_SOLAIA_701.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2015年09月25日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">SOLAIA EP-30</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_SOLAIA_EP_30.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年04月07日)</span></a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</section>
				<!--section--> 
				
				<!--section-->
				<section id="sec-03" class="section-02">
					<h2 class="title-03 square-c">湿し水添加液</h2>
					<div class="wrap-list-sds">
						<ul class="list-sds">
							<li class="single">
								<p class="title">IPA代替アルコール　添加液　AG-U2</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/CATALOG_AG_U2.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_AG_U2.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年08月23日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">IPA代替アルコール　添加液　AG-A1</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/CATALOG_AG_A1.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_AG_A1.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年08月23日)</span></a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</section>
				<!--section--> 
				
				<!--section-->
				<section id="sec-04" class="section-02">
					<h2 class="title-03 square-d">その他湿し水関連薬品</h2>
					<div class="wrap-list-sds">
						<ul class="list-sds">
							<li class="single">
								<p class="title">湿し水　防腐抗菌剤　PG-1</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_PG_1.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年05月18日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">湿し水　消泡剤　AF-1</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_AF_1.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年05月10日)</span></a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</section>
				<!--section-->
				
				<section id="sec-12" class="section-02">
					<p class="text-03 ta-c ">こちらに掲載のない製品についてのSDSは<a href="/inquiry/" class="link-01">お問合せ</a>ください</p>
				</section>
				<section class="section-01 ta-c">
					<p class="text-02">Acrobat Reader は、無料でダウンロードできます。</p>
					<p class="ind-02"><a href="https://get.adobe.com/jp/reader/" target="_blank" class="btn-02"><span class="ico-">ダウンロードはこちらから</span></a></p>
				</section>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc --> 
<script>
$(function() {
 
	$('.cmap area').hover(function() {
		
		var chBg = $(this).attr("name");
		$("#"+chBg+" img").attr('src', $("#"+chBg+" img").attr('src').replace('_off', '_on'));

		
		
	}, function() {
		var chBg = $(this).attr("name");
		$("#"+chBg+" img").attr('src', $("#"+chBg+" img").attr('src').replace('_on', '_off'));

	
	});
	
});
</script>
</body>
</html>
