<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>製品一覧（プリンティング事業） | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span>製品一覧（プリンティング事業）</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/products/" itemprop="url">製品一覧（プリンティング事業）</a></span></div>
			</div>
			<!--breadcrumb--> 
			
			<div class="lnav">
				<ul class="num-1">
					<li class="full-sp full"><a href="/products/environment.php"><span>環境関連法</span></a></li>
				</ul>
			</div>
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<div class="wrap-item-map p-rel">
					<p class="bg"><img src="../img/contents/top_printing.png" class="mw-1000" alt=""></p>
					<p class="cmap"><img src="../img/contents/cat_cmap.png" alt="" class="mw-1000" usemap="#Map">
						<map name="Map">
							<area shape="poly" name="bg05" coords="302,434,235,306,188,372,127,546,188,638,243,628,318,513" href="category05.php">
							<area shape="poly" name="bg01" coords="722,496,745,498,797,424,844,424,831,458,879,466,872,487,794,501,719,506" href="category01.php">
							<area shape="poly" name="bg02" coords="667,502,729,401,794,389,805,403,752,484,729,486,703,525,676,526" href="category02.php">
							<area shape="poly" name="bg03" coords="551,443,647,397,677,386,706,403,661,497,619,513" href="category03.php">
							<area shape="poly" name="bg04" coords="683,584,737,506,853,505,843,579,791,614" href="category04.php">
							<area shape="rect" name="bg07" coords="830,565,956,695" href="category07.php">
							<area shape="poly" name="bg06" coords="464,527,660,507,687,519,709,542,674,605,635,631,523,632" href="category06.php">
						</map>
					</p>
					<p id="bg06" class="item-link p-abs"><img src="/img/contents/cat_06_off.png" class="mw-1000" alt=""></p>
					<p id="bg01" class="item-link p-abs"><img src="/img/contents/cat_01_off.png" class="mw-1000" alt=""></p>
					<p id="bg02" class="item-link p-abs"><img src="/img/contents/cat_02_off.png" class="mw-1000" alt=""></p>
					<p id="bg03" class="item-link p-abs"><img src="/img/contents/cat_03_off.png" class="mw-1000" alt=""></p>
					<p id="bg04" class="item-link p-abs"><img src="/img/contents/cat_04_off.png" class="mw-1000" alt=""></p>
					<p id="bg05" class="item-link p-abs"><img src="/img/contents/cat_05_off.png" class="mw-1000" alt=""></p>
					<p id="bg07" class="item-link p-abs"><img src="/img/contents/cat_07_off.png" class="mw-1000" alt=""></p>
				</div>
			</section>
			<!--section--> 
			
			<!-- inc -->
			<?php include("../inc/arrow.php"); ?>
			<!-- /inc --> 
			
			<!-- inc -->
			<?php include("../inc/category_nav_01.php"); ?>
			<!-- /inc --> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc --> 
<script>
$(function() {
 
	$('.cmap area').hover(function() {
		
		var chBg = $(this).attr("name");
		$("#"+chBg+" img").attr('src', $("#"+chBg+" img").attr('src').replace('_off', '_on'));

		
		
	}, function() {
		var chBg = $(this).attr("name");
		$("#"+chBg+" img").attr('src', $("#"+chBg+" img").attr('src').replace('_on', '_off'));

	
	});
	
});
</script>
</body>
</html>
