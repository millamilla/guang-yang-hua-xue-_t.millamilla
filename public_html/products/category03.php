<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>インキローラー関連薬品 | 製品一覧 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span>製品一覧</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/products/" itemprop="url">製品一覧</a></span> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/products/category03.php" itemprop="url">インキローラー関連薬品</a></span></div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section class="section-01">
				<h2 class="title-00">インキローラー関連薬品</h2>
				
				<!--lnav-->
				<div class="lnav">
					<ul class="num-3n">
						<li><a href="#sec-06"><span class="square-a square">インキローラー洗浄液（油性）</span></a></li>
						<li><a href="#sec-07"><span class="square-b square">インキローラー洗浄液（UV）</span></a></li>
						<li><a href="#sec-08"><span class="square-c square">ローラーメンテナンス薬品</span></a></li>
						<li class="sp-only">&nbsp;</li>
					</ul>
				</div>
				<!--lnav--> 
				
				<!--section-->
				<section id="sec-06" class="section-02">
					<h2 class="title-03 square-a">インキローラー洗浄液（油性）</h2>
					<div class="wrap-list-sds">
						<ul class="list-sds">
							<li class="single">
								<p class="title"><span>セーフティーロールクリン　SR-1 <span class="new"><img src="/img/common/ico_new.gif" alt=""></span></span></p>
								<div class="in">
									<div class="img"><img src="/img/contents/PIC_SR_1.jpg" alt=""></div>
									<ul class="btn">
										<li><a href="/lab/index.php#sec-02" class="btn-detail"><span>技術情報はこちら</span></a></li>
										<li><a href="/doc/CATALOG_SR_1.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_SR_1.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年06月06日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title"><span>ロールクリン　KR-1 <span class="new"><img src="/img/common/ico_new.gif" alt=""></span></span></p>
								<div class="in">
									<div class="img"><img src="/img/contents/PIC_KR_1.jpg" alt=""></div>
									<ul class="btn">
										<li><a href="/lab/index.php#sec-02" class="btn-detail"><span>技術情報はこちら</span></a></li>
										<li><a href="/doc/CATALOG_KR_1.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_KR_1.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年06月08日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title"><span>セーフティーローラークリーナー　RC-10 <span class="new"><img src="/img/common/ico_new.gif" alt=""></span></span></p>
								<div class="in">
									<div class="img"><img src="/img/contents/PIC_RC_10.jpg" alt=""></div>
									<ul class="btn">
										<li><a href="/lab/index.php#sec-02" class="btn-detail"><span>技術情報はこちら</span></a></li>
										<li><a href="/doc/CATALOG_RC_10.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_RC_10.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年06月23日)</span></a></li>
									</ul>
								</div>
							</li>
							<li id="kc-77" class="single">
								<p class="title"><span>コーヨークリン　KC-77</span></p>
								<div class="in">
									<div class="img"><img src="/img/contents/PIC_KC_77.jpg" alt=""></div>
									<ul class="btn">
										<li><a href="/lab/index.php#sec-02" class="btn-detail"><span>技術情報はこちら</span></a></li>
										<li><a href="/doc/CATALOG_KC_77.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_KC_77.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年05月10日)</span></a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</section>
				<!--section--> 
				
				<!--section-->
				<section id="sec-07" class="section-02">
					<h2 class="title-03 square-b">インキローラー洗浄液（UV）</h2>
					<div class="wrap-list-sds">
						<ul class="list-sds">
							<li class="single">
								<p class="title">セーフティーUVインキ洗浄液 SV-1<span class="new"><img src="/img/common/ico_new.gif" alt=""></span></span></p>
								<div class="in">
									<div class="img"><img src="/img/contents/PIC_sv-1.jpg" alt=""></div>
									<ul class="btn">
										<li><a class="btn-detail no-link"><span>技術情報はこちら</span></a></li>
										<li><a href="/doc/cata_sv-1.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/sds_sv-1.pdf" class="btn-01" target="_blank"><span>SDS(作成日2019年09月04日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">UVインキ洗浄液　KV-9</p>
								<div class="in">
								<div class="img"><img src="/img/contents/PIC_KV-9.jpg" alt="UVインキ洗浄液　KV-9"></div>
									<ul class="btn">
										<li><a href="/doc/CATALOG_kv9.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_KV_9.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年03月01日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">UVインキ洗浄液　VC-P</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_VC_P.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年03月07日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">UVインキ洗浄液　VC-R</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_VC_R.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年03月30日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">UVインキ洗浄液　VC-7</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_VC_7.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年03月25日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
							</li>
						</ul>
					</div>
					<!-- <h2 class="bg-gray">&lt;販売中止商品&gt;</h2>
					<div class="wrap-list-sds">
						<ul class="list-sds">
							
							<li class="single" style="height: 188.109px;">
							</li>
								<li class="single">
							</li>
						</ul>
					</div> -->
					
				</section>
				<!--section--> 
				
				<!--section-->
				<section id="sec-08" class="section-02">
					<h2 class="title-03 square-c">インキローラーメンテナンス薬品</h2>
					<div class="wrap-list-sds">
						<ul class="list-sds">
							<li class="single">
								<p class="title"><span>ラバーメンテナンスクリーナーRB-2EX <span class="new"><img src="/img/common/ico_new.gif" alt=""></span></span></p>
								<div class="in">
									<div class="img"><img src="/img/contents/PIC_RB-2EX.png" alt=""></div>
									<ul class="btn">
										<li><a class="btn-detail" target="_blank"><span>技術情報はこちら</span></a></li>
										<li><a href="/doc/CATALOG_RB_2_EX.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_RB_2_EX.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2020年07月16日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title"><span>色替え洗浄剤　クリーンチェンジャー　CC-1 <span class="new"><img src="/img/common/ico_new.gif" alt=""></span></span></p>
								<div class="in">
									<div class="img"><img src="/img/contents/PIC_CC_1.jpg" alt=""></div>
									<ul class="btn">
										<li><a href="/lab/index.php#sec-02" class="btn-detail" target="_blank"><span>技術情報はこちら</span></a></li>
										<li><a href="/doc/CATALOG_CC_1.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_CC_1.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年11月29日)</span></a></li>
									</ul>
								</div>
							</li>
						
							<li class="single">
								<p class="title">サン・ソイクリーナー　SY-1</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/CATALOG_SY_1.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_SY_1.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2017年08月16日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">サン・ソイクリーナー　SY-3</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/CATALOG_SY_3.pdf" class="btn-01" target="_blank"><span>製品カタログ</span></a></li>
										<li><a href="/doc/SDS_SY_3.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2017年08月16日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
								<p class="title">ローラーガード　RG-3</p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_RG_3.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年03月25日)</span></a></li>
									</ul>
								</div>
							</li>
							<li class="single">
							</li>
						</ul>
						
					</div>
					<h2 class="bg-gray">&lt;販売中止商品&gt;</h2>
					<div class="wrap-list-sds">
						<ul class="list-sds">
							<li class="single">
								<p class="title nosell">ラバーメンテナンスクリーナー　RB-1<br><span class="lastday-goods">販売中止日:2020年8月21日</span></p>
								<div class="in">
									<ul class="btn">
										<li><a href="/doc/SDS_RB_1.pdf" class="btn-01" target="_blank"><span>SDS(改訂日2016年03月25日)</span></a></li>
										<li class="red">※SDS掲載期間:2022年8月31日まで</li>
									</ul>
								</div>
							</li>
							<li class="single">
							</li>
						</ul>
					</div>
				</section>
				<!--section-->
				
				<section id="sec-12" class="section-02">
					<p class="text-03 ta-c ">こちらに掲載のない製品についてのSDSは<a href="/inquiry/" class="link-01">お問合せ</a>ください</p>
				</section>
				<section class="section-01 ta-c">
					<p class="text-02">Acrobat Reader は、無料でダウンロードできます。</p>
					<p class="ind-02"><a href="https://get.adobe.com/jp/reader/" target="_blank" class="btn-02"><span class="ico-">ダウンロードはこちらから</span></a></p>
				</section>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc --> 
<script>
$(function() {
 
	$('.cmap area').hover(function() {
		
		var chBg = $(this).attr("name");
		$("#"+chBg+" img").attr('src', $("#"+chBg+" img").attr('src').replace('_off', '_on'));

		
		
	}, function() {
		var chBg = $(this).attr("name");
		$("#"+chBg+" img").attr('src', $("#"+chBg+" img").attr('src').replace('_on', '_off'));

	
	});
	
});
</script>
</body>
</html>
