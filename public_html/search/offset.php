<!doctype html>
<html lang="ja">

<head>
	<meta charset="utf-8">
	<link href="http://fonts.googleapis.com/earlyaccess/notosansjp.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@700&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@700&display=swap" rel="stylesheet">
	<title>オフセット輪転機用 | お薦め製品 | 光陽化学工業</title>

	<!-- inc -->
	<?php include("../inc/head.php"); ?>
	<!-- /inc -->

</head>

<body id="top">

	<!-- inc -->
	<?php include("../inc/header.php"); ?>
	<!-- /inc -->

	<!--main-->
	<div id="main-wrap">

		<!--contents-->
		<div id="contents-wrap" class="searchnew">

		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span class="eng">お薦め製品</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 

			<!--contents-2nd-->
			<div class="right-stick-box">
				<div class="box_in box_in-offset">
					<div class="form_link01"><a href="/inquiry/#check-product-contact">製品に関するお問合せ</a></div>
					<div class="form_link02"><a href="/inquiry/#check-purchase-contact">ご購入時に関するお問合せ</a></div>
					<div class="form_link03"><a href="/inquiry/#check-sample-order">サンプルのご依頼</a></div>
				</div>
			</div>
		<div id="contents-2nd2">

			<div class="title-box-offset">
				<div class="title offset_title">
					<h2 class="in_title1">オフセット輪転機用<h2>
					<h2 class="in_title2">消防法非該当製品<h2>
				</div>
				<h3 class="in_title3">Product lineup<h3>		
			</div>
			<div class="contents offset_contents">


			<div class="">
					<a href="/newitem/item.php#new_item06">
				<div class="box_lg" style="margin:50px 0 50px">
							<div class="left_boxin">
								<div class="num"><img src="/img/contents/offset_num01.png"></div>
								<div class="badges">
								</div>
								<div class="text">
									<p>オフセット輪転機用エッチ液</p>
									<h4 class="wk801"><span>SOLAIA WEB WK801</span></h4>
									<p>使用方法 添加量1.5%～3.5%でご使用ください</p>
								</div>
							</div>
							<div class="right_boxin">
								<div class="img"><img src="/img/contents/search_wk801.png"></div>
							</div>
					</div>
						</a>
				</div>
				<div class="">
					<a href="/newitem/item.php#new_item05">
						<div class="box_xl2 box_lg box_lg box_rightside mt-50 mb-50">
							<div class="left_boxin">
								<div class="num"><img src="/img/contents/offset_num02.png"></div>
								<div class="badges">
								</div>
								<div class="text">
									<p>除電滑走剤</p>
									<h4 class="cubos55s"><span>S-cubos 55S</span></h4>
									<p>使用方法 添加量5%～20%でご使用ください</p>
								</div>
							</div>
							<div class="right_boxin">
								<div class="img"><img src="/img/contents/search_scubos55s.png"></div>
							</div>
						</div>
					</a>
					<div class="f-c"></div>
				</div>				
				<div class="">
						<div class="box_lg box-01_lg2">
							<div class="left_boxin">
								<div class="num"><img src="/img/contents/offset_num03.png"></div>
								<div class="badges">
									<div class="badge4">Coming soon</div>
								</div>
								<div class="text">
									<p>インキローラー洗浄液</p>
									<h4 class=""><span></span></h4>
									<p>今後の新製品に、ご期待ください!</p>
								</div>
							</div>
							<div class="right_boxin">
								<div class="img"><img src="/img/contents/search_comingsoon.png"></div>
							</div>
						</div>
				</div>
				<div class="">
					<div class="box_xl2 box_lg box_lg box_rightside sp-mt">
						<a href="/newitem/item.php#new_item08">
							<div class="left_boxin">
								<div class="num"><img src="/img/contents/offset_num04.png"></div>
								<div class="badges">
								</div>
								<div class="text">
									<p>水棒洗浄液</p>
									<h4 class="px-25ex">セーフティーダンプキーパー <span>PK-25EX</span></h4>
									<p>使用方法 給水ローラー表面を拭き上げてください</p>
								</div>
							</div>
							<div class="right_boxin">
								<div class="img"><img src="/img/contents/search_pk25ex.png"></div>
							</div>
						</a>
						<div class="f-c"></div>
					</div>
				</div>
			</div>



		</div>
			<!--/contents-2nd-->

		</div>
		<!--/contents-->

		<!-- inc -->
		<?php include("../inc/footer.php"); ?>
		<!-- /inc -->

	</div>
	<!--/main-->

	<!-- inc -->
	<?php include("../inc/script.php"); ?>
	<!-- /inc -->

	<script src="https://unpkg.com/scrollreveal"></script>
	<script type="text/javascript">
        $(function(){
            ScrollReveal().reveal('.animateright', { distance: '200px', origin: 'right', viewFactor: '1', duration: '2000' });
            ScrollReveal().reveal('.animateleft', { distance: '200px', origin: 'left', viewFactor: '1', duration: '2000'});
        });
    </script>
</body>

</html>