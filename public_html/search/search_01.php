<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>オフセット輪転機用　お薦めラインアップ | お薦め製品 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!--pagetitle-->
		<!-- <div id="search-header" class="search-header01">
		<ul class="search-header__in">
					<li class="search-header__list">
						<a href="/inquiry/#check-product-contact"><img src="/img/contents/btn_search_header_01.png" alt=""/></a>
					</li>
					<li class="search-header__list">
						<a href="/inquiry/#check-purchase-contact"><img src="/img/contents/btn_search_header_02.png" alt=""/></a>
					</li>
					<li class="search-header__list">
						<a href="/inquiry/#check-sample-order"><img src="/img/contents/btn_search_header_03.png" alt=""/></a>
					</li>
				</ul>
		</div> -->
		<div id="search-header" class="search-header02">
		<ul class="search-header__in">
					<li class="search-header__list" id="serch-header1">
						<a href="/inquiry/#check-product-contact">製品に関するお問合せ</a>
					</li>
					<li class="search-header__list"  id="serch-header2">
						<a href="/inquiry/#check-purchase-contact">ご購入時に関するお問合せ</a>
					</li>
					<li class="search-header__list"  id="serch-header3">
						<a href="/inquiry/#check-sample-order">サンプルのご依頼</a>
					</li>
				</ul>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div class="bg_search">
		<div id="contents-2nd" class="contents-2nd1"> 
			
			<!--section-->
			<section id="sec-01" class="section-01 sec-search">
			<h2 class="title-search-in"><img src="/img/contents/search_items1_title.png" alt=""></h2>
			<ul class="list-search">
				<li class="search-single1 search-single1-1"><a href="/newitem/item.php#new_item06"><img src="/img/contents/search_items1_1.png" alt=""></a></li>
				<li class="search-single1 search-single1-2"><a href="/newitem/item.php#new_item05"><img src="/img/contents/search_items1_2.png" alt=""></a></li>
				<li class="search-single1 search-single1-3"><img src="/img/contents/search_items1_3.png" alt=""></li>
				<li class="search-single1 search-single1-4"><a href="/products/category03.php#sec-06"><img src="/img/contents/search_items1_4.png" alt=""></a></li>
				<li class="search-single1 search-single1-5"><a href="/newitem/item.php#new_item08"><img src="/img/contents/search_items1_5.png" alt=""></a></li>
			</ul>
			</section>
			<!--section--> 

			
		</div>
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->

</body>
</html>
