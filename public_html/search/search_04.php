<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>お薦め製品一覧表 | お薦め製品 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap" class="searchnew"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span class="eng">お薦め製品</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 		
		
	<!--contents-2nd-->
	<div >
	<div id="contents-2nd" > 
			<div class="title-box-04">
				<div class="title _title">
					<h2 class="in_title1">お薦め製品<h2>
					<h2 class="in_title2">一覧表<h2>
				</div>
				<h3 class="in_title3">Product lineup<h3>		
			</div>
		<div>
			<table class="search04_table search04_table1">
				<thead>
					<tr>
						<th rowspan="2" style="vertical-align:middle">用途</th>
						<th rowspan="2" style="vertical-align:middle">製品名</th>
						<th rowspan="2" style="vertical-align:middle">容量/規格</th>
						<th colspan="2">対応<br>機械</th>
						<th colspan="2">対応<br>インキ</th>
					</tr>
					<tr>
						<th style="width:5%">輪<br>転<br>機</th>
						<th style="width:5%">枚<br>葉<br>機</th>
						<th style="width:5%">油<br>性</th>
						<th style="width:5%">U<br>V</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td rowspan="3">湿し水エッチ液</td>
						<td class="ta-c">SOLAIA　WEB　WK801</td>
						<td class="ta-c">20L/ポリ容器<img src="/img/contents/search-04-item1.png"></td>
						<td class="ta-c">●</td>
						<td class="ta-c"></td>
						<td class="ta-c">●</td>
						<td class="ta-c"></td>
					</tr>
					<tr>
						<td class="ta-c">SOLAIA　517</td>
						<td class="ta-c"><span class="search04_tx-2row">20L/ポリ容器<br>10L/ポリ容器</span><img src="/img/contents/search-04-item5.png" class="search04__img-2bottle"></td>
						<td class="ta-c"></td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
					</tr>
					<tr>
						<td class="ta-c">SOLAIA　507</td>
						<td class="ta-c"><span class="search04_tx-2row">20L/ポリ容器<br>10L/ポリ容器</span><img src="/img/contents/search-04-item6.png" class="search04__img-2bottle"></td>
						<td class="ta-c"></td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
					</tr>
					<tr>
						<td rowspan="2">湿し水添加剤</td>
						<td class="ta-c">添加液 AG-A1</td>
						<td class="ta-c"><span class="amount-margin">14kg/斗缶</span><img src="/img/contents/search-04-item7.png"></td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
					</tr>
					<tr>
						<td class="ta-c">添加液 AG-U2</td>
						<td class="ta-c"><span class="amount-margin">14kg/斗缶</span><img src="/img/contents/search-04-item8.png"></td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
					</tr>
					<tr>
						<td rowspan="4">インキローラー洗浄液</td>
						<td class="ta-c">ロールクリン　KR-1</td>
						<td class="ta-c"><span class="amount-margin">18L/斗缶</span><img src="/img/contents/search-04-item9.png"></td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
						<td class="ta-c"></td>
					</tr>
					<tr>
						<td class="ta-c">セーフティーUVインキ洗浄液　SV-1</td>
						<td class="ta-c"><span class="amount-margin">16kg/斗缶</span><img src="/img/contents/search-04-item10.png"></td>
						<td class="ta-c"></td>
						<td class="ta-c">●</td>
						<td class="ta-c"></td>
						<td class="ta-c">●</td>
					</tr>
					<tr>
						<td class="ta-c">UVインキ洗浄液　KV-9</td>
						<td class="ta-c"><span class="amount-margin">18L/斗缶</span><img src="/img/contents/search-04-item11.png"></td>
						<td class="ta-c"></td>
						<td class="ta-c">●</td>
						<td class="ta-c"></td>
						<td class="ta-c">●</td>
					</tr>
					<tr>
						<td class="ta-c"><div class="new-icon"><div class="new-icon__in">NEW</div></div><br>ラバーメンテナンスクリーナーRB-2EX</td>
						<td class="ta-c"><span class="search04_tx-2row amount-margin2">18L/斗缶<br>1L/PET容器</span><img src="/img/contents/search-04-item200731.png" class="search04__img-2bottle"></td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
					</tr>
					<tr>
						<td>水棒洗浄液</td>
						<td class="ta-c">セーフティーダンプキーパー　PK-25EX</td>
						<td class="ta-c"><span class="search04_tx-2row amount-margin2">18L/斗缶<br>1L/PET容器</span><img src="/img/contents/search-04-item3.png" class="search04__img-2bottle"></td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
						<td class="ta-c">●</td>
					</tr>
					<tr>
						<td>除電滑走剤</td>
						<td class="ta-c"> S-cubos　55S</td>
						<td class="ta-c">17kg/複合容器<img src="/img/contents/search-04-item4.png"></td>
						<td class="ta-c">●</td>
						<td class="ta-c"></td>
						<td class="ta-c">●</td>
						<td class="ta-c"></td>
					</tr>
				</tbody>
			</table>
		</div>
			
	</div>
	</div>
	<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->

</body>
</html>