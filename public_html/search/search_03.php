<!doctype html>
<html lang="ja">

<head>
	<meta charset="utf-8">
	<title>枚葉機UVインキ用　お薦めラインアップ | お薦め製品 | 光陽化学工業</title>

	<!-- inc -->
	<?php include("../inc/head.php"); ?>
	<!-- /inc -->

</head>

<body id="top">

	<!-- inc -->
	<?php include("../inc/header.php"); ?>
	<!-- /inc -->

	<!--main-->
	<div id="main-wrap">

		<!--contents-->
		<div id="contents-wrap">

			<!--pagetitle-->
		<!-- <div id="header" class="search-header01">
				<ul class="search-header__in">
					<li class="search-header__list">
						<a href="/inquiry/#check-product-contact"><img src="/img/contents/btn_search_header_01.png" alt=""/></a>
					</li>
					<li class="search-header__list">
						<a href="/inquiry/#check-purchase-contact"><img src="/img/contents/btn_search_header_02.png" alt=""/></a>
					</li>
					<li class="search-header__list">
						<a href="/inquiry/#check-sample-order"><img src="/img/contents/btn_search_header_03.png" alt=""/></a>
					</li>
				</ul>
			</div> -->
			<div id="search-header" class="search-header02">
				<ul class="search-header__in">
					<li class="search-header__list">
						<a href="/inquiry/#check-product-contact">製品に関するお問合せ</a>
					</li>
					<li class="search-header__list">
						<a href="/inquiry/#check-purchase-contact">ご購入時に関するお問合せ</a>
					</li>
					<li class="search-header__list">
						<a href="/inquiry/#check-sample-order">サンプルのご依頼</a>
					</li>
				</ul>
			</div>
			<!--/pagetitle-->

			<!--contents-2nd-->
			<div class="bg_search">
				<div id="contents-2nd2">


		<!--section-->
		<section id="sec-01" class="section-01 sec-search">
			<div class="search_inbox">
				<img src="/img/contents/search_items3_.png" usemap="#map-items3" alt="枚葉機UVインキ用　お薦めラインアップ">
				<map name="map-items3">
					<area shape="poly" coords="17,145,530,145,530,428,17,438" href="/newitem/item.php#ni1807-02" onFocus="this.blur();" alt="消防法非該当,新発売,オフセット枚葉機用エッチ液,SOLAIA517,【使用方法】 添加量1.0%～3.0%でご使用ください">
					<area shape="poly" coords="543,168,1051,168,1051,461,543,461" href="/products/category01.php#sec-01" onFocus="this.blur();" alt="オフセット枚葉機用エッチ液,SOLAIA507,【使用方法】 添加量1.0%～3.0%でご使用ください,【 消防法 】 第四類第二石油類">
					<area shape="poly" coords="14,498,522,498,522,791,14,791" href="/products/category01.php#sec-03" onFocus="this.blur();" alt="IPA代替アルコール,添加液 AGｰA1,【使用方法】 IPAご使用時と同様ご使用ください,【 消防法 】 第四類第二石油類">
					<area shape="poly" coords="530,522,1038,522,1038,828,530,828" href="/products/category01.php#sec-03" onFocus="this.blur();" alt="IPA代替アルコール,添加液 AGｰU2,【使用方法】 IPAご使用時と同様ご使用ください,【 消防法 】 第四類第一石油類">
					<area shape="poly" coords="268,891,933,891,933,1176,268,1176" href="/products/category03.php#sec-07" onFocus="this.blur();" alt="消防法非該当,新発売,インキローラー洗浄液,セーフティーUVインキ洗浄液 SV-1,【 使用方法 】従来品と同様にご使用ください,【 消防法 】 第四類第二石油類">
					<area shape="poly" coords="38,1206,544,1206,544,1479,38,1479" href="/products/category03.php#sec-07" onFocus="this.blur();" alt="インキローラー洗浄液,UVインキ洗浄液 KV-9,【 使用方法 】従来品と同様にご使用ください,【 消防法 】 第四類第二石油類">
					<area shape="poly" coords="113,1536,969,1536,969,1848,113,1848" href="/newitem/item.php#new_item08" onFocus="this.blur();" alt="消防法非該当,新発売,水棒洗浄液,セーフティーダンプキーパー PK-25EX,【使用方法】 給水ローラー表面を拭き上げてください">
				</map>
			</div>
		</section>
		<!--section--> 


										


		</div>
			</div>
			<!--/contents-2nd-->

		</div>
		<!--/contents-->

		<!-- inc -->
		<?php include("../inc/footer.php"); ?>
		<!-- /inc -->

	</div>
	<!--/main-->

	<!-- inc -->
	<?php include("../inc/script.php"); ?>
	<!-- /inc -->

</body>

</html>