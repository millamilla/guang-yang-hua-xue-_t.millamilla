<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>お薦め製品 | 光陽化学工業</title>
<link href="test.css" rel="stylesheet">
<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span>お薦め製品</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/search/" itemprop="url">お薦め製品</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<div class="box_nav-search2_title">
					<h2 class="nav-search2_title"><span>光陽化学がお薦めする製品ラインアップ</span></h2>
					<p class="nav-search2_tex"><span>平成より準備を整え<br>令和元年からスタートした「消防法非該当製品」</span></p>
				</div>
				<div class="bg_circle">
				<ul class="nav-search2">
					<li class="single"><a href="/search/offset.php" width class="no-">
						<img src="/img/contents/top-slide1.png" alt=""/>
						</a></li>
					<li class="single"><a href="/search/uvink.php" class="no-">
						<img src="/img/contents/top-slide2.png" alt=""/>
						</a></li>
					<li class="single"><a href="/search/oilyink.php" class="no-">
						<img src="/img/contents/top-slide3.png" alt=""/>
						</a></li>
					<li class="single"><a href="/search/search_04.php" class="no-">
						<img src="/img/contents/focus-product_04.png" alt=""/>
						</a></li>
				</ul></div>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->

</body>
</html>
