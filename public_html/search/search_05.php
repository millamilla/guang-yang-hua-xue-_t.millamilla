<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>消防法非該当シリーズ | 目的別製品紹介 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-business">
			<div class="in">
				<h2><span>目的別製品紹介</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/search/" itemprop="url">お薦め製品</a></span> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/search/search_01.php" itemprop="url">オフセット輪転機用　お薦めラインアップ</a></span> </div>
			</div>
			<!--breadcrumb--> 
			<!--section-->
			<section id="sec-01" class="section-01">
				<h2 class="title-00">消防法 非該当 製品シリーズ&lt;第１弾&gt;</h2>
				<section class="section-02">
					<p>オフセット印刷工場では、多種の化学薬品が取り扱われており、その多くは消防法における危険物に該当しています。危険物の取り扱いに関しては、火災予防の観点から、取り扱い数量に応じて、消防法に基づく官庁への申請、危険物倉庫の設置、有資格の管理者の選任などの制約が設けられています。本製品シリーズは、工場から危険物を無くし、安全・安心で快適な環境作りにオススメの製品群です。</p>
				</section>
				<section class="section-02">
					<ul class="list-search">
						<li class="single">
							<p class="name">SOLAIA WEB WK801<br>&nbsp;</p>
							<dl class="spec">
								<dt>分類：</dt>
								<dd>輪転機用エッチ液</dd>
							</dl>
							<p class="img"><img src="/img/contents/pic_WK801_2.jpg" alt=""/></p>
							<p class="link"><a href="/newitem/item.php#new_item06">詳細はこちら</a></p>
						</li>
						<li class="single">
							<p class="name">SOLAIA 517<br>&nbsp;</p>
							<dl class="spec">
								<dt>分類：</dt>
								<dd>枚葉機用エッチ液</dd>
							</dl>
							<p class="img"><img src="/img/contents/pic_517_2.jpg" alt=""/></p>
							<p class="link"><a href="/newitem/item.php#new_item02">詳細はこちら</a></p>
						</li>
						<li class="single">
							<p class="name">セーフティーダンプキーパー<br>PK-25EX</p>
							<dl class="spec">
								<dt>分類：</dt>
								<dd>水棒洗浄液</dd>
							</dl>
							<p class="img"><img src="/img/contents/PIC_pk-25ex.jpg" alt=""/></p>
							<p class="link"><a href="/newitem/item.php#new_item08">詳細はこちら</a></p>
						</li>
						<li class="single">
							<p class="name">セーフティーブラン洗浄液<br>SB-1EX</p>
							<dl class="spec">
								<dt>分類：</dt>
								<dd>ブランケット洗浄液</dd>
							</dl>
							<p class="img"><img src="/img/contents/PIC_SB_1_EX.jpg" alt=""/></p>
							<p class="link"><a href="/products/category04.php#sec-01">詳細はこちら</a></p>
						</li>
						<li class="single">
							<p class="name">コーヨークリン KC-77<br>&nbsp;</p>
							<dl class="spec">
								<dt class="dt2">分類：</dt>
								<dd>インキローラー洗浄液 </dd>
							</dl>
							<p class="img"><img src="/img/contents/PIC_KC_77.jpg" alt=""/></p>
							<p class="link"><a href="/products/category03.php#kc-77">詳細はこちら</a></p>
						</li>
					</ul>
				</section>
			</section>
			<!--section--> 

		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/search/" itemprop="url">Focus Products</a></span> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/search/search_02.php" itemprop="url">オフセットUVインキ印刷向け製品シリーズ</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<h2 class="title-00">オフセットUV印刷向け 製品シリーズ</h2>
				<section class="section-02">
					<p>ＵＶによる硬化技術は、自動車や電化製品等のコーティング、お菓子のパッケージ、ジェルネイル（化粧品）など、身近な生活で多く使われています。ＵＶインキ印刷は、インキを瞬時に硬化することから、① 後加工工程に移行しやすく生産効率が高い、② 蒸着紙や樹脂フィルムなどの印刷媒体を選ばない、③ 皮膜が傷つきにくく意匠性に優れるなど、多くのメリットがあり、急速に普及しています。<br>
						本製品シリーズは、ＵＶインキへの適性に優れ、高品質印刷での安定生産を実現し、ＵＶインキ印刷の発展に貢献します。<br>
					</p>
				</section>
				<section class="section-02">
					<ul class="list-search">
						<li class="single">
							<p class="name">SOLAIA 507<br>&nbsp;</p>
							<dl class="spec">
								<dt>分類：</dt>
								<dd>枚葉機用エッチ液</dd>
							</dl>
							
							<p class="img"><img src="/img/contents/PIC_SOLAIA_507.jpg" alt=""/></p>
							<p class="link"><a href="/products/category01.php#sec-01">詳細はこちら</a></p>
						</li>
						<li class="single">
							<p class="name">セーフティーダンプキーパー<br>PK-25EX</p>
							<dl class="spec">
								<dt>分類：</dt>
								<dd>水棒洗浄液</dd>
							</dl>
							
							<p class="img"><img src="/img/contents/PIC_pk-25ex.jpg" alt=""/></p>
							<p class="link"><a href="/newitem/item.php#new_item08">詳細はこちら</a></p>
						</li>
						<li class="single">
							<p class="name">クリーンチェンジャー<br>CC-1</p>
							<dl class="spec">
								<dt class="dt2">分類：</dt>
								<dd>ローラーメンテナンス</dd>
							</dl>
							
							<p class="img"><img src="/img/contents/PIC_CC_1.jpg" alt=""/></p>
							<p class="link"><a href="/newitem/item.php#new_item03">詳細はこちら</a></p>
						</li>
					</ul>
				</section>
			</section>
			<!--section--> 

			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/search/" itemprop="url">Focus Products</a></span> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/search/search_03.php" itemprop="url">オフセット輪転印刷向け製品シリーズ</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<h2 class="title-00">オフセット輪転印刷向け 製品シリーズ</h2>
				<section class="section-02">
					<p>オフセット輪転印刷は、高速ロングランでの量産を特長としており、高品質を維持することが必要不可欠です。本製品シリーズは、湿し水に関わる各種課題を解決することで、湿し水を長期間クリーンに維持、印刷品質に関わる様々なトラブルの予防、作業環境と効率の改善など、安定した高品質印刷に貢献します。</p>
				</section>
				<section class="section-02">
					<ul class="list-search">
						<li class="single">
							<p class="name">SOLAIA WEB<br>WK711 クリア</p>
							<dl class="spec">
								<dt>分類：</dt>
								<dd>エッチ液</dd>
							</dl>
							
							<p class="img"><img src="/img/contents/PIC_SOLAIA_WK711.jpg" alt=""/></p>
							<p class="link"><a href="/newitem/item.php#new_item01">詳細はこちら</a></p>
						</li>
						<li class="single">
							<p class="name">SOLAIA WEB<br>WK801</p>
							<dl class="spec">
								<dt>分類：</dt>
								<dd>エッチ液</dd>
							</dl>
							
							<p class="img"><img src="/img/contents/pic_WK801_2.jpg" alt=""/></p>
							<p class="link"><a href="/newitem/item.php#new_item06">詳細はこちら</a></p>
						</li>
						<li class="single">
							<p class="name">セーフティーダンプキーパー<br>PK-25EX</p>
							<dl class="spec">
								<dt>分類：</dt>
								<dd>水棒洗浄液</dd>
							</dl>
							
							<p class="img"><img src="/img/contents/PIC_pk-25ex.jpg" alt=""/></p>
							<p class="link"><a href="/newitem/item.php#new_item08">詳細はこちら</a></p>
						</li>
					</ul>
				</section>
			</section>
			<!--section--> 

			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/search/" itemprop="url">Focus Products</a></span> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/search/search_04.php" itemprop="url">オフ輪印刷後加工用帯電防止液シリーズ</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<h2 class="title-00">オフ輪印刷 後加工用 帯電防止液シリーズ</h2>
				<section class="section-02">
					<p>静電気によるトラブルは、現在では電気・電子デバイス産業を始めとする全ての産業に及んでいます。製品への異物混入、製品どうしの付着や反発、デバイスの破壊、歩留まりの低下、静電着火など、全てが静電気が原因によるトラブルの事例です。<br>
					本シリーズは、最高水準の優れた除電性能により静電気の課題をクリアすることで、スタッカーバンドラーやシーター機での各種課題を解決し、安定した紙揃えを実現します。</p>
				</section>
				<section class="section-02">
					<ul class="list-search">
						<li class="single">
							<p class="name">除電滑走剤<br>
S-cubos 55S</p>
							<dl class="spec mls-2">
								<dt>特長：</dt>
								<dd>高滑走タイプ</dd>
							</dl>
							<p class="img"><img src="/img/contents/PIC_S_cubos_55_S.jpg" alt=""/></p>
							<p class="link"><a href="/newitem/item.php#new_item05">詳細はこちら</a></p>
						</li>
						<li class="single">
							<p class="name">除電滑走剤<br>
S-cubos 33N</p>
							<dl class="spec mls-2">
								<dt>特長：</dt>
								<dd>高除電タイプ</dd>
							</dl>
							<p class="img"><img src="/img/contents/PIC_S_cubos_33_N.jpg" alt=""/></p>
							<p class="link"><a href="/products/category05.php#sec-01">詳細はこちら</a></p>
						</li>
					</ul>
				</section>
			</section>
			<!--section--> 
			
















































			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/search/" itemprop="url">目的別製品紹介</a></span> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/search/search_05.php" itemprop="url">消防法非該当シリーズ</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<h2 class="title-00">インキローラー洗浄 シリーズ</h2>
				<section class="section-02">
					<p>ダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミー</p>
				</section>
				<section class="section-02">
					<h3 class="title-01">PICK　UPはこちら</h3>
					<ul class="list-search">
						<li class="single">
							<p class="name">ダミーダミーダミー</p>
							<p class="spec">特長特長特長特長特長特長特長特長特長特長</p>
							<p class="img"><img src="/img/contents/dummy_search.jpg" alt=""/></p>
							<p class="link"><a href="/">詳細はこちら</a></p>
						</li>
						<li class="single">
							<p class="name">ダミーダミーダミー</p>
							<p class="spec">特長特長特長特長特長特長特長特長特長特長</p>
							<p class="img"><img src="/img/contents/dummy_search.jpg" alt=""/></p>
							<p class="link"><a href="/">詳細はこちら</a></p>
						</li>
						<li class="single">
							<p class="name">ダミーダミーダミー</p>
							<p class="spec">特長特長特長特長特長特長特長特長特長特長</p>
							<p class="img"><img src="/img/contents/dummy_search.jpg" alt=""/></p>
							<p class="link"><a href="/">詳細はこちら</a></p>
						</li>
						<li class="single">
							<p class="name">ダミーダミーダミー</p>
							<p class="spec">特長特長特長特長特長特長特長特長特長特長</p>
							<p class="img"><img src="/img/contents/dummy_search.jpg" alt=""/></p>
							<p class="link"><a href="/">詳細はこちら</a></p>
						</li>
						<li class="single">
							<p class="name">ダミーダミーダミー</p>
							<p class="spec">特長特長特長特長特長特長特長特長特長特長</p>
							<p class="img"><img src="/img/contents/dummy_search.jpg" alt=""/></p>
							<p class="link"><a href="/">詳細はこちら</a></p>
						</li>
					</ul>
				</section>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->

</body>
</html>
