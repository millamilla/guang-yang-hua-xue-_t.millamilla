var windowWidth = $(window).width();
var windowSm = 640;
if (windowWidth <= windowSm) {
	
	$(document).ready(function(){
		
		$(".nav-parent > a").click(function(){
			//$(".nav-child").slideUp();
			//$(this).next(".nav-child").slideDown();
			if($(this).next(".nav-child").css('display') != 'block'){
				   $(".nav-child").slideUp();
			   }
			   $(this).next(".nav-child").slideToggle();
			
			return false;
		});
		
		$(".nav-child-on > a").click(function(){
			$(".nav-grandson").slideUp();
			$(this).next(".nav-grandson").slideDown();
			return false;
		});
		
	});
	
	$(window).on('load', function() {
		var url = $(location).attr('href');
		if(url.indexOf("#") != -1){
		var anchor = url.split("#");
		var target = $('#' + anchor[anchor.length - 1]);
		if(target.length){
		var pos = Math.floor(target.offset().top) - 61;
		$("html, body").animate({scrollTop:pos}, 100);
		}
		}
	});
	
} else {
	
	$(document).ready(function(){
		
		$(".nav-parent").hover(
			function () {
				$(this).addClass("on");
				$(this).children(".nav-child").show();
			},
			function () {
				$(this).removeClass("on");
				$(this).children(".nav-child").hide();	
			}
		);
		
		$(".nav-child-on").hover(
			function () {
				$(this).addClass("on");
				$(this).children(".nav-grandson").show();
			},
			function () {
				$(this).removeClass("on");
				$(this).children(".nav-grandson").hide();	
			}
		);
		
	});
}

$(document).ready(function(){
	
	//--------------------------
	$(".list-sds .single").matchHeight();
	
	//--------------------------
	$('#footer-wrap').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
		if (isInView) {
			$(".wrap-arrow").hide();
		} else {
			$(".wrap-arrow").show();
		}
	});
	
	//--------------------------
	var topBtn = $('#page-top');    
    topBtn.hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
	
	//--------------------------
	$('a[href^="#"]').click(function(){
		var speed = 500;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
	
	//--------------------------
	$('.no-link').click(function(){
		return false;
	});
	
	//--------------------------
	$('.no-href').click(function(){
		return false;
	});
	
	//--------------------------
	$('.toggle-head').click(function(){
		$(this).toggleClass("on");
		$(this).next(".toggle-body").slideToggle("slow");
	});
	
	//--------------------------
	$('.icon-animation').click(function(){
		$("#g-nav").slideToggle();
	});
	
	
	//--------------------------
	var $animation = $('.icon-animation');
    $animation.on('click',function(){
        if ($(this).hasClass('is-open')){
            $(this).removeClass('is-open');
        } else {
            $(this).addClass('is-open');
        }
    });
});

	$('#check-product-contact').click(function() {
		$("#check-product-contact_in").stop().slideToggle(this.checked);
		$("#check-all").show();
	});
	$('#check-purchase-contact').click(function() {
		$("#check-purchase-contact_in").stop().slideToggle(this.checked);
		$("#check-all").show();
	});
	$('#check-sample-order').click(function() {
		$("#check-sample-order_in").stop().slideToggle(this.checked);
		$("#check-all").show();
	});
$(function(){
  $("input[type='checkbox']").on('change', function(){
	  if($(".title_list:checked").length > 0){
		  $("#form_tit_tx1").css('display', 'none');
		  $("#form_tit_tx2").css('display', 'block');
		}else{
			$("#form_tit_tx1").css('display', 'block');
		    $("#form_tit_tx2").css('display', 'none');
	  }
	});
});
$(function(){
	if( location.hash == "#check-product-contact" || location.hash == "#check-purchase-contact" || location.hash == "#check-sample-order") {
		$(location.hash).prop("checked",true);
		$(location.hash+"_in").show();
		$("#check-all").css('display', 'block');
	}
});


$('input[name="sample_in-0').click(function() {
	if ($('input[name="sample_in-0"]').prop('checked')) {
		$("#num_select_ok0").show();
		$("#num_select_no0").hide();
	}else{
		$("#num_select_ok0").hide();
		$("#num_select_no0").show();
	}
});
$('input[name="sample_in-1').click(function() {
	if ($('input[name="sample_in-1"]').prop('checked')) {
		$("#num_select_ok1").show();
		$("#num_select_no1").hide();
	}else{
		$("#num_select_ok1").hide();
		$("#num_select_no1").show();
	}
});
$('input[name="sample_in-2').click(function() {
	if ($('input[name="sample_in-2"]').prop('checked')) {
		$("#num_select_ok2").show();
		$("#num_select_no2").hide();
	}else{
		$("#num_select_ok2").hide();
		$("#num_select_no2").show();
	}
});
$('input[name="sample_in-3').click(function() {
	if ($('input[name="sample_in-3"]').prop('checked')) {
		$("#num_select_ok3").show();
		$("#num_select_no3").hide();
	}else{
		$("#num_select_ok3").hide();
		$("#num_select_no3").show();
	}
});
$('input[name="sample_in-4').click(function() {
	if ($('input[name="sample_in-4"]').prop('checked')) {
		$("#num_select_ok4").show();
		$("#num_select_no4").hide();
	}else{
		$("#num_select_ok4").hide();
		$("#num_select_no4").show();
	}
});
$('input[name="sample_in-5').click(function() {
	if ($('input[name="sample_in-5"]').prop('checked')) {
		$("#num_select_ok5").show();
		$("#num_select_no5").hide();
	}else{
		$("#num_select_ok5").hide();
		$("#num_select_no5").show();
	}
});
$('input[name="sample_in-6').click(function() {
	if ($('input[name="sample_in-6"]').prop('checked')) {
		$("#num_select_ok6").show();
		$("#num_select_no6").hide();
	}else{
		$("#num_select_ok6").hide();
		$("#num_select_no6").show();
	}
});
$('input[name="sample_in-7').click(function() {
	if ($('input[name="sample_in-7"]').prop('checked')) {
		$("#num_select_ok7").show();
		$("#num_select_no7").hide();
	}else{
		$("#num_select_ok7").hide();
		$("#num_select_no7").show();
	}
});
$('input[name="sample_in-8').click(function() {
	if ($('input[name="sample_in-8"]').prop('checked')) {
		$("#num_select_ok8").show();
		$("#num_select_no8").hide();
	}else{
		$("#num_select_ok8").hide();
		$("#num_select_no8").show();
	}
});
$('input[name="sample_in-9').click(function() {
	if ($('input[name="sample_in-9"]').prop('checked')) {
		$("#num_select_ok9").show();
		$("#num_select_no9").hide();
	}else{
		$("#num_select_ok9").hide();
		$("#num_select_no9").show();
	}
});
$('input[name="sample_in-10').click(function() {
	if ($('input[name="sample_in-10"]').prop('checked')) {
		$("#num_select_ok10").show();
		$("#num_select_no10").hide();
	}else{
		$("#num_select_ok10").hide();
		$("#num_select_no10").show();
	}
});
$('input[name="sample_in-11').click(function() {
	if ($('input[name="sample_in-11"]').prop('checked')) {
		$("#num_select_ok11").show();
		$("#num_select_no11").hide();
	}else{
		$("#num_select_ok11").hide();
		$("#num_select_no11").show();
	}
});