<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>お得情報 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<div id="pagetitle" class="bg-lab">
			<div class="in">
				<h2><span>お得情報</span></h2>
			</div>
		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/valuableinfo/" itemprop="url">お得情報</a></span> </div>
			</div>
			<!--breadcrumb--> 
			
			<!--section-->
			<section id="sec-01" class="section-01">
				<div class="box-lab-01">
					<p class="text-03">いつも光陽化学工業のホームページをご活用くださりありがとうございます。</p>
					<p class="text-03">閲覧いただいている皆様にだけ、光陽化学から「お得な情報」を特別に配信します。</p>
					<p class="text-03">是非、下のバナーから気になる情報をチェックしてください。</p>
						<div class="valueinfo-box">
							<p class="valueinfo-box__btn"><a href="/campaign"><img src="/img/contents/btn_campaigninfo.png" alt=""/></a></p>
							<div class="">
						        <p class="text-04-1">キャンペーン内容を、お見逃しなく！！</p>
					        </div>
						</div>
						<div class="valueinfo-box">
							<p class="valueinfo-box__btn"><a href="/campaign_exist"><img src="/img/contents/btn_campaigninfo_2.png" alt=""/></a></p>
							<div class="">
						        <p class="text-04-1">キャンペーン内容を、お見逃しなく！！</p>
					        </div>
						</div>
						<div class="valueinfo-box">
							<p class="valueinfo-box__btn"><a href="environment.php"><img src="/img/contents/btn_environment.png" alt=""/></a></p>
							<div class="">
						        <p class="text-04-2">情報は随時更新します。乞うご期待！</p>
					        </div>
						</div>
					
				</div>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->

</body>
</html>
