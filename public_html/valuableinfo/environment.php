<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>さぁ、始めよう  環境対策 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->

</head>
<body id="top" class="env">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!--contents-2nd-->
		<div class="environment"> 
			
			<!--section-->
			<section id="sec-01" class="section-01"> 
							
				<!--section-->
				<section class="environment_in"> 
					<div><img src="/img/contents/env_top.png"></div>
					<h2 class="item-title"><span>第1弾</span>&nbsp;湿し水のノンアルコール、消防法非該当化を目指して！！</h2>
					<section class="env_index">
						<div class="env_index_in1">
							<p class="title">光陽化学がお薦めする湿し水環境別 ステップ</p>
							<ul class="stepmark delay-show">
								<li class="mark-all mark-1" style="opacity: 0.728259;">ステップ 1</li>
								<li class="mark-arrrow"><img src="/img/contents/env_index_arrow2.png"></li>
								<li class="mark-all mark-2"  style="opacity: 0.402369;">ステップ 2</li>
								<li class="mark-arrrow"><img src="/img/contents/env_index_arrow2.png"></li>
								<li class="mark-all mark-3"  style="opacity: 0.102369;">ステップ 3</li>
								<li class="mark-arrrow"><img src="/img/contents/env_index_arrow2.png"></li>
								<li class="mark-all mark-4"  style="opacity: 0;">ステップ 4</li>
							</ul>

							<div class="stepcontent">
							<p class="aim_step4"><span class="in_tex">ステップ4</span>を目指しましょう!!</p>
							<ul class="">
								<li class="step step1"><span class="in_tex-lg">ステップ1</span>&nbsp;＝&nbsp;アルコールをＩＰＡから<br class="sp-only"><span class="uline">代替アルコールのご提案</span>。</li>
								<li class="step step2"><span class="in_tex-lg">ステップ2</span>&nbsp;＝&nbsp;エッチ液を消防法の該当製品から、<br class="sp-only"><span class="uline">非該当製品のご提案</span>。</li>
								<li class="step step3"><span class="in_tex-lg">ステップ3</span>&nbsp;＝&nbsp;エッチ液は消防法の該当製品で<br class="sp-only"><span class="uline">ノンアルコールをご提案</span>。</li>
								<li class="step step4"><span class="in_tex-lg">ステップ4</span>&nbsp;＝&nbsp;<span class="uline">エッチ液を消防法の非該当製品</span>にして<br class="sp-only"><span class="uline">ノンアルコールをご提案</span>。</li>
							</ul>
						</div>
							<p class="kome">※光陽化学の基準として　ステップ1～ステップ4を設定しました</p>
						</div>
						<div class="env_index_in2">
							<div class="table-img">
								<img src="/img/contents/env_table.png" alt="step0からstep4までの簡易説明表">
							</div>
							<p class="shihyou">※ 弊社基準で、湿し水の環境を４段階に分けました。
								<img src="/img/contents/env_shihyou.png"></p>
						</div>
					</section>
					<section class="section1_main section_main">
						<div class="box-q">
							<ul>
								<li class="tit-q"><img src="/img/contents/env_question.png"></li>
								<li class="con-q">現在、御社の湿し水環境は、どのステップですか？</li>
								<li class="tit-q"><img src="/img/contents/env_question.png"></li>
							</ul>
						</div>
						<div class="box-step1 box-step">
							<h4><span class="step_mark">Step1</span>エッチ液は消防法の該当製品で、アルコールはIPAを使用している。</h4>
							<div class="step_in">
								<div class="step_in__img"><img src="/img/contents/env_step1-1.png"></div>
							</div>
						</div>
						<div class="box-step2 box-step">
							<h4><span class="step_mark">Step2</span>エッチ液は消防法の該当製品で、アルコールは代替アルコールを使用している。</h4>
							<div class="step_in">
								<div class="step_in__img"><img src="/img/contents/env_step1-2.png"></div>
							</div>
						</div>
						<div class="box-step3 box-step">
							<h4><span class="step_mark">Step3</span>エッチ液は消防法の該当製品で、アルコールは使用していない。</h4>
							<div class="step_in">
								<div class="step_in__img"><img src="/img/contents/env_step1-3.png"></div>
							</div>
						</div>
					</section>
					<div class="triangle"><div class="triangle_in"></div></div>
					<section class="section2_main section_main">
						<div class="box-a">
							<ul>
								<li class="tit-a"><img src="/img/contents/env_answer.png"></li>
								<li class="con-a">光陽化学がおすすめする、ステップ別製品</li>
								<li class="tit-a"><img src="/img/contents/env_answer.png"></li>
							</ul>
						</div>
						<div class="box-step1 box-step">
							<h4><span class="step_mark">Step1</span>エッチ液は消防法の該当製品で、<span class="uline">IPAを代替アルコールへ</span></h4>
							<div class="step_in">
								<div class="step_in__img"><img src="/img/contents/env_step2-1.png"></div>
							</div>
						</div>
						<div class="box-step2 box-step">
							<h4><span class="step_mark">Step2</span>アルコールは代替アルコールで、<span class="uline">エッチ液を消防法の非該当製品へ</span></h4>
							<div class="step_in">
								<div class="step_in__img"><img src="/img/contents/env_step2-2.png"></div>
							</div>
						</div>
						<div class="box-step3 box-step">
							<h4><span class="step_mark">Step3</span>消防法が該当のエッチ液で、<span class="uline">アルコールを使用しない環境へ</span></h4>
							<div class="step_in">
								<div class="step_in__img"><img src="/img/contents/env_step2-3.png"></div>
							</div>
						</div>
						<div class="box-step4 box-step">
							<h4><span class="step_mark">Step4</span><span class="uline">エッチ液を消防法の該当から非該当化へ</span></h4>
							<div class="step_in">
								<div class="step_in__img"><img src="/img/contents/env_step2-4.png"></div>
							</div>
						</div>
					</section>
				</section>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->
<script>
$(function() {
    $('ul.delay-show li')
        .css({opacity: 0})
        .each(function(i){
            $(this).delay(500 * i).animate({opacity:1}, 1500);
        });
});
</script>
</body>
</html>