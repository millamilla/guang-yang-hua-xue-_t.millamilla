<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>さぁ、始めよう  環境対策 | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->
<style>
.env_index_in2 .main-table{
  font-size: 1.6vw;
  margin: 3% auto 2%;
  width: 90%;
  border-collapse:separate;
}
.env_index_in2 .main-table tr th{
  background: #3273ba;
  color: white;
  padding: 10px 0;
}

.env_index_in2 .main-table .span-arrow img{
  width: 11%;
  vertical-align: middle;
}
.env_index_in2 .main-table tr.title1 th:first-child{
  border-right: white 4px solid;
  border-bottom: #1f4e79 4px solid;
  width: 15%;
}
.env_index_in2 .main-table tr.title1 th:last-child{
  border-bottom: white 4px solid;
}
.env_index_in2 .main-table tr.title2 th{
  border-bottom: #1f4e79 4px solid;
}
.env_index_in2 .main-table tr.title2 th:first-child{
  border-right: white 4px solid;
  width: 42.6%;
}
.env_index_in2 .main-table tr.step0 td{
  border-top: white 4px solid;
}
.env_index_in2 .main-table tr td{
  border-bottom: #002060 2px solid;
  text-align: center;
  padding: 14px 0;
  vertical-align: middle;
}
.env_index_in2 .main-table tr td span.medal1 img{
  width:23%;
  vertical-align: middle;
}
.env_index_in2 .main-table tr td span.medal2 img{
  width:30%;
  vertical-align: middle;
}
.env_index_in2 .main-table tr td span.medal1, .env_index_in2 .main-table tr td span.medal2{
  padding-right: 6px;
}
.env_index_in2 .main-table tr.step td.stepin_1,.env_index_in2 .main-table tr.step td.stepin_2{
  border-right: white 4px solid;
}
.env_index_in2 .main-table tr.step td.stepin_2,.env_index_in2 .main-table tr.step td.stepin_3{
  font-weight: bold;
}
.env_index_in2 .main-table tr.step3 td,.env_index_in2 .main-table tr.step4 td{
  padding: 10px 0 ;
}
.env_index_in2 .main-table tr.step4 td.stepin_2{
  position: relative;
}
.env_index_in2 .main-table tr.step0 td,.env_index_in2 .main-table tr.step1 td, .env_index_in2 .main-table tr.step2 td, .env_index_in2 .main-table tr.step3 td{
  height: 120px;
}
.env_index_in2 .main-table td.tex-h1{
  line-height: 3.8;
}
.env_index_in2 .main-table td.tex-h2{
  line-height: 1.8;
}
/* step0 */
.env_index_in2 .main-table tr.step0 td{
  background: #fff422a4;
}
.env_index_in2 .main-table tr.step0 td.stepin_1{
  color: #000;
  line-height: 1.5;
}
.env_index_in2 .main-table tr.step0 td.stepin_1 span{
  color: #002060;
  font-weight: bold;
}
/* step1 */
.env_index_in2 .main-table tr.step1 td{
  background: rgba(195,215,24,.7);
}
.env_index_in2 .main-table tr.step1 td.stepin_1{
  line-height: 1.5;
}
.env_index_in2 .main-table tr.step1 td.stepin_1 span{
  color: #002060;
  font-weight: bold;
}
/* step2 */
.env_index_in2 .main-table tr.step2 td{
  background: rgba(143,195,31,.7);
}
.env_index_in2 .main-table tr.step3 td{
  background: rgba(79,178,51,.7);
}
.env_index_in2 .main-table tr.step4 td{
  background: rgba(0,153,68,.7);
}
.env_index_in2 .main-table tr.step4 .right_up_border {
  background-image: linear-gradient(-45deg, /*角度*/
                   transparent 49%,
                   black 49%, /*斜線の色*/
                   black 51%, /*斜線の色*/
                   transparent 51%, 
                   transparent); 
}
.env_index_in2 .main-table td.stepin_1{
  color: #000;
}

.env_index_in2 .sub-table, .env_index_in2 .sub-table tr, .env_index_in2 .sub-table td{
  background-color: rgba(0, 0, 0, .0) !important;
  border: none !important;
  padding: 0;
  margin: 0;
  width: 100%;
}
.env_index_in2 .sub-table{
  width: 90%;
  margin: 0 auto;
}
.env_index_in2 .main-table tr td{
  color: #000;
}
.env_index_in2 .main-table tr.step4 td.stepin_2 .span-tex7{
  color: #faed00;
  font-size: 1.7vw;
  text-align: left;
}
.env_index_in2 .sub-table .span-tex1{
  vertical-align: middle;
  display: inline-block;
  line-height: 1.3;
  width: 100%;
}
.env_index_in2 .main-table .span-red{
  color: #ff0000;
}
.env_index_in2 .main-table .span-blue{
  color: #0070c1;
}
.env_index_in2 .sub-table tr td.span-mark{
  padding-left: 16%;
  width: 30%;
}
.env_index_in2 .main-table .span-mark img{
  vertical-align: middle;
  width: 60%;
}
.env_index_in2 .main-table tr .span-comingsoon{
  color:#faed00;
  font-size: 1.7vw;
}
@media screen and (max-width: 640px) {
.env_index_in2 .sub-table{
    width: 96%;
  }
  .env_index_in2 .main-table tr.step4 td.stepin_2 .span-tex7{
    font-size: 120%;
  }
  .env_index_in2 .sub-table .span-tex1{
    line-height: 1.3;
    width: 100%;
    font-size: 155%;
  }
  .env_index_in2 .sub-table tr td.span-mark{
    padding-left: 16%;
    width: 30%;
  }
  .env_index_in2 .main-table .span-mark img{
    width: 76%;
  }
  .env_index_in2 .main-table tr .span-comingsoon{
    font-size: 120%;
  }
  .env_index_in2 .main-table td.stepin_1{
    font-weight: bold;
    font-weight: 130%;
  }

}



</style>
</head>
<body id="top" class="env">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!--contents-2nd-->
		<div class="environment"> 
			
			<!--section-->
			<section id="sec-01" class="section-01"> 
							
				<!--section-->
				<section class="environment_in"> 
					<div><img src="/img/contents/env_top.png"></div>
					<h2 class="item-title"><span>第1弾</span>&nbsp;湿し水のノンアルコール、消防法非該当化を目指して！！</h2>
					<section class="env_index">
						<div class="env_index_in2">
							<table class="main-table">
								<thead>
									<tr class="title1">
										<th rowspan="2"></th>
										<th colspan="2">湿し水環境</th>
									</tr>
									<tr class="title2">
										<th>エッチ液</th>
										<th>アルコール</th>
									</tr>
								</thead>
								<tbody>
									<tr class="step step0">
										<td class="stepin_1">Step0</td>
										<td class="stepin_2">
											<table class="sub-table">
												<tr>
													<td class="span-mark"><img src="/img/contents/env_index_triangle.png"></td>
													<td class="span-tex span-tex1 tex-h1">(消防法　<span class="span-red">該当</span>)</td>
												</tr>
											</table>
										</td>
										<td class="stepin_3">
											<table class="sub-table">
												<tr>
													<td class="span-mark"><img src="/img/contents/env_index_batu.png"></td>
													<td class="span-tex span-tex1 tex-h1">(<span class="span-red">※IPAを使用</span>)</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr class="step step1">
										<td class="stepin_1">Step1<br><span>Standard</span></td>
										<td class="stepin_2">
											<table class="sub-table">
												<tr>
													<td class="span-mark"><img src="/img/contents/env_index_triangle.png"></td>
													<td class="span-tex span-tex1 tex-h1">(消防法　<span class="span-red">該当</span>)</td>
												</tr>
											</table>
										</td>
										<td class="stepin_3">
											<table class="sub-table">
												<tr>
													<td class="span-mark"><img src="/img/contents/env_index_triangle.png"></td>
													<td class="span-tex span-tex1 tex-h2">(消防法 <span class="span-red">該当</span>)<br>(有機則 非該当)</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr class="step step2">
										<td class="stepin_1"><span class="medal1"><img src="/img/contents/env_medal3.png"></span>Step2</td>
										<td class="stepin_2">
											<table class="sub-table">
												<tr>
													<td class="span-mark"><img src="/img/contents/env_index_circle.png"></td>
													<td class="span-tex span-tex1 tex-h1">(消防法 <span class="span-blue">非該当</span>)</td>
												</tr>
											</table>
										</td>
										<td class="stepin_3">
											<table class="sub-table">
												<tr>
													<td class="span-mark"><img src="/img/contents/env_index_triangle.png"></td>
													<td class="span-tex span-tex1 tex-h2">(消防法 <span class="span-red">該当</span>)<br>(有機則 非該当)</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr class="step step3">
										<td class="stepin_1"><span class="medal1"><img src="/img/contents/env_medal2.png"></span>Step3</td>
										<td class="stepin_2">
											<table class="sub-table">
												<tr>
													<td class="span-mark"><img src="/img/contents/env_index_circle.png"></td>
													<td class="span-tex span-tex1 tex-h1">(消防法　<span class="span-red">該当</span>)</td>
												</tr>
											</table>
										</td>
										<td class="stepin_3">
											<table class="sub-table">
												<tr>
													<td class="span-tex2">※添加剤なし</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr class="step step4">
										<td class="stepin_1"><span class="medal2"><img src="/img/contents/env_medal1.png"></span>Step4</td>
										<td class="stepin_2">
											<table class="sub-table">
												<tr><td class="span-tex7" colspan="2">完成</td></tr>
												<tr>
													<td class="span-mark"><img src="/img/contents/env_index_d-circle.png"></td>
													<td class="span-tex span-tex1">(消防法　<span class="span-blue">非該当</span>)</td>
												</tr>
												<tr><td class="span-comingsoon" colspan="2">Coming soon</td></tr>
											</table>
										</td>
										<td class="stepin_3 ">
												<table class="sub-table">
													<tr>
														<td class="span-tex2"><div></div>※添加剤なし</td>
													</tr>
												</table>
										</td>
									</tr>
								</tbody>
							</table>
							<p class="shihyou">※ 弊社基準で、湿し水の環境を４段階に分けました。<img src="/img/contents/env_shihyou.png"></p>
						</div>
					</section>
				</section>
			</section>
			<!--section--> 
			
		</div>
		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->
</body>
</html>