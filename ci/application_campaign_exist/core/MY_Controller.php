<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	function __construct()
    {
        parent::__construct();

		//デバッグ用表示設定
		//$this->output->enable_profiler(TRUE);

		//エラーメッセージ用設定
		$this->form_validation->set_error_delimiters('<p class="attention">', '</p>');


		//初期変数設定
		$this->data=array();
		$this->data["view_content"]=''; //メインコンテンツ
		$this->data["view_sidebar"]=''; //サイドバーコンテンツ
		$this->data["error_str"]=''; //サイドバーコンテンツ
		$this->data["ticket"]='';

		header('Cache-Control: max-age=900');

		//HTPPSでなければリダイレクト転送
		if (empty($_SERVER['HTTPS']) && config_item("https_falg")) {
			$redirect_url = config_item("https_url") . $_SERVER["REQUEST_URI"];
			header('Location: '. $redirect_url);
			exit;
		}

    }



	/**
	* テンプレート設定
    * @params void
    * @return void
    * @version 1.0
	*/
	function _load_view(){

		//基本テンプレート読み込み
		$this->data["tmp_head"]		=$this->load->view('common/head', $this->data , true); //headタグ内テンプレート読み込み
		$this->data["tmp_header"]	=$this->load->view('common/header', $this->data , true); //headerタグ内テンプレート読み込み
		$this->data["tmp_footer"]	=$this->load->view('common/footer', $this->data , true); //footerタグ内テンプレート読み込み
		$this->data["tmp_script"]	=$this->load->view('common/script', $this->data , true); //scriptタグ内テンプレート読み込み
		$this->data["tmp_sidebar"]	=($this->data["view_sidebar"]) ? $this->load->view($this->data["view_sidebar"], $this->data , true):''; //サイドバーテンプレート読み込み
		$this->data["tmp_content"]	=($this->data["view_content"]) ? $this->load->view($this->data["view_content"], $this->data , true):''; //メインコンテンツテンプレート読み込み


		$this->load->view('common/template', $this->data); //テンプレート表示出力


	}



	/**
	 * メール送信処理
    * @params array
    * @return bool
    * @version 1.0
	 */
	function _sendmail($send){

		$from = array( $send['from'] , $send['from_name']);
		$option = array( 'type'=> $send['type'] , 'option'=>$send['option']);

		$flg=qd_send_mail( $option , $send['to'] , $send['subject'] , $send['body'] , $from);
		//$flg2=qd_send_mail( $option , "takeda@kadofusa.com" , $send['subject'] , $send['body'] , $from);
        
        //log_message("debug",json_encode($send['to']));

		if($flg){
			return TRUE;
		}else{
			return FALSE;
		}

	}


	/**
	 * エラー画面を表示
    * @params void
    * @return void
    * @version 1.0
	 */
	function _error_404() {

		$this->output->set_status_header('404');
		$this->data["view_content"]="error/404";
		$this->_load_view();

	}

}