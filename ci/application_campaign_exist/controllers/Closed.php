<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Closed extends MY_Controller {

	public function __construct(){
		parent::__construct();
	}
	
	public function index()
	{
		$this->data["view_content"]='closed/index';
		$this->_load_view();
	}
}