<?php
defined("BASEPATH") OR exit("No direct script access allowed");

/**
* フォーム設定
*/

/**
* メールライブラリPATH
*/
$config["qd_mail_pass"] =APPPATH."../lib/qdmail/qdmail.php";

/**
* CSV PATH
*/
$config["csv_pass"] =APPPATH."../data/mail.csv";

if ($_SERVER["HTTP_HOST"]==="koyo-chemicals.t.millamilla.co.jp") {
	/**
	* ユーザー宛通知設定
	*/
    $config["http_url"] ="http://koyo-chemicals.t.millamilla.co.jp/";
    $config["https_url"] ="http://koyo-chemicals.t.millamilla.co.jp/";
	$config["url_path"] = "";

	/**
	* 管理者宛通知設定
	*/
	//管理者宛メールアドレス
	$config["admin_to"] =array( array( "webteam@millamilla.jp") );
	//メール件名
	$config["admin_subject"] ="サンプルキャンペーン";
	//管理者宛振り分け
}else{
	//本サーバ

	/**
	* ユーザー宛通知設定
	*/
	$config["http_url"] ="https://www.koyo-chemicals.co.jp";
	$config["https_url"] ="https://www.koyo-chemicals.co.jp";
	$config["url_path"] = "";

	/**
	* 管理者宛通知設定
	*/
	//管理者宛メールアドレス
	$config["admin_to"] =array( array( "n.tsukamoto@koyo-chemicals.net"),array( "koyo-otoiawase@koyo-chemicals.co.jp"));
	//$config["admin_to"] =array( array( "takeda@millamilla.jp") );
	//メール件名
	$config["admin_subject"] ="サンプルキャンペーン";
	//管理者宛振り分け
/*
    $config["admin_select_to"]=array("select"=>array(
                                            0=>array(
                                                array( "koyo-otoiawase@koyo-chemicals.co.jp")
                                            ),
                                            1=>array(
                                                array( "koyo-otoiawase@koyo-chemicals.co.jp")
                                            ),
                                            2=>array(
                                                array( "koyo-otoiawase@koyo-chemicals.co.jp")
                                            ),
                                            3=>array(
                                                array( "koyo-otoiawase@koyo-chemicals.co.jp")
                                            )
                                    ));
                                    */
}
	//確認メール送信
	$config["user_mail_flg"] =true;


	/**
	* ユーザー宛通知設定
	*/
	//差出人メールアドレス
	$config["user_from"] ="koyo-otoiawase@koyo-chemicals.co.jp";
    
	//差出人名
	$config["user_from_name"] ="光陽化学工業株式会社";
	//メール件名
	$config["user_subject"] ="サンプルのお申込みをいただきありがとうございました";


/**
* プロトコル取得
*/
$proto = "http" . ((isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") ? "s" : "") . "://";


/**
* URL、ディレクトリ関連設定
*/
$config["website_url"]									= $proto . $_SERVER["HTTP_HOST"] . $config["url_path"]; //会社サイトURL
$config["website_link_url"]				= $config["http_url"] . $config["url_path"]; //会社リンクURL

//HTTPSの切替
$config["https_falg"]= false;

/**
* エラーメッセージ文
*/
$config["error_str"]="エラーが発生しました。<br>再度サンプルお申込み画面よりご入力ください。";
$config["error_send_str"]="メール送信エラー。<br>再度サンプルお申込み画面よりご入力ください。";


/**
* 項目設定
*/

//都道府県リスト
$config["pref_list"] = array(""=>"都道府県を選択","北海道"=>"北海道","青森県"=>"青森県","岩手県"=>"岩手県","宮城県"=>"宮城県","秋田県"=>"秋田県","山形県"=>"山形県","福島県"=>"福島県","茨城県"=>"茨城県","栃木県"=>"栃木県","群馬県"=>"群馬県","埼玉県"=>"埼玉県","千葉県"=>"千葉県","東京都"=>"東京都","神奈川県"=>"神奈川県","山梨県"=>"山梨県","長野県"=>"長野県","新潟県"=>"新潟県","富山県"=>"富山県","石川県"=>"石川県","福井県"=>"福井県","岐阜県"=>"岐阜県","静岡県"=>"静岡県","愛知県"=>"愛知県","三重県"=>"三重県","滋賀県"=>"滋賀県","京都府"=>"京都府","大阪府"=>"大阪府","兵庫県"=>"兵庫県","奈良県"=>"奈良県","和歌山県"=>"和歌山県","鳥取県"=>"鳥取県","島根県"=>"島根県","岡山県"=>"岡山県","広島県"=>"広島県","山口県"=>"山口県","徳島県"=>"徳島県","香川県"=>"香川県","愛媛県"=>"愛媛県","高知県"=>"高知県","福岡県"=>"福岡県","佐賀県"=>"佐賀県","長崎県"=>"長崎県","熊本県"=>"熊本県","大分県"=>"大分県","宮崎県"=>"宮崎県","鹿児島県"=>"鹿児島県","沖縄県"=>"沖縄県");


$config['sample_in_num'] =array("1"=>"1","5"=>"5","10"=>"10","15"=>"15","20"=>"20");
$config['sample_in_num_disabled'] =array("-"=>"-");