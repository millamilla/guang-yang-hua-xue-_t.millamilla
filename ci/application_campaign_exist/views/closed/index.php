<div id="campaign_closed">
	<div class="att-box">
		<p>当ページにご訪問いただき誠にありがとうございます。<br>
			ご好評頂いておりました今回のサンプル定番品サンプルキャンペーンは、<br>
			2020年 12月28日の期限を持ちまして終了致しました。<br><br>
			当社キャンペーンは次回開催も予定しておりますのでご期待ください。</p>
	</div>
	<section class="section-03">
		<h3 class="toggle-head"><span>過去の定番品サンプルキャンペーン  対象製品</span></h3>
		<div class="toggle-body">
			<section class="section-lab">
			<h5 class="title-04 mb-10">2020年 9月 ～ 2020年 12月</h5>
				<ul class="list-01">
					<li>S-cubos 55S (17kg)</li>
					<li>S-cubos 33N (17kg)</li>
				</ul>
			</section>
		</div>
	</section><br>
	<p class="ta-c mb-20">定番品サンプルキャンペーン以外のサンプルは<a href="/inquiry/" class="link-01">問合せフォーム</a>よりご連絡ください</p>
</div>