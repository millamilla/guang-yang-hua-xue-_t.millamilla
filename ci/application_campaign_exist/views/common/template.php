<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>サンプルキャンペーン | 光陽化学工業</title>

<!-- inc -->
<?php include("../inc/head.php"); ?>
<!-- /inc -->
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
<style>
@mixin positionCenter() {
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}
.page {
  position: relative;
}
.target {
  @include positionCenter();
}
</style>
</head>
<body id="top">

<!-- inc -->
<?php include("../inc/header.php"); ?>
<!-- /inc --> 

<!--main-->
<div id="main-wrap"> 
	
	<!--contents-->
	<div id="contents-wrap"> 
		
		<!-- inc -->
		<?php include("../inc/search.php"); ?>
		<!-- /inc --> 
		
		<!--pagetitle-->
		<p class="mb30"></p>
		<div id="campaign_title">

		
	<img src="/img/contents/exist_top.png" style="max-width:2500px" alt="">


		</div>
		<!--/pagetitle--> 
		
		<!--contents-2nd-->
		<div id="contents-2nd_exist-hum"> 
			
			<!--breadcrumb-->
			<div id="breadcrumb">
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="/" itemprop="url"> <span itemprop="title">ホーム</span> </a> &gt; </div>
				<div itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><a href="/campaign_exist/" itemprop="url">定番品サンプルキャンペーン</a></span> </div>
			</div>
			<!--breadcrumb--> 
		</div> 

        <?php echo $tmp_content;?>

		<!--/contents-2nd--> 
		
	</div>
	<!--/contents--> 
	
	<!-- inc -->
	<?php include("../inc/footer.php"); ?>
	<!-- /inc --> 
	
</div>
<!--/main--> 

<!-- inc -->
<?php include("../inc/script.php"); ?>
<!-- /inc -->
<script src="https://unpkg.com/scrollreveal"></script>
<script type="text/javascript">
ScrollReveal().reveal('.animateleft', { 
  duration: 1600, 
  origin: 'left', 
  distance: '50px',
  reset: true   
});
ScrollReveal().reveal('.animateright', { 
  duration: 1600, 
  origin: 'right', 
  distance: '50px',
  reset: true   
});

$(function() {
    var w = $(window).width();
    var x = 640;
    if (w <= x) {
		var t1 = $('.first-box .text-box');
		var h1 = t1.outerHeight();
		$('.first-box .img-box img').height(h1).width(h1);
		var t2 = $('.second-box .text-box');
		var h2 = t2.outerHeight();
		$('.second-box .img-box img').height(h2).width(h2);
		var t3 = $('.third-box .text-box');
		var h3 = t3.outerHeight() - 50;
		$('.third-box .img-box img').height(h3).width(h3);
        }
});
</script>
</body>
</html>