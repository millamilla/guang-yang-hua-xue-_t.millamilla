<div class="form-exist">
			<div class="contents exist_contents">
				<div class="first-box">
					<div class="animateleft">
					<div class="text-box">
						<p class="tex-01"><< キャンペーン対象製品 >></p>
						<p class="tex-02"><span>除電滑走剤</span></p>
						<p class="tex-03"><span>S<ruby><rb>-cubos</rb><rp>（</rp><rt>エス・クーボ</rt><rp>）</rp></ruby> 55S／33N</span></p>
					</div>
					</div>
					<div class="img-box">
						<div class="img">
							<img src="/img/contents/exist_s-cubo1.png">
						</div>
					</div>
					<div class="f-c"></div>
				</div>
				<div class="second-box">
					<div class="img-box">
						<div class="img">
							<img src="/img/contents/exist_s-cubo2.png">
						</div>
					</div>
					<div class="animateright">
					<div class="text-box text-box-pt">
						<p class="tex-01">サンプルキャンペーン実施期間</p>
						<p class="tex-02"><span>2020.9.1～2020.12.28</span></p>
					</div>
					</div>
					<div class="f-c"></div>
				</div>
				<div class="third-box">
					<div class="animateleft">
						<div class="text-box">
							<p class="tex-04">サンプルキャンペーンは<br>&lt;&lt;特典が満載 &gt;&gt; </p>
							<div class="box_tex-05">
							<p class="tex-05">特典&#9312;<br class="sp-only"><span class="space1">1社、最大で20本のサンプルを</span><br><span class="space4">ご利用いただけます。</span><br><br class="sp-only">
							特典&#9313;<br class="sp-only"><span class="space1">アンケートにお答えいただいた</span><br><span class="space4">お客様へ、粗品を進呈いたします。</span></p>
							</div>
							<p class="tex-06"><span>※アンケート用紙はサンプルに同封させていただきます。</span></p>
						</div>
					</div>
					<div class="img-box">
						<div class="img">
							<img src="/img/contents/exist_s-cubo3.png">
						</div>
					</div>
					<div class="f-c"></div>
				</div>
			</div>
		</div>

<div id="contents-2nd"> 
<!--section-->
<section id="sec-01" class="section-01">

<div class="form_tit_before campaign_form">
    <div class="form_tit">
        <img src="/img/contents/form_topicon.png">
        <p id="" class="form_tit_intex" style="font-size:110%">下記フォームに必要事項を明記の上、<br>「内容を確認する」ボタンを押してください。</p>
    </div>
</div>
<ul class="mark-forms">
    <li class="mark-form-on">1.入力</li>
    <li class="mark-form-arrrow"><img src="/img/contents/form-arrow.png"></li>
    <li class="mark-form-off">2.確認</li>
    <li class="mark-form-arrrow"><img src="/img/contents/form-arrow.png"></li>
    <li class="mark-form-off">3.完了</li>
</ul>
<form action="/campaign_exist/confirm/#sec-01" method="post" accept-charset="utf-8">
<?php echo form_hidden(array('ticket'=>$ticket));?>
<div class="form-01 campaign_form">
	<div id="" class="title_list_in" style="display:block">
        <div class="form_tit_before2">
            <div class="form_tit_before_in3">
                <div class="form_tit">
                    <img src="/img/contents/form_topicon.png">
                    <p class="form_tit_intex">サンプルのご依頼</p>
                </div>
            </div>
        </div>
        <dl class="<?php echo (form_error('sample_in'))?'error':'';?>">
            <dt>お問合せ項目<br>(該当する製品名にチェックを入れ<br> ▼で本数を選択してください)<span class="red"><br>
                ※必須</span></dt>
            <dd class="form-gray-bg sample_num"><?= form_error('sample_in'); ?>
                <div class="form_sample_checkboxin color3">除電滑走剤</div>

                <? $name="sample_in-9"; $value="S-cubos 55S (17kg)" ?>
                <label><input type="checkbox" class="title_in_num" name="<?=$name;?>" value="<?=$value;?>" <?= set_checkbox($name, $value); ?>><?=$value;?></label>
                <? $name="sample_in_num9";?>

                <div id="num_select_ok9" class="drop-rb2ex" <?php if(set_value('sample_in-9') == "S-cubos 55S (17kg)"){?>style="display:block;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown($name, $this->config->item('sample_in_num'),set_value('sample_in_num9'));?><br>
                </div>
                <div id="num_select_no9" class="drop-rb2ex" <?php if(set_value('sample_in-9') == "S-cubos 55S (17kg)"){?>style="display:none;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown('sample_in_num_no', $this->config->item('sample_in_num_disabled'));?><br>
                </div>
				

                <? $name="sample_in-11"; $value="S-cubos 33N (17kg)" ?>
                <label><input type="checkbox" class="title_in_num" name="<?=$name;?>" value="<?=$value;?>" <?= set_checkbox($name, $value); ?>><?=$value;?></label>
                <? $name="sample_in_num11";?>

                <div id="num_select_ok11" class="drop-rb2ex" <?php if(set_value('sample_in-11') == "S-cubos 33N (17kg)"){?>style="display:block;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown($name, $this->config->item('sample_in_num'),set_value('sample_in_num11'));?><br>
                </div>
                <div id="num_select_no11" class="drop-rb2ex" <?php if(set_value('sample_in-11') == "S-cubos 33N (17kg)"){?>style="display:none;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown('sample_in_num_no', $this->config->item('sample_in_num_disabled'));?><br>
                </div>
			</dd>
        </dl>
        <dl class="<?php echo (form_error('sample_intex'))?'error':'';?>">
            <dt>その他ご要望</dt>
            <dd><?= form_error('sample_intex'); ?>
                <?php echo form_textarea(array('name'=>'sample_intex','value'=>set_value('sample_intex'), 'cols'=>50, 'rows'=>10));?> 
			</dd>
        </dl>
    </div>

    <div id="check-all" style="display:block">
	<dl class="<?php echo (form_error('company'))?'error':'';?>">
		<dt>送付先社名（全角記入）　<span class="red"><br>
                ※必須</dt>
		<dd>
			<?php echo  form_error('company'); ?>
			<?php echo form_input(array('name'=>'company','value'=>set_value('company'), 'size'=>'30'));?>
			<br> 例）○○○○株式会社
		</dd>
	</dl>
	<dl class="<?php echo (form_error('zip'))?'error':'';?>">
		<dt>送付先郵便番号（半角記入）　<span class="red"><br>
                ※必須</dt>
		<dd>
			<?php echo  form_error('zip'); ?>
			<?php echo form_input(array('name'=>'zip','value'=>set_value('zip'), 'size'=>'15','onKeyUp'=>"AjaxZip3.zip2addr(this,'','pref','address');"));?>
			<br> 例） 536-0025</dd>
	</dl>
	<dl class="<?php echo (form_error('pref'))?'error':'';?>">
		<dt>送付先都道府県　<span class="red"><br>
                ※必須</dt>
		<dd>
			<?php echo  form_error('pref'); ?>
			<?php echo form_dropdown('pref', $this->config->item('pref_list'),set_value('pref'));?>
		</dd>
	</dl>
	<dl class="<?php echo (form_error('address'))?'error':'';?>">
		<dt>送付先ご住所　<span class="red"><br>
                ※必須</dt>
		<dd>
			<?php echo  form_error('address'); ?>
			<?php echo form_input(array('name'=>'address','value'=>set_value('address'), 'size'=>'50'));?>
			<br> 例）大阪市城東区森之宮2-3-5 </dd>
	</dl>
	<dl class="<?php echo (form_error('tel'))?'error':'';?>">
		<dt>お電話番号　<span class="red"><br>
                ※必須</dt>
		<dd>
			<?php echo  form_error('tel'); ?>
			<?php echo form_input(array('name'=>'tel','value'=>set_value('tel'), 'size'=>'30'));?>
			<br> 例）06-6969-0026 </dd>
	</dl>
	<dl class="<?php echo (form_error('email'))?'error':'';?>">
		<dt>メ－ルアドレス（半角記入）　<br>
                <span class="red">※必須</span></dt>
		<dd>
			<?php echo  form_error('email'); ?>
			<?php echo form_input(array('name'=>'email','value'=>set_value('email'), 'oncopy'=>"alert('コピーできません');return false", 'size'=>50));?>
			<br> 例）taro@abcxyz.co.jp
		</dd>
	</dl>
	<dl class="<?php echo (form_error('email2'))?'error':'';?>">
		<dt>メ－ルアドレス（半角記入）確認用　<br>
                <span class="red">※必須</span></dt>
		<dd>
			<?php echo  form_error('email2'); ?>
			<?php echo form_input(array('name'=>'email2','value'=>set_value('email2'), 'onpaste'=>"alert('貼付けできません');return false", 'size'=>50));?>
		</dd>
	</dl>
	<dl class="<?php echo (form_error('name'))?'error':'';?>">
		<dt>お名前（全角記入）　<span class="red"><br>
                ※必須</span></dt>
		<dd>
			<?php echo  form_error('name'); ?>
			<?php echo form_input(array('name'=>'name','value'=>set_value('name'), 'size'=>'30'));?>
			<br> 例）山田太郎
		</dd>
	</dl>
	<dl class="<?php echo (form_error('supplier'))?'error':'';?>">
		<dt>貴社への印材納入業者様名</dt>
		<dd>
			<?php echo  form_error('supplier'); ?>
			<?php echo form_input(array('name'=>'supplier','value'=>set_value('supplier'), 'size'=>'40'));?>
		</dd>
	</dl>
	<div class="box-contact-01">
		<p class="text-02">下記フォームの情報を送信する前に、下記の「当社における個人情報の取り扱いについて」をご一読の上、<br> ご同意いただける場合は「同意する」にチェックを入れてください。
		</p>
		<p class="ind-01"><a href="/privacy/" target="_blank" class="btn-01"><span>当社における個人情報の取り扱いについて</span></a>
		</p>
		<p class="check">
			<?php echo  form_error('privacy'); ?>
			<label><input type="checkbox" name="privacy" value="privacy" <?php echo set_checkbox('privacy', 'privacy'); ?>>
					同意する</label>
		</p>
	</div>
	<div class="ta-c mb-15">
		<input class="btn-confirm" type="submit" value="内容を確認する">
	</div>
</div>
</div>
</form>
<p class="ta-c">キャンペーン以外のサンプルは<a href="/inquiry/" class="link-01">問合せフォーム</a>よりご連絡ください</p>


</section>
<!--section--> 
			
</div>
