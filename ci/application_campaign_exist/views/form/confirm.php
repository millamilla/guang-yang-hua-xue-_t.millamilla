<div id="contents-2nd"> 
<!--section-->
<section id="sec-01" class="section-01">


<p class="text-02">ご記入されました個人情報は、アンケートやご質問に対する返答・ご請求資料やご希望サンプルの発送用・キャンペーンや弊社製品のご紹介・登録内容の確認以外での使用は致しません。 ご質問等は、弊社お問合せ窓口までご連絡頂きますと共に、弊社「<a href="/privacy/">個人情報保護方針</a>」をご参照願います。 </p>
<div class="form_tit_before">
    <div class="form_tit">
        <img src="/img/contents/form_topicon.png">
        <p class="form_tit_intex">ご記入事項を確認頂き、「当社における個人情報の取り扱いについて」を<br>ご一読の上、「送信」ボタンを押してください。</p>
    </div>
</div>
<ul class="mark-forms">
    <li class="mark-form-off">1.入力</li>
    <li class="mark-form-arrrow"><img src="/img/contents/form-arrow.png"></li>
    <li class="mark-form-on">2.確認</li>
    <li class="mark-form-arrrow"><img src="/img/contents/form-arrow.png"></li>
    <li class="mark-form-off">3.完了</li>
</ul>
<div class="form-01">
	<dl>
		<dt>サンプルのご依頼</dt>
		<dd class="dd-confirm">
			<?php if(set_value('sample_in-9')||set_value('sample_in-11')){ ?><div class="form_sample_checkboxin color2">湿し水添加剤</div><?php };?>
			<?php if(set_value('sample_in-9')){?><? echo set_value('sample_in-9') ?>
			<div class="sample_in_num"><? echo set_value('sample_in_num9') ;?></div><br><?};?>
			<?php if(set_value('sample_in-11')){?><? echo set_value('sample_in-11') ?>
			<div class="sample_in_num"><? echo set_value('sample_in_num11') ;?></div><br><?};?>
		</dd>
	</dl>
	<dl>
		<dt>その他ご要望</dt>
		<dd class="dd-confirm"><?php echo nl2br(set_value('sample_intex'));?></dd>
	</dl>
	<dl>
		<dt>送付先社名</dt>
		<dd class="dd-confirm"><?php echo set_value('company');?></dd>
	</dl>
	<dl>
		<dt>送付先郵便番号</dt>
		<dd class="dd-confirm">〒<?php echo set_value('zip');?></dd>
	</dl>
	<dl>
		<dt>送付先都道府県</dt>
		<dd class="dd-confirm"><?php echo set_value('pref');?></dd>
	</dl>
	<dl>
		<dt>送付先ご住所</dt>
		<dd class="dd-confirm"><?php echo set_value('address');?></dd>
	</dl>
	<dl>
		<dt>お電話番号</dt>
		<dd class="dd-confirm"><?php echo set_value('tel');?></dd>
	</dl>
	<dl>
		<dt>メ－ルアドレス</dt>
		<dd class="dd-confirm"><?php echo set_value('email');?></dd>
	</dl>
	<dl>
		<dt>お名前</dt>
		<dd class="dd-confirm"><?php echo set_value('name');?></dd>
	</dl>
	<dl>
		<dt>貴社への印材納入業者様名</dt>
		<dd class="dd-confirm"><?php echo set_value('supplier');?></dd>
	</dl>
	<div class="contact_btn">
		<div class="contact_btn_center contact_btn_center_double clearfix"> <?php echo form_open('./', array('class'=>'w-50 f-l ta-c')); ?>
			<?
                    echo form_hidden("back",true);
                    foreach($this->input->post() as $key=>$val){
                        echo form_hidden(array($key=>form_prep($val)));
                    }
                    ?>
			<p class="btn-back-wrap">
				<input class="btn-back" name="hoge" type="submit" id="btnReturn" value="戻る">
			</p>
			<?php echo form_close()?> <?php echo form_open('complete', array('class'=>'w-50 f-r ta-c')); ?>
			<?
            foreach($this->input->post() as $key=>$val){
                echo form_hidden(array($key=>form_prep($val)));
            }
            ?>
			<p class="btn-submit">
				<input class="btn-confirm" name="hoge" type="submit" id="btnSubmit" value="送信">
			</p>
			<?php echo form_close()?> </div>
	</div>
</div>
</section>
<!--section--> 
</div>