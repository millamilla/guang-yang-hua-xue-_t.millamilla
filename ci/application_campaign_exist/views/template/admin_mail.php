定番品サンプルキャンペーンから下記のお申込みがありましたので、ご連絡致します。

■定番品サンプルキャンペーン内容

[日時]
<?php echo wrapMAIL(date("Y-m-d H:i:s")."\r\n");?>

[サンプルのご依頼]
<?php if(set_value('sample_in-9')||set_value('sample_in-11')){ ?>

除電滑走剤
<?php }?>
<?php if(set_value('sample_in-9'))echo wrapMAIL(set_value('sample_in-9').' '.set_value('sample_in_num9') . "\r\n");?>
<?php if(set_value('sample_in-11'))echo wrapMAIL(set_value('sample_in-11').' '.set_value('sample_in_num11') . "\r\n");?>

[その他ご要望]
<?php echo wrapMAIL(set_value("sample_intex")."\r\n");?>

[送付先社名]
<?php echo wrapMAIL(set_value('company')."\r\n");?>

[送付先住所]
〒<?php echo wrapMAIL(set_value('zip')."\r\n");?>
<?php echo wrapMAIL(set_value('pref'));?><?php echo wrapMAIL(set_value('address')."\r\n");?>

[お電話番号]
<?php echo wrapMAIL(set_value('tel')."\r\n");?>

[お名前]
<?php echo wrapMAIL(set_value('name')."\r\n");?>

[メールアドレス]
<?php echo wrapMAIL(set_value('email')."\r\n");?>

[貴社への印材納入業者様名]
<?php echo wrapMAIL(set_value('supplier')."\r\n");?>


以上ご連絡致しますので、ご対応よろしくお願い致します。


==　光陽化学工業株式会社　============================================

大阪本社：536-0025　大阪府大阪市城東区森之宮2-3-5
[TEL]06-6969-0026　[FAX]06-6969-1824

東京支社：103-0024　東京都中央区日本橋小舟町15-15　ルネ小舟町ビル２Ｆ
[TEL]03-3661-2700　[FAX]03-3661-2706

======================================================================
