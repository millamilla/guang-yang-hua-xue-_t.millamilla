<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Formの説明
 *
 * @package	Form
 * @author	Jumpei Takeda
 */
class Test extends MY_Controller {


	public function __construct()
	{
		parent::__construct();

	}

	/**
	* 入力画面
    * @params void
    * @return void
    * @version 1.0
	*/
	public function index()
	{
		//管理者宛通知設定
		$email='jmp342@gmail.com';
		
		$admin_body='お問合せから下記のお問合せがありましたので、ご連絡致します。

■お問合せ内容

[日時]
2018-05-22 17:04:07

[お名前]
大石紗恵子

[会社名]
キヤノン株式会社

[部署名]
画像情報技術１３開発室

[役職]
一般

[メールアドレス]
oishi.saeko@canon.co.jp

[ご住所]
〒212-8602
神奈川県神奈川県川崎市幸区柳町70-1

[お電話番号]
03-3758-2111
					
[お問合せ項目]
印刷事業に関する問合せ 

[お問合せ内容]
キヤノン株式会社の大石と申します。

貴社のニューシリンダークリーナーNC-3および、ニューフィルムクリーナーNF-3のSDSをお送りいただくことは可能でしょうか？
以前上記を購入し、成分を確認したいと考えております。

お手数をおかけいたしますが、よろしくお願いいたします。

[貴社への印材納入業者様名]



以上ご連絡致しますので、ご対応よろしくお願い致します。


==　光陽化学工業株式会社　============================================

大阪本社：536-0025　大阪府大阪市城東区森之宮2-3-5
[TEL]06-6969-0026　[FAX]06-6969-1824

東京支社：103-0024　東京都中央区日本橋小舟町15-15　ルネ小舟町ビル２Ｆ
[TEL]03-3661-2700　[FAX]03-3661-2706

======================================================================';
				$send['from']			= $email;
				$send['from_name']		= $email;
				$send['to']				= 'takeda@millamilla.jp';
				$send['subject']		= config_item('admin_subject');
				$send['body']			= $admin_body;
				$send['type']			= "text";
				$send['option']	= array( 'charset'=>array('UTF-8','base64'),'lineFeed'=>"\n");
		
		echo $this->_sendmail($send);
print_r($send);
	}


}