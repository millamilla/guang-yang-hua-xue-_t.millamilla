<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Formの説明
 *
 * @package	Form
 * @author	Jumpei Takeda
 */
class Form extends MY_Controller {


	public function __construct()
	{
		parent::__construct();
		//$this->load->helper('url');
		//redirect('/closed');

// trim										: 先頭と末尾の空白を削除します。
// required						: 必須項目にします。
// prep_for_form	: 特殊文字が変換されます。
// phone									: 電話番号チェック 日本版
//	katakana						:	カタカナのみ入力可能にします。
// 更に細かいバリテーション内容は controllers/MY_Form_validationm.php を参照

		//検証用設定
		$this->form_validation->set_rules('privacy',			'当社における個人情報の取り扱いについて',                  'trim|required|prep_for_form');

		$contact_title_flg=false;
		$sample_flg=true;
			if($this->input->post("sample_in-10")){
				$this->form_validation->set_rules('sample_in_num10','サンプルのご依頼 数量',    	 	'trim|required|prep_for_form');
				$sample_flg=false;
			}
			if($sample_flg){
				$this->form_validation->set_rules('sample_in',		'サンプルのご依頼',     			'trim|required|prep_for_form');
			}

		$this->form_validation->set_rules('sample_intex',	'サンプルのご依頼 その他ぎ要望',     'trim|prep_for_form');

		$this->form_validation->set_rules('company',		'送付先社名',					'trim|required|prep_for_form');
		$this->form_validation->set_rules('zip',			'送付先郵便番号',                  'trim|required|prep_for_form');
		$this->form_validation->set_rules('pref',			'送付先都道府県',                  'trim|required|prep_for_form');
		$this->form_validation->set_rules('address',		'送付先ご住所',                  'trim|required|prep_for_form');
		$this->form_validation->set_rules('tel',			'お電話番号',                  'trim|required|prep_for_form');
		$this->form_validation->set_rules('email',			'メールアドレス',                  'trim|required|valid_email');
		$this->form_validation->set_rules('email2',			'メールアドレス(確認)',                  'trim|required|valid_email|matches[email]');
		$this->form_validation->set_rules('name',			'お名前',                  'trim|required|prep_for_form');
		$this->form_validation->set_rules('supplier',			'貴社への印材納入業者様名',                  'trim|prep_for_form');

	}

	/**
	* 入力画面
    * @params void
    * @return void
    * @version 1.0
	*/
	public function index()
	{
		$this->data["view_content"]='form/input'; //メインコンテンツ


		//チケット生成
		$this->data["ticket"]=uniqid(mt_rand());
		$this->session->set_userdata('ticket',$this->data["ticket"]);

		$this->form_validation->run();

		$this->_load_view();

	}


	/**
	* 確認画面
    * @params void
    * @return void
    * @version 1.0
	*/
	public function confirm()
	{
		$this->data["view_content"]='form/confirm'; //メインコンテンツ
        
		//チケットチェック
		$this->data["ticket"]=$this->session->userdata('ticket');
		if(!$this->input->post('ticket') or $this->data["ticket"]!=$this->input->post('ticket')){
			$this->data["error_str"]=config_item('error_str');
			$this->session->set_userdata('ticket',"");
			$this->data["view_content"]='common/error'; //メインコンテンツ

		}else{
			
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->data["view_content"]='form/input'; //メインコンテンツ
			}else{

			}
		}
		$this->_load_view();
	}



	/**
	* 完了画面
    * @params void
    * @return void
    * @version 1.0
	*/
	public function complete()
	{
		//項目検証
		if ($this->form_validation->run() == FALSE)
		{
			$this->index();
		}
		else
		{
			//チケットチェック
			$ticket=$this->session->userdata('ticket');
			if(!$this->input->post('ticket') or $ticket!=$this->input->post('ticket')){
				$this->data["error_str"]=config_item('error_str');
				$this->data["view_content"]='common/error'; //メインコンテンツ

			}else{

				//送信日時設定
				$this->data["subtime"] = date('Y-m-d H:i:s', time());

				//テンプレート項目差し替え
				$admin_body = $this->load->view('template/admin_mail',$this->data,true);
				$user_body 	= $this->load->view('template/user_mail',$this->data,true);

				//日本語用メールライブラリ読み込み
				include(config_item('qd_mail_pass'));

				//管理者振り分け
				if(config_item('admin_select_to')){
					foreach(config_item('admin_select_to') as $key=>$row){
						foreach($row as $row_key=>$val){
							if(set_value($key)==$row_key){
								$admin_to=$val;
							}
						}

					}
				}else{
					$admin_to=config_item('admin_to');
				}


				//管理者宛通知設定
				$send['from']			= set_value('email');
				$send['from_name']		= set_value('email');
				$send['to']				= $admin_to;
				$send['subject']		= config_item('admin_subject');
				$send['body']			= $admin_body;
				$send['type']			= "text";
				$send['option']	= array( 'charset'=>array('UTF-8','base64'),'lineFeed'=>"\n");

				//ユーザー宛通知設定
				$send_user['from']		= config_item('user_from');
				$send_user['from_name']	= config_item('user_from_name');
				$send_user['to']		= set_value('email');
				$send_user['subject']	= config_item('user_subject');
				$send_user['body']		= $user_body;
				$send_user['type']		= "text";
				$send_user['option']	= array( 'charset'=>array('UTF-8','base64'),'lineFeed'=>"\n");

				//CSV出力
				if(config_item('csv_pass')){
					$this->load->helper('file');

					if(!file_exists(config_item('csv_pass'))){
						$csv_title 	= $this->load->view('template/csv_title',$this->data,true);
						$csv_title 	= mb_convert_encoding($csv_title,"SJIS-win", "UTF-8")."\r\n";
						write_file(config_item('csv_pass'), $csv_title);
					}

					$csv 	= $this->load->view('template/csv',$this->data,true);
					$csv 	= mb_convert_encoding($csv,"SJIS-win", "UTF-8")."\r\n";
					write_file(config_item('csv_pass'), $csv, 'a');
				}

				//管理者宛通知
				if($this->_sendmail($send)){

					//ユーザー宛通知
					$this->_sendmail($send_user);

					//セッション破棄
					$this->session->sess_destroy();
					$this->data["view_content"]='form/complete'; //メインコンテンツ
				}else{
					$this->data["error_str"]=config_item('error_send_str');
					$this->data["view_content"]='common/error'; //メインコンテンツ
				}


			}

		}
		$this->_load_view();
	}
}