<div id="campaign_closed">
	<div class="att-box">
		<p>当ページにご訪問いただき誠にありがとうございます。<br>
			ご好評頂いておりました今回のサンプル提供キャンペーンは、<br>
			2020年 4月30日の期限を持ちまして終了致しました。<br><br>
			当社キャンペーンは次回開催も予定しておりますのでご期待ください。</p>
	</div>
	<section class="section-03">
		<h3 class="toggle-head"><span>過去のサンプル提供キャンペーン  対象製品</span></h3>
		<div class="toggle-body">
			<section class="section-lab">
			<h5 class="title-04 mb-10">2019年 10月 ～ 2020年 4月</h5>
				<ul class="list-01">
					<li>セーフティーUVインキ洗浄液SV-1 (サンプル規格：1L)</li>
					<li>セーフティーダンプキーパーPK-25EX (サンプル規格：300ml)</li>
				</ul>
			</section>
			<section class="section-lab">
				<h5 class="title-04 mb-10">2018年 9月 ～ 2019年 2月</h5>
				<ul class="list-01">
					<li>SOLAIA WEB WK711 クリア (サンプル規格：20L)</li>
					<li>SOLAIA WEB WK801 (サンプル規格：20L)</li>
					<li>SOLAIA 517 (サンプル規格：10L)</li>
					<li>セーフティーダンプキーパー　PK-23 (サンプル規格：500mL)</li>
					<li>クリーンチェンジャー　CC-1 (サンプル規格：200mL)</li>
					<li>除電滑走剤　S-cubos 55S (サンプル規格：17kg)</li>
				</ul>
			</section>
		</div>
	</section><br>
	<p class="ta-c">キャンペーン以外のサンプルは<a href="/inquiry/" class="link-01">問合せフォーム</a>よりご連絡ください</p>
</div>