この度は光陽化学株式会社の新製品サンプルキャンペーンにお申し込みいただき
誠にありがとうございます。


──ご送信内容の確認─────────────────


[日時]
<?php echo wrapMAIL(date("Y-m-d H:i:s")."\r\n");?>

[サンプルのご依頼]
<?php if(set_value('sample_in-10')){ ?>

インキローラー洗浄液
<?php }?>
<?php if(set_value('sample_in-10'))echo wrapMAIL(set_value('sample_in-10').' '.set_value('sample_in_num10') . "\r\n");?>

[その他ご要望]
<?php echo wrapMAIL(set_value("sample_intex")."\r\n");?>

[送付先社名]
<?php echo wrapMAIL(set_value('company')."\r\n");?>

[送付先住所]
〒<?php echo wrapMAIL(set_value('zip')."\r\n");?>
<?php echo wrapMAIL(set_value('pref'));?><?php echo wrapMAIL(set_value('address')."\r\n");?>

[お電話番号]
<?php echo wrapMAIL(set_value('tel')."\r\n");?>

[お名前]
<?php echo wrapMAIL(set_value('name')."\r\n");?>

[メールアドレス]
<?php echo wrapMAIL(set_value('email')."\r\n");?>

[貴社への印材納入業者様名]
<?php echo wrapMAIL(set_value('supplier')."\r\n");?>


==　光陽化学工業株式会社　============================================

大阪本社：536-0025　大阪府大阪市城東区森之宮2-3-5
[TEL]06-6969-0026　[FAX]06-6969-1824

東京支社：103-0024　東京都中央区日本橋小舟町15-15　ルネ小舟町ビル２Ｆ
[TEL]03-3661-2700　[FAX]03-3661-2706

======================================================================