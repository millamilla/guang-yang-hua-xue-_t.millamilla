<?php
 
$lang['form_validation_required']           = "%s 欄は必須です。";
$lang['form_validation_isset']              = "%s 欄は空欄にできません。";
$lang['form_validation_valid_email']        = "%s 欄には正しいEmailアドレスを入力する必要があります。";
$lang['form_validation_valid_emails']       = "%s 欄には正しいEmailアドレスを入力する必要があります。";
$lang['form_validation_valid_url']          = "%s 欄には正しいURLを入力する必要があります。";
$lang['form_validation_valid_ip']           = "%s 欄には正しいIPアドレスを入力する必要があります。";
$lang['form_validation_min_length']         = "%s 欄は最低 %s 文字以上でなければなりません。";
$lang['form_validation_max_length']         = "%s 欄は %s 文字を超えてはいけません。";
$lang['form_validation_exact_length']       = "%s 欄は %s 文字でなければなりません。";
$lang['form_validation_alpha']              = "%s 欄には、半角アルファベット以外は入力できません。";
$lang['form_validation_alpha_numeric']      = "%s 欄には、半角英数字以外は入力できません。";
$lang['form_validation_alpha_dash']         = "%s 欄には、半角英数字、アンダースコア(_)、ハイフン(-)以外は入力できません。";
$lang['form_validation_numeric']            = "%s 欄には、数字以外は入力できません。";
$lang['form_validation_is_numeric']         = "%s 欄には、数値以外は入力できません。";
$lang['form_validation_integer']            = "%s 欄には、整数以外は入力できません。";
$lang['form_validation_regex_match']        = "%s 欄は、正しい形式ではありません。";
$lang['form_validation_matches']            = "%s 欄が %s 欄と一致しません。";
$lang['form_validation_is_natural']         = "%s 欄には、正の整数以外は入力できません。";
$lang['form_validation_is_natural_no_zero'] = "%s 欄には、0より大きい整数以外は入力できません。";
 
// 拡張
$lang['form_validation_single']             = "%s 欄には、半角文字以外は入力できません。";
$lang['form_validation_double']             = "%s 欄には、全角文字以外は入力できません。";
$lang['form_validation_hiragana']           = "%s 欄には、ひらがな以外は入力できません。";
$lang['form_validation_katakana']           = "%s 欄には、カタカナ以外は入力できません。";
$lang['form_validation_single_katakana']    = "%s 欄には、半角カタカナ以外は入力できません。";
$lang['form_validation_phone']              = "%s 欄は、正しい電話番号を入力する必要があります。";
$lang['form_validation_postal']             = "%s 欄に、正しい郵便番号を入力する必要があります。";
$lang['form_validation_creditcard_name']    = "%s 欄は、大文字の半角アルファベットで正しく入力してください。";
$lang['form_validation_ymd']                = "%s 欄は、正しい日付の形式で入力してください。";
$lang['form_validation_ymd_slash']                = "%s 欄は、正しい日付の形式で入力してください。";
$lang['form_validation_jis']                = "%s 欄には、環境依存文字・旧漢字がご利用いただけません。";
$lang['form_validation_pair']               = "%s 欄を入力した場合、%s 欄は必須となります。";
$lang['form_validation_url']               = "%s 欄は、正しいURLを入力する必要があります。";



$lang['form_validation_dubble_shorttag']                = "このタグはすでに利用されています。";
 
/* End of file form_validation_lang.php */
/* Location: ./application/language/japanese/form_validation_lang.php */