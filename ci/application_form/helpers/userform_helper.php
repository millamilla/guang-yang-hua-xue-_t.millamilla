<?php
/*
 * フォーム用関数
 */

if ( ! function_exists('wrapMAIL'))
{
    function wrapMAIL($str){  
		$str = html_entity_decode($str, ENT_QUOTES, 'UTF-8');
		$str = htmlspecialchars_decode($str);
		
		return $str;
	}
}

if ( ! function_exists('wrapCSV'))
{
    function wrapCSV($str){  
		$str = html_entity_decode($str, ENT_QUOTES, 'UTF-8');
		$str = htmlspecialchars_decode($str);
		$str = str_replace( '"', '""', $str );
		$str = str_replace("\n", chr(10), $str);
		$str = '"'.$str.'"';
		
		return $str;
	}
}