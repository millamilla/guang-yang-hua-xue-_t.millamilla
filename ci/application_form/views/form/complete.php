<div class="form_tit_before">
    <div class="form_tit">
        <img src="/img/contents/form_topicon.png">
        <p class="form_tit_intex">ご記入頂き有難う御座いました。</p>
    </div>
</div>
<ul class="mark-forms">
    <li class="mark-form-off">1.入力</li>
    <li class="mark-form-arrrow"><img src="/img/contents/form-arrow.png"></li>
    <li class="mark-form-off">2.確認</li>
    <li class="mark-form-arrrow"><img src="/img/contents/form-arrow.png"></li>
    <li class="mark-form-on">3.完了</li>
</ul>