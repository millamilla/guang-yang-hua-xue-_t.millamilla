<div class="form_tit_before">
    <div class="form_tit">
        <img src="/img/contents/form_topicon.png">
        <p class="form_tit_intex">ご記入事項を確認頂き、「当社における個人情報の取り扱いについて」を<br>ご一読の上、「送信」ボタンを押してください。</p>
    </div>
</div>
<ul class="mark-forms">
    <li class="mark-form-off">1.入力</li>
    <li class="mark-form-arrrow"><img src="/img/contents/form-arrow.png"></li>
    <li class="mark-form-on">2.確認</li>
    <li class="mark-form-arrrow"><img src="/img/contents/form-arrow.png"></li>
    <li class="mark-form-off">3.完了</li>
</ul>
<div class="form-01">

	<?php if(set_value("contact_title1")=="製品に関するお問合せ"){ ?>
	<dl>
		<dt>お問合せ項目</dt>
		<dd class="dd-confirm"><?= implode("<br>",set_value('contact_title_in1')); ?></dd>
	</dl>
	<dl>
		<dt>お問合せ内容</dt>
		<dd class="dd-confirm"><?php echo nl2br(set_value('contact_title_in1tex'));?></dd>
	</dl>
	<?php }?>
	<?php if(set_value("contact_title2")=="ご購入時に関するお問合せ"){ ?>
	<dl>
		<dt>ご購入に関するお問合せ</dt>
		<dd class="dd-confirm"><?= implode("<br>",set_value('contact_title_in2')); ?></dd>
	</dl>
	<dl>
		<dt>お問合せ内容</dt>
		<dd class="dd-confirm"><?php echo nl2br(set_value('contact_title_in2tex'));?></dd>
	</dl>
	<?php }?>
	<?php if(set_value("contact_title3")=="サンプルのご依頼"){ ?>
	<dl>
		<dt>サンプルのご依頼</dt>
		<dd class="dd-confirm">
			<?php if(set_value('sample_in-0')||set_value('sample_in-1')||set_value('sample_in-2')){ ?><div class="form_sample_checkboxin color1">湿し水エッチ液</div><?php };?>
			<?php if(set_value('sample_in-0')){?><? echo set_value('sample_in-0') ?>
			<div class="sample_in_num"><? echo set_value('sample_in_num0') ;?></div><br><?};?>
			<?php if(set_value('sample_in-1')){?><? echo set_value('sample_in-1') ?>
			<div class="sample_in_num"><? echo set_value('sample_in_num1') ;?></div><br><?};?>
			<?php if(set_value('sample_in-2')){?><? echo set_value('sample_in-2') ?>
			<div class="sample_in_num"><? echo set_value('sample_in_num2') ;?></div><br><?};?>
			<?php if(set_value('sample_in-3')||set_value('sample_in-4')){ ?><div class="form_sample_checkboxin color2">湿し水添加剤</div><?php };?>
			<?php if(set_value('sample_in-3')){?><? echo set_value('sample_in-3') ?>
			<div class="sample_in_num"><? echo set_value('sample_in_num3') ;?></div><br><?};?>
			<?php if(set_value('sample_in-4')){?><? echo set_value('sample_in-4') ?>
			<div class="sample_in_num"><? echo set_value('sample_in_num4') ;?></div><br><?};?>
			<?php if(set_value('sample_in-5')||set_value('sample_in-6')||set_value('sample_in-7')||set_value('sample_in-10')){ ?><div class="form_sample_checkboxin color3">インキローラー洗浄液</div><?php };?>
			<?php if(set_value('sample_in-5')){?><? echo set_value('sample_in-5') ?>
			<div class="sample_in_num"><? echo set_value('sample_in_num5') ;?></div><br><?};?>
			<?php if(set_value('sample_in-6')){?><? echo set_value('sample_in-6') ?>
			<div class="sample_in_num"><? echo set_value('sample_in_num6') ;?></div><br><?};?>
			<?php if(set_value('sample_in-7')){?><? echo set_value('sample_in-7') ?>
			<div class="sample_in_num"><? echo set_value('sample_in_num7') ;?></div><br><?};?>
			<?php if(set_value('sample_in-10')){?><? echo set_value('sample_in-10') ?>
			<div class="sample_in_num"><? echo set_value('sample_in_num10') ;?></div><br><?};?>
			<?php if(set_value('sample_in-8')){ ?><div class="form_sample_checkboxin color4">水棒洗浄液</div><?php };?>
			<?php if(set_value('sample_in-8')){?><? echo set_value('sample_in-8') ?>
			<div class="sample_in_num"><? echo set_value('sample_in_num8') ;?></div><br><?};?>
			<?php if(set_value('sample_in-9')){ ?><div class="form_sample_checkboxin color5">除電滑走剤</div><?php };?>
			<?php if(set_value('sample_in-9')){?><? echo set_value('sample_in-9') ?>
			<div class="sample_in_num"><? echo set_value('sample_in_num9') ;?></div><br><?};?>
		</dd>
	</dl>
	<dl>
		<dt>その他ご要望</dt>
		<dd class="dd-confirm"><?php echo nl2br(set_value('sample_intex'));?></dd>
	</dl>
	<?php }?>
	<dl>
		<dt>お名前</dt>
		<dd class="dd-confirm"><?php echo set_value('name');?></dd>
	</dl>
	<dl>
		<dt>所属機関</dt>
		<dd class="dd-confirm"><?php echo set_value('company');?></dd>
	</dl>
	<dl>
		<dt>所属部署</dt>
		<dd class="dd-confirm"><?php echo set_value('section');?></dd>
	</dl>
	<dl>
		<dt>役職</dt>
		<dd class="dd-confirm"><?php echo set_value('position');?></dd>
	</dl>
	<dl>
		<dt>メ－ルアドレス</dt>
		<dd class="dd-confirm"><?php echo set_value('email');?></dd>
	</dl>
	<dl>
		<dt>郵便番号</dt>
		<dd class="dd-confirm">〒<?php echo set_value('zip');?></dd>
	</dl>
	<dl>
		<dt>都道府県</dt>
		<dd class="dd-confirm"><?php echo set_value('pref');?></dd>
	</dl>
	<dl>
		<dt>ご住所</dt>
		<dd class="dd-confirm"><?php echo set_value('address');?></dd>
	</dl>
	<dl>
		<dt>お電話番号</dt>
		<dd class="dd-confirm"><?php echo set_value('tel');?></dd>
	</dl>
	<dl>
		<dt>貴社への印材納入業者様名</dt>
		<dd class="dd-confirm"><?php echo set_value('supplier');?></dd>
	</dl>
	<div class="contact_btn">
		<div class="contact_btn_center contact_btn_center_double clearfix"> <?php echo form_open('./', array('class'=>'w-50 f-l ta-c')); ?>
			<?
                    echo form_hidden("back",true);
                    foreach($this->input->post() as $key=>$val){
                        echo form_hidden(array($key=>form_prep($val)));
                    }
                    ?>
			<p class="btn-back-wrap">
				<input class="btn-back" name="hoge" type="submit" id="btnReturn" value="戻る">
			</p>
			<?php echo form_close()?> <?php echo form_open('complete', array('class'=>'w-50 f-r ta-c')); ?>
			<?
            foreach($this->input->post() as $key=>$val){
                echo form_hidden(array($key=>form_prep($val)));
            }
            ?>
			<p class="btn-submit">
				<input class="btn-confirm" name="hoge" type="submit" id="btnSubmit" value="送信">
			</p>
			<?php echo form_close()?> </div>
	</div>
</div>
