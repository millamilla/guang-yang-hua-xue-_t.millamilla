<div class="form_tit_before">
    <div class="form_tit">
        <img src="/img/contents/form_topicon.png">
        <p id="form_tit_tx1" class="form_tit_intex">下記の「お問合せ内容」よりお選びください</p>
        <p id="form_tit_tx2" class="form_tit_intex_checked" style="font-size:110%">下記フォームに必要事項を明記の上、<br>「内容を確認する」ボタンを押してください。</p>
    </div>
</div>
<ul class="mark-forms">
    <li class="mark-form-on">1.入力</li>
    <li class="mark-form-arrrow"><img src="/img/contents/form-arrow.png"></li>
    <li class="mark-form-off">2.確認</li>
    <li class="mark-form-arrrow"><img src="/img/contents/form-arrow.png"></li>
    <li class="mark-form-off">3.完了</li>
</ul>

<?php echo form_open('confirm'); ?>
<?php echo form_hidden(array('ticket'=>$ticket));?>
<div class="form-01">
    <div class="<?php echo (form_error('contact_title1') or form_error('contact_title2') or form_error('contact_title3'))?'error':'';?>" style="margin-bottom:15px">
        <div><span style="font-weight:bold">お問合せ内容</span><span class="red"><br>※必須</span></div>
        <div id="choose-content"><?php echo  form_error('contact_title1'); ?><?php echo  form_error('contact_title2'); ?><?php echo  form_error('contact_title3'); ?>
            <label class="" ><input type="checkbox" id="check-product-contact" class="title_list" name="contact_title1" value="製品に関するお問合せ"  <?= set_checkbox('contact_title1', '製品に関するお問合せ'); ?>> 製品に関するお問合せ</label><br>                 
            <label><input type="checkbox" id="check-purchase-contact" class="title_list" name="contact_title2" value="ご購入時に関するお問合せ"  <?= set_checkbox('contact_title2', 'ご購入時に関するお問合せ'); ?>> ご購入時に関するお問合せ</label><br>
            <label><input type="checkbox" id="check-sample-order" class="title_list" name="contact_title3" value="サンプルのご依頼"  <?= set_checkbox('contact_title3', 'サンプルのご依頼'); ?>> サンプルのご依頼</label>
        </div>
    </div>


    <div id="check-product-contact_in" class="title_list_in" <?php if(set_value('contact_title1') == "製品に関するお問合せ"){?>style="display:block;"<?php }?>>
        <div class="form_tit_before2">
        <div class="form_tit_before_in1">
            <div class="form_tit">
                <img src="/img/contents/form_topicon.png">
                <p class="form_tit_intex">製品に関するお問合せ</p>
            </div>
            </div>
        </div>
        <dl class="<?php echo (form_error('contact_title_in1'))?'error':'';?>">
            <dt>お問合せ項目<br><span class="font_small">(該当する内容にチェックを入れてください)</span><span class="red"><br>
                ※必須</span></dt>
            <dd class="form-gray-bg"><?= form_error('contact_title_in1[]'); ?>
				<?php $name="contact_title_in1"; foreach(config_item($name) as $key=>$val){ ?>
				<label class="" for="<?php echo $name;?><?=$key;?>"><input type="checkbox" name="<?php echo $name;?>[]" id="<?php echo $name;?><?=$key;?>" value="<?=$val;?>" <?php if(@in_array($val,$this->input->post($name))){ echo "checked" ; }?>><?php echo $val;?></label><br>
				<?php } ?>
			</dd>
        </dl>
        <dl class="<?php echo (form_error('contact_title_in1tex'))?'error':'';?>">
            <dt>お問合せ内容<span class="red"><br>※必須</span></dt>
            <dd><?= form_error('contact_title_in1tex'); ?>
                <?php echo form_textarea(array('name'=>'contact_title_in1tex','value'=>set_value('contact_title_in1tex'), 'cols'=>50, 'rows'=>10));?> 
			</dd>
        </dl>
    </div>     



    <div id="check-purchase-contact_in" class="title_list_in" <?php if(set_value('contact_title2') == "ご購入時に関するお問合せ"){?>style="display:block;"<?php }?>>
        <div class="form_tit_before2">
        <div class="form_tit_before_in2">
            <div class="form_tit">
                <img src="/img/contents/form_topicon.png">
                <p class="form_tit_intex">ご購入時に関するお問合せ</p>
            </div>
            </div>
        </div>
        <dl class="<?php echo (form_error('contact_title_in2'))?'error':'';?>">
            <dt>お問合せ項目<br><span class="font_small">(該当する内容にチェックを入れてください)</span><span class="red"><br>
                ※必須</span></dt>
            <dd class="form-gray-bg"><?= form_error('contact_title_in2[]'); ?>
				<?php $name="contact_title_in2"; foreach(config_item($name) as $key=>$val){ ?>
				<label class="" for="<?php echo $name;?><?=$key;?>"><input type="checkbox" name="<?php echo $name;?>[]" id="<?php echo $name;?><?=$key;?>" value="<?=$val;?>" <?php if(@in_array($val,$this->input->post($name))){ echo "checked" ; }?>><?php echo $val;?></label><br>
				<?php } ?>
			</dd>
        </dl>
        <dl class="<?php echo (form_error('contact_title_in2tex'))?'error':'';?>">
            <dt>お問合せ内容<span class="red"><br>※必須</span></dt>
            <dd><?= form_error('contact_title_in2tex'); ?>
                <?php echo form_textarea(array('name'=>'contact_title_in2tex','value'=>set_value('contact_title_in2tex'), 'cols'=>50, 'rows'=>10));?> 
			</dd>
        </dl>
    </div>


    <div id="check-sample-order_in" class="title_list_in" <?php if(set_value('contact_title3') == "サンプルのご依頼"){?>style="display:block;"<?php }?>>
        <div class="form_tit_before2">
            <div class="form_tit_before_in3">
                <div class="form_tit">
                    <img src="/img/contents/form_topicon.png">
                    <p class="form_tit_intex">サンプルのご依頼</p>
                </div>
            </div>
        </div>
        <dl class="<?php echo (form_error('sample_in'))?'error':'';?>">
            <dt>お問合せ項目<br>(該当する製品名にチェックを入れ<br> ▼で本数を選択してください)<span class="red"><br>
                ※必須</span></dt>
            <dd class="form-gray-bg sample_num"><?= form_error('sample_in'); ?>
            <div class="form_sample_checkboxin color1">湿し水エッチ液</div>

                <? $name="sample_in-0"; $value="SOLAIA WEB WK801(20L)";?>
                <label><input type="checkbox" name="<?=$name;?>" value="<?=$value;?>" <?= set_checkbox($name, $value); ?>><?=$value;?></label>
                <? $name="sample_in_num0";?>
                <div id="num_select_ok0" <?php if(set_value('sample_in-0') == "SOLAIA WEB WK801(20L)"){?>style="display:block;"<?php }?> >
                    <?php echo form_error('sample_in_num'); ?><?php echo form_dropdown($name, $this->config->item('sample_in_num'),set_value('sample_in_num0'));?><br>
                </div>
                <div id="num_select_no0" <?php if(set_value('sample_in-0') == "SOLAIA WEB WK801(20L)"){?>style="display:none;"<?php }?>>
                    <?php echo form_error('sample_in_num'); ?><?php echo form_dropdown('sample_in_num_no', $this->config->item('sample_in_num_disabled'));?><br>
                </div>
                    
                <? $name="sample_in-1"; $value="SOLAIA 517 (10L)" ?>
                <label><input type="checkbox" class="title_in_num" name="<?=$name;?>" value="<?=$value;?>" <?= set_checkbox($name, $value); ?>><?=$value;?></label>
                <? $name="sample_in_num1";?>
                <div id="num_select_ok1" <?php if(set_value('sample_in-1') == "SOLAIA 517 (10L)"){?>style="display:block;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown($name, $this->config->item('sample_in_num'),set_value('sample_in_num1'));?><br>
                </div>
                <div id="num_select_no1" <?php if(set_value('sample_in-1') == "SOLAIA 517 (10L)"){?>style="display:none;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown('sample_in_num_no', $this->config->item('sample_in_num_disabled'));?><br>
                </div>

                <? $name="sample_in-2"; $value="SOLAIA 507 (10L)" ?>
                <label><input type="checkbox" class="title_in_num" name="<?=$name;?>" value="<?=$value;?>" <?= set_checkbox($name, $value); ?>><?=$value;?></label>
                <? $name="sample_in_num2";?>
                <div id="num_select_ok2" <?php if(set_value('sample_in-2') == "SOLAIA 507 (10L)"){?>style="display:block;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown($name, $this->config->item('sample_in_num-2'),set_value('sample_in_num2'));?><br>
                </div>
                <div id="num_select_no2" <?php if(set_value('sample_in-2') == "SOLAIA 507 (10L)"){?>style="display:none;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown('sample_in_num_no', $this->config->item('sample_in_num_disabled'));?><br>
                </div>

                <div class="form_sample_checkboxin color2">湿し水添加剤</div>
                <? $name="sample_in-3"; $value="添加剤 AG-A1 (14kg)" ?>
                <label><input type="checkbox" class="title_in_num" name="<?=$name;?>" value="<?=$value;?>" <?= set_checkbox($name, $value); ?>><?=$value;?></label>
                <? $name="sample_in_num3";?>
                <div id="num_select_ok3" <?php if(set_value('sample_in-3') == "添加剤 AG-A1 (14kg)"){?>style="display:block;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown($name, $this->config->item('sample_in_num-2'),set_value('sample_in_num3'));?><br>
                </div>
                <div id="num_select_no3" <?php if(set_value('sample_in-3') == "添加剤 AG-A1 (14kg)"){?>style="display:none;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown('sample_in_num_no', $this->config->item('sample_in_num_disabled'));?><br>
                </div>

                <? $name="sample_in-4"; $value="添加剤 AG-U2 (14kg)" ?>
                <label><input type="checkbox" class="title_in_num" name="<?=$name;?>" value="<?=$value;?>" <?= set_checkbox($name, $value); ?>><?=$value;?></label>
                <? $name="sample_in_num4";?>
                <div id="num_select_ok4" <?php if(set_value('sample_in-4') == "添加剤 AG-U2 (14kg)"){?>style="display:block;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown($name, $this->config->item('sample_in_num-2'),set_value('sample_in_num4'));?><br>
                </div>
                <div id="num_select_no4" <?php if(set_value('sample_in-4') == "添加剤 AG-U2 (14kg)"){?>style="display:none;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown('sample_in_num_no', $this->config->item('sample_in_num_disabled'));?><br>
                </div>

                <div class="form_sample_checkboxin color3">インキローラー洗浄液</div>
                <? $name="sample_in-5"; $value="ロールクリンKR-1 (1L)";?>
                <label><input type="checkbox" class="title_in_num" name="<?=$name;?>" value="<?=$value;?>" <?= set_checkbox($name, $value); ?>><?=$value;?></label>
                <? $name="sample_in_num5";?>
                <div id="num_select_ok5" <?php if(set_value('sample_in-5') == "ロールクリンKR-1 (1L)"){?>style="display:block;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown($name, $this->config->item('sample_in_num'),set_value('sample_in_num5'));?><br>
                </div>
                <div id="num_select_no5" <?php if(set_value('sample_in-5') == "ロールクリンKR-1 (1L)"){?>style="display:none;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown('sample_in_num_no', $this->config->item('sample_in_num_disabled'));?><br>
                </div>

                <? $name="sample_in-6"; $value="セーフティーUVインキ洗浄液 SV-1 (1L)" ?>
                <label><input type="checkbox" class="title_in_num" name="<?=$name;?>" value="<?=$value;?>" <?= set_checkbox($name, $value); ?>><?=$value;?></label>
                <? $name="sample_in_num6";?>

                <div id="num_select_ok6" <?php if(set_value('sample_in-6') == "セーフティーUVインキ洗浄液 SV-1 (1L)"){?>style="display:block;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown($name, $this->config->item('sample_in_num'),set_value('sample_in_num6'));?><br>
                </div>
                <div id="num_select_no6" <?php if(set_value('sample_in-6') == "セーフティーUVインキ洗浄液 SV-1 (1L)"){?>style="display:none;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown('sample_in_num_no', $this->config->item('sample_in_num_disabled'));?><br>
                </div>

                <? $name="sample_in-7"; $value="UVインキ洗浄液 KV-9 (1L)" ?>
                <label><input type="checkbox" class="title_in_num" name="<?=$name;?>" value="<?=$value;?>" <?= set_checkbox($name, $value); ?>><?=$value;?></label>
                <? $name="sample_in_num7";?>

                <div id="num_select_ok7" <?php if(set_value('sample_in-7') == "UVインキ洗浄液 KV-9 (1L)"){?>style="display:block;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown($name, $this->config->item('sample_in_num'),set_value('sample_in_num7'));?><br>
                </div>
                <div id="num_select_no7" <?php if(set_value('sample_in-7') == "UVインキ洗浄液 KV-9 (1L)"){?>style="display:none;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown('sample_in_num_no', $this->config->item('sample_in_num_disabled'));?><br>
                </div>

                <? $name="sample_in-10"; $value="ラバーメンテナンスクリーナー RB-2EX (200ml)" ?>
                <div class="new-icon"><div class="new-icon__in">NEW</div></div>
                <label><input type="checkbox" class="title_in_num" name="<?=$name;?>" value="<?=$value;?>" <?= set_checkbox($name, $value); ?>><?=$value;?></label>
                <? $name="sample_in_num10";?>

                <div id="num_select_ok10" <?php if(set_value('sample_in-10') == "ラバーメンテナンスクリーナー RB-2EX (200ml)"){?>style="display:block;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown($name, $this->config->item('sample_in_num'),set_value('sample_in_num10'));?><br>
                </div>
                <div id="num_select_no10" <?php if(set_value('sample_in-10') == "ラバーメンテナンスクリーナー RB-2EX (200ml)"){?>style="display:none;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown('sample_in_num_no', $this->config->item('sample_in_num_disabled'));?><br>
                </div>

                <div class="form_sample_checkboxin color4">水棒洗浄液</div>
                <? $name="sample_in-8"; $value="セーフティーダンプキーパー PK-25EX (300ml)" ?>
                <label><input type="checkbox" class="title_in_num" name="<?=$name;?>" value="<?=$value;?>" <?= set_checkbox($name, $value); ?>><?=$value;?></label>
                <? $name="sample_in_num8";?>

                <div id="num_select_ok8" <?php if(set_value('sample_in-8') == "セーフティーダンプキーパー PK-25EX (300ml)"){?>style="display:block;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown($name, $this->config->item('sample_in_num'),set_value('sample_in_num8'));?><br>
                </div>
                <div id="num_select_no8" <?php if(set_value('sample_in-8') == "セーフティーダンプキーパー PK-25EX (300ml)"){?>style="display:none;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown('sample_in_num_no', $this->config->item('sample_in_num_disabled'));?><br>
                </div>

                <div class="form_sample_checkboxin color5">除電滑走剤</div>
                <? $name="sample_in-9"; $value="S-cubos 55S (17kg)" ?>
                <label><input type="checkbox" class="title_in_num" name="<?=$name;?>" value="<?=$value;?>" <?= set_checkbox($name, $value); ?>><?=$value;?></label>
                <? $name="sample_in_num9";?>

                <div id="num_select_ok9" <?php if(set_value('sample_in-9') == "S-cubos 55S (17kg)"){?>style="display:block;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown($name, $this->config->item('sample_in_num'),set_value('sample_in_num9'));?><br>
                </div>
                <div id="num_select_no9" <?php if(set_value('sample_in-9') == "S-cubos 55S (17kg)"){?>style="display:none;"<?php }?>>
                    <?php echo  form_error('sample_in_num'); ?><?php echo form_dropdown('sample_in_num_no', $this->config->item('sample_in_num_disabled'));?><br>
                </div>

			</dd>
        </dl>
        <dl class="<?php echo (form_error('sample_intex'))?'error':'';?>">
            <dt>その他ご要望</dt>
            <dd><?= form_error('sample_intex'); ?>
                <?php echo form_textarea(array('name'=>'sample_intex','value'=>set_value('sample_intex'), 'cols'=>50, 'rows'=>10));?> 
			</dd>
        </dl>
    </div>
 

    <div id="check-all" <?php if(set_value('contact_title1') == "製品に関するお問合せ" || set_value('contact_title2') == "ご購入時に関するお問合せ" || set_value('contact_title3') == "サンプルのご依頼" ){?>style="display:block;"<?php }?>>
        <dl class="<?php echo (form_error('name'))?'error':'';?>">
            <dt>お名前（全角記入）　<span class="red"><br>
                ※必須</span></dt>
            <dd>
                <?php echo  form_error('name'); ?><?php echo form_input(array('name'=>'name','value'=>set_value('name'), 'size'=>'30'));?>
                <br>
                例）山田太郎</dd>
        </dl>
        <dl class="<?php echo (form_error('company'))?'error':'';?>">
            <dt>所属機関（全角記入）　<span class="red"><br>
                ※必須</span></dt>
            <dd>
                <?php echo  form_error('company'); ?><?php echo form_input(array('name'=>'company','value'=>set_value('company'), 'size'=>'30'));?>
                <br>
                例）○○○○株式会社</dd>
        </dl>
        <dl class="<?php echo (form_error('section'))?'error':'';?>">
            <dt>所属部署（全角記入）　</dt>
            <dd>
                <?php echo  form_error('section'); ?><?php echo form_input(array('name'=>'section','value'=>set_value('section'), 'size'=>'30'));?>
                <br>
                例）○○○部○○課</dd>
        </dl>
        <dl class="<?php echo (form_error('position'))?'error':'';?>">
            <dt>役職（全角記入）　</dt>
            <dd>
                <?php echo  form_error('position'); ?><?php echo form_input(array('name'=>'position','value'=>set_value('position'), 'size'=>'30'));?>
                <br>
                例）主任</dd>
        </dl>
        <dl class="<?php echo (form_error('email'))?'error':'';?>">
            <dt>メ－ルアドレス（半角記入）　<br>
                <span class="red">※必須</span></dt>
            <dd>
                <?php echo  form_error('email'); ?><?php echo form_input(array('name'=>'email','value'=>set_value('email'), 'oncopy'=>"alert('コピーできません');return false", 'size'=>50));?>
                <br>
                例）taro@abcxyz.co.jp</dd>
        </dl>
        <dl class="<?php echo (form_error('email2'))?'error':'';?>">
            <dt>メ－ルアドレス（半角記入）確認用　<br>
                <span class="red">※必須</span></dt>
            <dd>
                <?php echo  form_error('email2'); ?><?php echo form_input(array('name'=>'email2','value'=>set_value('email2'), 'onpaste'=>"alert('貼付けできません');return false", 'size'=>50));?>
            </dd>
        </dl>
        <dl class="<?php echo (form_error('company'))?'error':'';?>">
            <dt>郵便番号（半角記入）</dt>
            <dd>
                <?php echo  form_error('zip'); ?><?php echo form_input(array('name'=>'zip','value'=>set_value('zip'), 'size'=>'15','onKeyUp'=>"AjaxZip3.zip2addr(this,'','pref','address');"));?>
                <br>
                例） 536-0025</dd>
        </dl>
        <dl class="<?php echo (form_error('pref'))?'error':'';?>">
            <dt>都道府県<br>
                <span class="red">※必須</span></dt>
            <dd>
            <?php echo  form_error('pref'); ?><?php echo form_dropdown('pref', $this->config->item('pref_list'),set_value('pref'));?>
                
            </dd>
        </dl>
        <dl class="<?php echo (form_error('address'))?'error':'';?>">
            <dt>ご住所</dt>
            <dd>
                <?php echo  form_error('address'); ?><?php echo form_input(array('name'=>'address','value'=>set_value('address'), 'size'=>'50'));?>
                <br>
                例）大阪市城東区森之宮2-3-5 </dd>
        </dl>
        <dl class="<?php echo (form_error('tel'))?'error':'';?>">
            <dt>お電話番号</dt>
            <dd>
                <?php echo  form_error('tel'); ?><?php echo form_input(array('name'=>'tel','value'=>set_value('tel'), 'size'=>'30'));?>
                <br>
                例）06-6969-0026 </dd>
        </dl>
        <dl class="<?php echo (form_error('supplier'))?'error':'';?>">
            <dt>貴社への印材納入業者様名</dt>
            <dd>
                <?php echo  form_error('supplier'); ?><?php echo form_input(array('name'=>'supplier','value'=>set_value('supplier'), 'size'=>'40'));?>
            </dd>
        </dl>
        <dl>
            <div class="box-contact-01">
                <p class="text-02">上記フォームの情報を「送信」する前に、下記の「当社における個人情報の取り扱いについて」をご一読の上、<br>
                ご同意いただける場合は「同意する」にチェックを入れてください。</p>
                <p class="ind-01"><a href="/privacy/" target="_blank" class="btn-01"><span>当社における個人情報の取り扱いについて</span></a></p>
                <p class="check">
                    <?php echo  form_error('privacy'); ?>
                    <label><input type="checkbox" name="privacy" value="privacy" <?php echo set_checkbox('privacy', 'privacy'); ?>>
                        同意する</label></p>
            </div>
        </dl>
        <dl>
            <dd>
                <input class="btn-confirm" type="submit" value="内容を確認する">
            </dd>
        </dl>
    </div><!--#check-all-->
</div>
    </form>