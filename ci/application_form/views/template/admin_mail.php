お問合せから下記のお問合せがありましたので、ご連絡致します。

■お問合せ内容

[日時]
<?php echo wrapMAIL(date("Y-m-d H:i:s") . "\r\n"); ?>

<?php if(set_value('contact_title1')){ ?>
[製品に関するお問合せ]
<?=implode("\r\n",set_value('contact_title_in1'))."\r\n";?>

[製品に関するお問合せ 内容]
<?php echo wrapMAIL(set_value("contact_title_in1tex")."\r\n"); ?>

<?php } ?>
<?php if(set_value('contact_title2')){ ?>
[ご購入時に関するお問合せ]
<?=implode("\r\n",set_value('contact_title_in2'))."\r\n";?>

[ご購入時に関するお問合せ 内容]
<?php echo wrapMAIL(set_value("contact_title_in2tex")."\r\n"); ?>

<?php } ?>
<?php if(set_value('contact_title3')){?>
[サンプルのご依頼]
<?php if(set_value('sample_in-0')||set_value('sample_in-1')||set_value('sample_in-2')){ ?>
湿し水エッチ液
<?php }?>
<?php if(set_value('sample_in-0'))echo wrapMAIL(set_value('sample_in-0').' '.set_value('sample_in_num0') . "\r\n");?>
<?php if(set_value('sample_in-1'))echo wrapMAIL(set_value('sample_in-1').' '.set_value('sample_in_num1') . "\r\n");?>
<?php if(set_value('sample_in-2'))echo wrapMAIL(set_value('sample_in-2').' '.set_value('sample_in_num2') . "\r\n");?>
<?php if(set_value('sample_in-3')||set_value('sample_in-4')){ ?>

湿し水添加剤
<?php }?>
<?php if(set_value('sample_in-3'))echo wrapMAIL(set_value('sample_in-3').' '.set_value('sample_in_num3') . "\r\n");?>
<?php if(set_value('sample_in-4'))echo wrapMAIL(set_value('sample_in-4').' '.set_value('sample_in_num4') . "\r\n");?>
<?php if(set_value('sample_in-5')||set_value('sample_in-6')||set_value('sample_in-7')||set_value('sample_in-10')){ ?>

インキローラー洗浄液
<?php }?>
<?php if(set_value('sample_in-5'))echo wrapMAIL(set_value('sample_in-5').' '.set_value('sample_in_num5') . "\r\n");?>
<?php if(set_value('sample_in-6'))echo wrapMAIL(set_value('sample_in-6').' '.set_value('sample_in_num6') . "\r\n");?>
<?php if(set_value('sample_in-7'))echo wrapMAIL(set_value('sample_in-7').' '.set_value('sample_in_num7') . "\r\n");?>
<?php if(set_value('sample_in-10'))echo wrapMAIL(set_value('sample_in-10').' '.set_value('sample_in_num10') . "\r\n");?>
<?php if(set_value('sample_in-8')){ ?>

水棒洗浄液
<?php }?>
<?php if(set_value('sample_in-8'))echo wrapMAIL(set_value('sample_in-8').' '.set_value('sample_in_num8') . "\r\n");?>
<?php if(set_value('sample_in-9')){ ?>

除電滑走剤
<?php }?>
<?php if(set_value('sample_in-9'))echo wrapMAIL(set_value('sample_in-9').' '.set_value('sample_in_num9') . "\r\n");?>

[その他ご要望]
<?php echo wrapMAIL(set_value("sample_intex")."\r\n");?>

<?php } ?>
[お名前]
<?php echo wrapMAIL(set_value('name') . "\r\n"); ?>

[会社名]
<?php echo wrapMAIL(set_value('company') . "\r\n"); ?>

[部署名]
<?php echo wrapMAIL(set_value('section') . "\r\n"); ?>

[役職]
<?php echo wrapMAIL(set_value('position') . "\r\n"); ?>

[メールアドレス]
<?php echo wrapMAIL(set_value('email') . "\r\n"); ?>

[ご住所]
〒<?php echo wrapMAIL(set_value('zip') . "\r\n"); ?>
<?php echo wrapMAIL(set_value('pref')); ?><?php echo wrapMAIL(set_value('address') . "\r\n"); ?>

[お電話番号]
<?php echo wrapMAIL(set_value('tel') . "\r\n"); ?>

[貴社への印材納入業者様名]
<?php echo wrapMAIL(set_value('supplier') . "\r\n"); ?>


以上ご連絡致しますので、ご対応よろしくお願い致します。


==　光陽化学工業株式会社　============================================

大阪本社：536-0025　大阪府大阪市城東区森之宮2-3-5
[TEL]06-6969-0026　[FAX]06-6969-1824

東京支社：103-0024　東京都中央区日本橋小舟町15-15　ルネ小舟町ビル２Ｆ
[TEL]03-3661-2700　[FAX]03-3661-2706

======================================================================